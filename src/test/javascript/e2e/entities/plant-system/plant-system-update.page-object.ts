import { element, by, ElementFinder } from 'protractor';

export default class PlantSystemUpdatePage {
  pageTitle: ElementFinder = element(by.id('iplantNestApp.plantSystem.home.createOrEditLabel'));
  saveButton: ElementFinder = element(by.id('save-entity'));
  cancelButton: ElementFinder = element(by.id('cancel-save'));
  codeInput: ElementFinder = element(by.css('input#plant-system-code'));
  nameInput: ElementFinder = element(by.css('input#plant-system-name'));
  commentInput: ElementFinder = element(by.css('textarea#plant-system-comment'));
  plantsSelect: ElementFinder = element(by.css('select#plant-system-plants'));

  getPageTitle() {
    return this.pageTitle;
  }

  async setCodeInput(code) {
    await this.codeInput.sendKeys(code);
  }

  async getCodeInput() {
    return this.codeInput.getAttribute('value');
  }

  async setNameInput(name) {
    await this.nameInput.sendKeys(name);
  }

  async getNameInput() {
    return this.nameInput.getAttribute('value');
  }

  async setCommentInput(comment) {
    await this.commentInput.sendKeys(comment);
  }

  async getCommentInput() {
    return this.commentInput.getAttribute('value');
  }

  async plantsSelectLastOption() {
    await this.plantsSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async plantsSelectOption(option) {
    await this.plantsSelect.sendKeys(option);
  }

  getPlantsSelect() {
    return this.plantsSelect;
  }

  async getPlantsSelectedOption() {
    return this.plantsSelect.element(by.css('option:checked')).getText();
  }

  async save() {
    await this.saveButton.click();
  }

  async cancel() {
    await this.cancelButton.click();
  }

  getSaveButton() {
    return this.saveButton;
  }
}
