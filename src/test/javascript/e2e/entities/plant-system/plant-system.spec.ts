import { browser, element, by } from 'protractor';

import NavBarPage from './../../page-objects/navbar-page';
import SignInPage from './../../page-objects/signin-page';
import PlantSystemComponentsPage, { PlantSystemDeleteDialog } from './plant-system.page-object';
import PlantSystemUpdatePage from './plant-system-update.page-object';
import {
  waitUntilDisplayed,
  waitUntilAnyDisplayed,
  click,
  getRecordsCount,
  waitUntilHidden,
  waitUntilCount,
  isVisible
} from '../../util/utils';

const expect = chai.expect;

describe('PlantSystem e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let plantSystemComponentsPage: PlantSystemComponentsPage;
  let plantSystemUpdatePage: PlantSystemUpdatePage;
  let plantSystemDeleteDialog: PlantSystemDeleteDialog;
  let beforeRecordsCount = 0;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.waitUntilDisplayed();

    await signInPage.username.sendKeys('admin');
    await signInPage.password.sendKeys('admin');
    await signInPage.loginButton.click();
    await signInPage.waitUntilHidden();
    await waitUntilDisplayed(navBarPage.entityMenu);
    await waitUntilDisplayed(navBarPage.adminMenu);
    await waitUntilDisplayed(navBarPage.accountMenu);
  });

  it('should load PlantSystems', async () => {
    await navBarPage.getEntityPage('plant-system');
    plantSystemComponentsPage = new PlantSystemComponentsPage();
    expect(await plantSystemComponentsPage.title.getText()).to.match(/Plant Systems/);

    expect(await plantSystemComponentsPage.createButton.isEnabled()).to.be.true;
    await waitUntilAnyDisplayed([plantSystemComponentsPage.noRecords, plantSystemComponentsPage.table]);

    beforeRecordsCount = (await isVisible(plantSystemComponentsPage.noRecords))
      ? 0
      : await getRecordsCount(plantSystemComponentsPage.table);
  });

  it('should load create PlantSystem page', async () => {
    await plantSystemComponentsPage.createButton.click();
    plantSystemUpdatePage = new PlantSystemUpdatePage();
    expect(await plantSystemUpdatePage.getPageTitle().getText()).to.match(/Create or edit a PlantSystem/);
    await plantSystemUpdatePage.cancel();
  });

  it('should create and save PlantSystems', async () => {
    await plantSystemComponentsPage.createButton.click();
    await plantSystemUpdatePage.setCodeInput('code');
    expect(await plantSystemUpdatePage.getCodeInput()).to.match(/code/);
    await plantSystemUpdatePage.setNameInput('name');
    expect(await plantSystemUpdatePage.getNameInput()).to.match(/name/);
    await plantSystemUpdatePage.setCommentInput('comment');
    expect(await plantSystemUpdatePage.getCommentInput()).to.match(/comment/);
    await plantSystemUpdatePage.plantsSelectLastOption();
    await waitUntilDisplayed(plantSystemUpdatePage.saveButton);
    await plantSystemUpdatePage.save();
    await waitUntilHidden(plantSystemUpdatePage.saveButton);
    expect(await isVisible(plantSystemUpdatePage.saveButton)).to.be.false;

    expect(await plantSystemComponentsPage.createButton.isEnabled()).to.be.true;

    await waitUntilDisplayed(plantSystemComponentsPage.table);

    await waitUntilCount(plantSystemComponentsPage.records, beforeRecordsCount + 1);
    expect(await plantSystemComponentsPage.records.count()).to.eq(beforeRecordsCount + 1);
  });

  it('should delete last PlantSystem', async () => {
    const deleteButton = plantSystemComponentsPage.getDeleteButton(plantSystemComponentsPage.records.last());
    await click(deleteButton);

    plantSystemDeleteDialog = new PlantSystemDeleteDialog();
    await waitUntilDisplayed(plantSystemDeleteDialog.deleteModal);
    expect(await plantSystemDeleteDialog.getDialogTitle().getAttribute('id')).to.match(/iplantNestApp.plantSystem.delete.question/);
    await plantSystemDeleteDialog.clickOnConfirmButton();

    await waitUntilHidden(plantSystemDeleteDialog.deleteModal);

    expect(await isVisible(plantSystemDeleteDialog.deleteModal)).to.be.false;

    await waitUntilAnyDisplayed([plantSystemComponentsPage.noRecords, plantSystemComponentsPage.table]);

    const afterCount = (await isVisible(plantSystemComponentsPage.noRecords)) ? 0 : await getRecordsCount(plantSystemComponentsPage.table);
    expect(afterCount).to.eq(beforeRecordsCount);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
