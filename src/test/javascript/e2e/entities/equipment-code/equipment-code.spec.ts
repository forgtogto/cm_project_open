import { browser, element, by } from 'protractor';

import NavBarPage from './../../page-objects/navbar-page';
import SignInPage from './../../page-objects/signin-page';
import EquipmentCodeComponentsPage, { EquipmentCodeDeleteDialog } from './equipment-code.page-object';
import EquipmentCodeUpdatePage from './equipment-code-update.page-object';
import {
  waitUntilDisplayed,
  waitUntilAnyDisplayed,
  click,
  getRecordsCount,
  waitUntilHidden,
  waitUntilCount,
  isVisible
} from '../../util/utils';

const expect = chai.expect;

describe('EquipmentCode e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let equipmentCodeComponentsPage: EquipmentCodeComponentsPage;
  let equipmentCodeUpdatePage: EquipmentCodeUpdatePage;
  let equipmentCodeDeleteDialog: EquipmentCodeDeleteDialog;
  let beforeRecordsCount = 0;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.waitUntilDisplayed();

    await signInPage.username.sendKeys('admin');
    await signInPage.password.sendKeys('admin');
    await signInPage.loginButton.click();
    await signInPage.waitUntilHidden();
    await waitUntilDisplayed(navBarPage.entityMenu);
    await waitUntilDisplayed(navBarPage.adminMenu);
    await waitUntilDisplayed(navBarPage.accountMenu);
  });

  it('should load EquipmentCodes', async () => {
    await navBarPage.getEntityPage('equipment-code');
    equipmentCodeComponentsPage = new EquipmentCodeComponentsPage();
    expect(await equipmentCodeComponentsPage.title.getText()).to.match(/Equipment Codes/);

    expect(await equipmentCodeComponentsPage.createButton.isEnabled()).to.be.true;
    await waitUntilAnyDisplayed([equipmentCodeComponentsPage.noRecords, equipmentCodeComponentsPage.table]);

    beforeRecordsCount = (await isVisible(equipmentCodeComponentsPage.noRecords))
      ? 0
      : await getRecordsCount(equipmentCodeComponentsPage.table);
  });

  it('should load create EquipmentCode page', async () => {
    await equipmentCodeComponentsPage.createButton.click();
    equipmentCodeUpdatePage = new EquipmentCodeUpdatePage();
    expect(await equipmentCodeUpdatePage.getPageTitle().getText()).to.match(/Create or edit a EquipmentCode/);
    await equipmentCodeUpdatePage.cancel();
  });

  it('should create and save EquipmentCodes', async () => {
    await equipmentCodeComponentsPage.createButton.click();
    await equipmentCodeUpdatePage.setCodeInput('code');
    expect(await equipmentCodeUpdatePage.getCodeInput()).to.match(/code/);
    await equipmentCodeUpdatePage.setCommentInput('comment');
    expect(await equipmentCodeUpdatePage.getCommentInput()).to.match(/comment/);
    await waitUntilDisplayed(equipmentCodeUpdatePage.saveButton);
    await equipmentCodeUpdatePage.save();
    await waitUntilHidden(equipmentCodeUpdatePage.saveButton);
    expect(await isVisible(equipmentCodeUpdatePage.saveButton)).to.be.false;

    expect(await equipmentCodeComponentsPage.createButton.isEnabled()).to.be.true;

    await waitUntilDisplayed(equipmentCodeComponentsPage.table);

    await waitUntilCount(equipmentCodeComponentsPage.records, beforeRecordsCount + 1);
    expect(await equipmentCodeComponentsPage.records.count()).to.eq(beforeRecordsCount + 1);
  });

  it('should delete last EquipmentCode', async () => {
    const deleteButton = equipmentCodeComponentsPage.getDeleteButton(equipmentCodeComponentsPage.records.last());
    await click(deleteButton);

    equipmentCodeDeleteDialog = new EquipmentCodeDeleteDialog();
    await waitUntilDisplayed(equipmentCodeDeleteDialog.deleteModal);
    expect(await equipmentCodeDeleteDialog.getDialogTitle().getAttribute('id')).to.match(/iplantNestApp.equipmentCode.delete.question/);
    await equipmentCodeDeleteDialog.clickOnConfirmButton();

    await waitUntilHidden(equipmentCodeDeleteDialog.deleteModal);

    expect(await isVisible(equipmentCodeDeleteDialog.deleteModal)).to.be.false;

    await waitUntilAnyDisplayed([equipmentCodeComponentsPage.noRecords, equipmentCodeComponentsPage.table]);

    const afterCount = (await isVisible(equipmentCodeComponentsPage.noRecords))
      ? 0
      : await getRecordsCount(equipmentCodeComponentsPage.table);
    expect(afterCount).to.eq(beforeRecordsCount);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
