import { element, by, ElementFinder } from 'protractor';

export default class CompPipeUpdatePage {
  pageTitle: ElementFinder = element(by.id('iplantNestApp.compPipe.home.createOrEditLabel'));
  saveButton: ElementFinder = element(by.id('save-entity'));
  cancelButton: ElementFinder = element(by.id('cancel-save'));
  pipeClassInput: ElementFinder = element(by.css('input#comp-pipe-pipeClass'));
  scheduleInput: ElementFinder = element(by.css('input#comp-pipe-schedule'));
  thicknessInput: ElementFinder = element(by.css('input#comp-pipe-thickness'));
  plantSystemSelect: ElementFinder = element(by.css('select#comp-pipe-plantSystem'));
  equipmentCodeSelect: ElementFinder = element(by.css('select#comp-pipe-equipmentCode'));

  getPageTitle() {
    return this.pageTitle;
  }

  async setPipeClassInput(pipeClass) {
    await this.pipeClassInput.sendKeys(pipeClass);
  }

  async getPipeClassInput() {
    return this.pipeClassInput.getAttribute('value');
  }

  async setScheduleInput(schedule) {
    await this.scheduleInput.sendKeys(schedule);
  }

  async getScheduleInput() {
    return this.scheduleInput.getAttribute('value');
  }

  async setThicknessInput(thickness) {
    await this.thicknessInput.sendKeys(thickness);
  }

  async getThicknessInput() {
    return this.thicknessInput.getAttribute('value');
  }

  async plantSystemSelectLastOption() {
    await this.plantSystemSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async plantSystemSelectOption(option) {
    await this.plantSystemSelect.sendKeys(option);
  }

  getPlantSystemSelect() {
    return this.plantSystemSelect;
  }

  async getPlantSystemSelectedOption() {
    return this.plantSystemSelect.element(by.css('option:checked')).getText();
  }

  async equipmentCodeSelectLastOption() {
    await this.equipmentCodeSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async equipmentCodeSelectOption(option) {
    await this.equipmentCodeSelect.sendKeys(option);
  }

  getEquipmentCodeSelect() {
    return this.equipmentCodeSelect;
  }

  async getEquipmentCodeSelectedOption() {
    return this.equipmentCodeSelect.element(by.css('option:checked')).getText();
  }

  async save() {
    await this.saveButton.click();
  }

  async cancel() {
    await this.cancelButton.click();
  }

  getSaveButton() {
    return this.saveButton;
  }
}
