import { browser, element, by } from 'protractor';

import NavBarPage from './../../page-objects/navbar-page';
import SignInPage from './../../page-objects/signin-page';
import CompPipeComponentsPage, { CompPipeDeleteDialog } from './comp-pipe.page-object';
import CompPipeUpdatePage from './comp-pipe-update.page-object';
import {
  waitUntilDisplayed,
  waitUntilAnyDisplayed,
  click,
  getRecordsCount,
  waitUntilHidden,
  waitUntilCount,
  isVisible
} from '../../util/utils';

const expect = chai.expect;

describe('CompPipe e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let compPipeComponentsPage: CompPipeComponentsPage;
  let compPipeUpdatePage: CompPipeUpdatePage;
  let compPipeDeleteDialog: CompPipeDeleteDialog;
  let beforeRecordsCount = 0;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.waitUntilDisplayed();

    await signInPage.username.sendKeys('admin');
    await signInPage.password.sendKeys('admin');
    await signInPage.loginButton.click();
    await signInPage.waitUntilHidden();
    await waitUntilDisplayed(navBarPage.entityMenu);
    await waitUntilDisplayed(navBarPage.adminMenu);
    await waitUntilDisplayed(navBarPage.accountMenu);
  });

  it('should load CompPipes', async () => {
    await navBarPage.getEntityPage('comp-pipe');
    compPipeComponentsPage = new CompPipeComponentsPage();
    expect(await compPipeComponentsPage.title.getText()).to.match(/Comp Pipes/);

    expect(await compPipeComponentsPage.createButton.isEnabled()).to.be.true;
    await waitUntilAnyDisplayed([compPipeComponentsPage.noRecords, compPipeComponentsPage.table]);

    beforeRecordsCount = (await isVisible(compPipeComponentsPage.noRecords)) ? 0 : await getRecordsCount(compPipeComponentsPage.table);
  });

  it('should load create CompPipe page', async () => {
    await compPipeComponentsPage.createButton.click();
    compPipeUpdatePage = new CompPipeUpdatePage();
    expect(await compPipeUpdatePage.getPageTitle().getText()).to.match(/Create or edit a CompPipe/);
    await compPipeUpdatePage.cancel();
  });

  it('should create and save CompPipes', async () => {
    await compPipeComponentsPage.createButton.click();
    await compPipeUpdatePage.setPipeClassInput('pipeClass');
    expect(await compPipeUpdatePage.getPipeClassInput()).to.match(/pipeClass/);
    await compPipeUpdatePage.setScheduleInput('schedule');
    expect(await compPipeUpdatePage.getScheduleInput()).to.match(/schedule/);
    await compPipeUpdatePage.setThicknessInput('thickness');
    expect(await compPipeUpdatePage.getThicknessInput()).to.match(/thickness/);
    await compPipeUpdatePage.plantSystemSelectLastOption();
    await compPipeUpdatePage.equipmentCodeSelectLastOption();
    await waitUntilDisplayed(compPipeUpdatePage.saveButton);
    await compPipeUpdatePage.save();
    await waitUntilHidden(compPipeUpdatePage.saveButton);
    expect(await isVisible(compPipeUpdatePage.saveButton)).to.be.false;

    expect(await compPipeComponentsPage.createButton.isEnabled()).to.be.true;

    await waitUntilDisplayed(compPipeComponentsPage.table);

    await waitUntilCount(compPipeComponentsPage.records, beforeRecordsCount + 1);
    expect(await compPipeComponentsPage.records.count()).to.eq(beforeRecordsCount + 1);
  });

  it('should delete last CompPipe', async () => {
    const deleteButton = compPipeComponentsPage.getDeleteButton(compPipeComponentsPage.records.last());
    await click(deleteButton);

    compPipeDeleteDialog = new CompPipeDeleteDialog();
    await waitUntilDisplayed(compPipeDeleteDialog.deleteModal);
    expect(await compPipeDeleteDialog.getDialogTitle().getAttribute('id')).to.match(/iplantNestApp.compPipe.delete.question/);
    await compPipeDeleteDialog.clickOnConfirmButton();

    await waitUntilHidden(compPipeDeleteDialog.deleteModal);

    expect(await isVisible(compPipeDeleteDialog.deleteModal)).to.be.false;

    await waitUntilAnyDisplayed([compPipeComponentsPage.noRecords, compPipeComponentsPage.table]);

    const afterCount = (await isVisible(compPipeComponentsPage.noRecords)) ? 0 : await getRecordsCount(compPipeComponentsPage.table);
    expect(afterCount).to.eq(beforeRecordsCount);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
