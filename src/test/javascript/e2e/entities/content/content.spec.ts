import { browser, element, by } from 'protractor';

import NavBarPage from './../../page-objects/navbar-page';
import SignInPage from './../../page-objects/signin-page';
import ContentComponentsPage, { ContentDeleteDialog } from './content.page-object';
import ContentUpdatePage from './content-update.page-object';
import {
  waitUntilDisplayed,
  waitUntilAnyDisplayed,
  click,
  getRecordsCount,
  waitUntilHidden,
  waitUntilCount,
  isVisible
} from '../../util/utils';

const expect = chai.expect;

describe('Content e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let contentComponentsPage: ContentComponentsPage;
  let contentUpdatePage: ContentUpdatePage;
  let contentDeleteDialog: ContentDeleteDialog;
  let beforeRecordsCount = 0;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.waitUntilDisplayed();

    await signInPage.username.sendKeys('admin');
    await signInPage.password.sendKeys('admin');
    await signInPage.loginButton.click();
    await signInPage.waitUntilHidden();
    await waitUntilDisplayed(navBarPage.entityMenu);
    await waitUntilDisplayed(navBarPage.adminMenu);
    await waitUntilDisplayed(navBarPage.accountMenu);
  });

  it('should load Contents', async () => {
    await navBarPage.getEntityPage('content');
    contentComponentsPage = new ContentComponentsPage();
    expect(await contentComponentsPage.title.getText()).to.match(/Contents/);

    expect(await contentComponentsPage.createButton.isEnabled()).to.be.true;
    await waitUntilAnyDisplayed([contentComponentsPage.noRecords, contentComponentsPage.table]);

    beforeRecordsCount = (await isVisible(contentComponentsPage.noRecords)) ? 0 : await getRecordsCount(contentComponentsPage.table);
  });

  it('should load create Content page', async () => {
    await contentComponentsPage.createButton.click();
    contentUpdatePage = new ContentUpdatePage();
    expect(await contentUpdatePage.getPageTitle().getText()).to.match(/Create or edit a Content/);
    await contentUpdatePage.cancel();
  });

  it('should create and save Contents', async () => {
    await contentComponentsPage.createButton.click();
    await contentUpdatePage.setVersionInput('version');
    expect(await contentUpdatePage.getVersionInput()).to.match(/version/);
    await contentUpdatePage.setCodeInput('code');
    expect(await contentUpdatePage.getCodeInput()).to.match(/code/);
    await contentUpdatePage.setNameInput('name');
    expect(await contentUpdatePage.getNameInput()).to.match(/name/);
    await contentUpdatePage.setSerialNoInput('serialNo');
    expect(await contentUpdatePage.getSerialNoInput()).to.match(/serialNo/);
    await contentUpdatePage.setFilePathInput('filePath');
    expect(await contentUpdatePage.getFilePathInput()).to.match(/filePath/);
    await contentUpdatePage.setFileNameInput('fileName');
    expect(await contentUpdatePage.getFileNameInput()).to.match(/fileName/);
    await contentUpdatePage.plantSystemSelectLastOption();
    await contentUpdatePage.departmentCodeSelectLastOption();
    await waitUntilDisplayed(contentUpdatePage.saveButton);
    await contentUpdatePage.save();
    await waitUntilHidden(contentUpdatePage.saveButton);
    expect(await isVisible(contentUpdatePage.saveButton)).to.be.false;

    expect(await contentComponentsPage.createButton.isEnabled()).to.be.true;

    await waitUntilDisplayed(contentComponentsPage.table);

    await waitUntilCount(contentComponentsPage.records, beforeRecordsCount + 1);
    expect(await contentComponentsPage.records.count()).to.eq(beforeRecordsCount + 1);
  });

  it('should delete last Content', async () => {
    const deleteButton = contentComponentsPage.getDeleteButton(contentComponentsPage.records.last());
    await click(deleteButton);

    contentDeleteDialog = new ContentDeleteDialog();
    await waitUntilDisplayed(contentDeleteDialog.deleteModal);
    expect(await contentDeleteDialog.getDialogTitle().getAttribute('id')).to.match(/iplantNestApp.content.delete.question/);
    await contentDeleteDialog.clickOnConfirmButton();

    await waitUntilHidden(contentDeleteDialog.deleteModal);

    expect(await isVisible(contentDeleteDialog.deleteModal)).to.be.false;

    await waitUntilAnyDisplayed([contentComponentsPage.noRecords, contentComponentsPage.table]);

    const afterCount = (await isVisible(contentComponentsPage.noRecords)) ? 0 : await getRecordsCount(contentComponentsPage.table);
    expect(afterCount).to.eq(beforeRecordsCount);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
