import { element, by, ElementFinder } from 'protractor';

export default class ContentUpdatePage {
  pageTitle: ElementFinder = element(by.id('iplantNestApp.content.home.createOrEditLabel'));
  saveButton: ElementFinder = element(by.id('save-entity'));
  cancelButton: ElementFinder = element(by.id('cancel-save'));
  versionInput: ElementFinder = element(by.css('input#content-version'));
  codeInput: ElementFinder = element(by.css('input#content-code'));
  nameInput: ElementFinder = element(by.css('input#content-name'));
  serialNoInput: ElementFinder = element(by.css('input#content-serialNo'));
  filePathInput: ElementFinder = element(by.css('input#content-filePath'));
  fileNameInput: ElementFinder = element(by.css('input#content-fileName'));
  plantSystemSelect: ElementFinder = element(by.css('select#content-plantSystem'));
  departmentCodeSelect: ElementFinder = element(by.css('select#content-departmentCode'));

  getPageTitle() {
    return this.pageTitle;
  }

  async setVersionInput(version) {
    await this.versionInput.sendKeys(version);
  }

  async getVersionInput() {
    return this.versionInput.getAttribute('value');
  }

  async setCodeInput(code) {
    await this.codeInput.sendKeys(code);
  }

  async getCodeInput() {
    return this.codeInput.getAttribute('value');
  }

  async setNameInput(name) {
    await this.nameInput.sendKeys(name);
  }

  async getNameInput() {
    return this.nameInput.getAttribute('value');
  }

  async setSerialNoInput(serialNo) {
    await this.serialNoInput.sendKeys(serialNo);
  }

  async getSerialNoInput() {
    return this.serialNoInput.getAttribute('value');
  }

  async setFilePathInput(filePath) {
    await this.filePathInput.sendKeys(filePath);
  }

  async getFilePathInput() {
    return this.filePathInput.getAttribute('value');
  }

  async setFileNameInput(fileName) {
    await this.fileNameInput.sendKeys(fileName);
  }

  async getFileNameInput() {
    return this.fileNameInput.getAttribute('value');
  }

  async plantSystemSelectLastOption() {
    await this.plantSystemSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async plantSystemSelectOption(option) {
    await this.plantSystemSelect.sendKeys(option);
  }

  getPlantSystemSelect() {
    return this.plantSystemSelect;
  }

  async getPlantSystemSelectedOption() {
    return this.plantSystemSelect.element(by.css('option:checked')).getText();
  }

  async departmentCodeSelectLastOption() {
    await this.departmentCodeSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async departmentCodeSelectOption(option) {
    await this.departmentCodeSelect.sendKeys(option);
  }

  getDepartmentCodeSelect() {
    return this.departmentCodeSelect;
  }

  async getDepartmentCodeSelectedOption() {
    return this.departmentCodeSelect.element(by.css('option:checked')).getText();
  }

  async save() {
    await this.saveButton.click();
  }

  async cancel() {
    await this.cancelButton.click();
  }

  getSaveButton() {
    return this.saveButton;
  }
}
