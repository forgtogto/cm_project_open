import { element, by, ElementFinder } from 'protractor';

export default class RequirementUpdatePage {
  pageTitle: ElementFinder = element(by.id('iplantNestApp.requirement.home.createOrEditLabel'));
  saveButton: ElementFinder = element(by.id('save-entity'));
  cancelButton: ElementFinder = element(by.id('cancel-save'));
  codeInput: ElementFinder = element(by.css('input#requirement-code'));
  nameInput: ElementFinder = element(by.css('input#requirement-name'));
  commentInput: ElementFinder = element(by.css('textarea#requirement-comment'));
  reqStatusSelect: ElementFinder = element(by.css('select#requirement-reqStatus'));
  cmGradeSelect: ElementFinder = element(by.css('select#requirement-cmGrade'));
  requesterGroupSelect: ElementFinder = element(by.css('select#requirement-requesterGroup'));
  userSelect: ElementFinder = element(by.css('select#requirement-user'));
  plantSystemSelect: ElementFinder = element(by.css('select#requirement-plantSystem'));

  getPageTitle() {
    return this.pageTitle;
  }

  async setCodeInput(code) {
    await this.codeInput.sendKeys(code);
  }

  async getCodeInput() {
    return this.codeInput.getAttribute('value');
  }

  async setNameInput(name) {
    await this.nameInput.sendKeys(name);
  }

  async getNameInput() {
    return this.nameInput.getAttribute('value');
  }

  async setCommentInput(comment) {
    await this.commentInput.sendKeys(comment);
  }

  async getCommentInput() {
    return this.commentInput.getAttribute('value');
  }

  async setReqStatusSelect(reqStatus) {
    await this.reqStatusSelect.sendKeys(reqStatus);
  }

  async getReqStatusSelect() {
    return this.reqStatusSelect.element(by.css('option:checked')).getText();
  }

  async reqStatusSelectLastOption() {
    await this.reqStatusSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }
  async setCmGradeSelect(cmGrade) {
    await this.cmGradeSelect.sendKeys(cmGrade);
  }

  async getCmGradeSelect() {
    return this.cmGradeSelect.element(by.css('option:checked')).getText();
  }

  async cmGradeSelectLastOption() {
    await this.cmGradeSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }
  async setRequesterGroupSelect(requesterGroup) {
    await this.requesterGroupSelect.sendKeys(requesterGroup);
  }

  async getRequesterGroupSelect() {
    return this.requesterGroupSelect.element(by.css('option:checked')).getText();
  }

  async requesterGroupSelectLastOption() {
    await this.requesterGroupSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }
  async userSelectLastOption() {
    await this.userSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async userSelectOption(option) {
    await this.userSelect.sendKeys(option);
  }

  getUserSelect() {
    return this.userSelect;
  }

  async getUserSelectedOption() {
    return this.userSelect.element(by.css('option:checked')).getText();
  }

  async plantSystemSelectLastOption() {
    await this.plantSystemSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async plantSystemSelectOption(option) {
    await this.plantSystemSelect.sendKeys(option);
  }

  getPlantSystemSelect() {
    return this.plantSystemSelect;
  }

  async getPlantSystemSelectedOption() {
    return this.plantSystemSelect.element(by.css('option:checked')).getText();
  }

  async save() {
    await this.saveButton.click();
  }

  async cancel() {
    await this.cancelButton.click();
  }

  getSaveButton() {
    return this.saveButton;
  }
}
