import { browser, element, by } from 'protractor';

import NavBarPage from './../../page-objects/navbar-page';
import SignInPage from './../../page-objects/signin-page';
import RequirementComponentsPage, { RequirementDeleteDialog } from './requirement.page-object';
import RequirementUpdatePage from './requirement-update.page-object';
import {
  waitUntilDisplayed,
  waitUntilAnyDisplayed,
  click,
  getRecordsCount,
  waitUntilHidden,
  waitUntilCount,
  isVisible
} from '../../util/utils';

const expect = chai.expect;

describe('Requirement e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let requirementComponentsPage: RequirementComponentsPage;
  let requirementUpdatePage: RequirementUpdatePage;
  let requirementDeleteDialog: RequirementDeleteDialog;
  let beforeRecordsCount = 0;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.waitUntilDisplayed();

    await signInPage.username.sendKeys('admin');
    await signInPage.password.sendKeys('admin');
    await signInPage.loginButton.click();
    await signInPage.waitUntilHidden();
    await waitUntilDisplayed(navBarPage.entityMenu);
    await waitUntilDisplayed(navBarPage.adminMenu);
    await waitUntilDisplayed(navBarPage.accountMenu);
  });

  it('should load Requirements', async () => {
    await navBarPage.getEntityPage('requirement');
    requirementComponentsPage = new RequirementComponentsPage();
    expect(await requirementComponentsPage.title.getText()).to.match(/Requirements/);

    expect(await requirementComponentsPage.createButton.isEnabled()).to.be.true;
    await waitUntilAnyDisplayed([requirementComponentsPage.noRecords, requirementComponentsPage.table]);

    beforeRecordsCount = (await isVisible(requirementComponentsPage.noRecords))
      ? 0
      : await getRecordsCount(requirementComponentsPage.table);
  });

  it('should load create Requirement page', async () => {
    await requirementComponentsPage.createButton.click();
    requirementUpdatePage = new RequirementUpdatePage();
    expect(await requirementUpdatePage.getPageTitle().getText()).to.match(/Create or edit a Requirement/);
    await requirementUpdatePage.cancel();
  });

  it('should create and save Requirements', async () => {
    await requirementComponentsPage.createButton.click();
    await requirementUpdatePage.setCodeInput('code');
    expect(await requirementUpdatePage.getCodeInput()).to.match(/code/);
    await requirementUpdatePage.setNameInput('name');
    expect(await requirementUpdatePage.getNameInput()).to.match(/name/);
    await requirementUpdatePage.setCommentInput('comment');
    expect(await requirementUpdatePage.getCommentInput()).to.match(/comment/);
    await requirementUpdatePage.reqStatusSelectLastOption();
    await requirementUpdatePage.cmGradeSelectLastOption();
    await requirementUpdatePage.requesterGroupSelectLastOption();
    await requirementUpdatePage.userSelectLastOption();
    await requirementUpdatePage.plantSystemSelectLastOption();
    await waitUntilDisplayed(requirementUpdatePage.saveButton);
    await requirementUpdatePage.save();
    await waitUntilHidden(requirementUpdatePage.saveButton);
    expect(await isVisible(requirementUpdatePage.saveButton)).to.be.false;

    expect(await requirementComponentsPage.createButton.isEnabled()).to.be.true;

    await waitUntilDisplayed(requirementComponentsPage.table);

    await waitUntilCount(requirementComponentsPage.records, beforeRecordsCount + 1);
    expect(await requirementComponentsPage.records.count()).to.eq(beforeRecordsCount + 1);
  });

  it('should delete last Requirement', async () => {
    const deleteButton = requirementComponentsPage.getDeleteButton(requirementComponentsPage.records.last());
    await click(deleteButton);

    requirementDeleteDialog = new RequirementDeleteDialog();
    await waitUntilDisplayed(requirementDeleteDialog.deleteModal);
    expect(await requirementDeleteDialog.getDialogTitle().getAttribute('id')).to.match(/iplantNestApp.requirement.delete.question/);
    await requirementDeleteDialog.clickOnConfirmButton();

    await waitUntilHidden(requirementDeleteDialog.deleteModal);

    expect(await isVisible(requirementDeleteDialog.deleteModal)).to.be.false;

    await waitUntilAnyDisplayed([requirementComponentsPage.noRecords, requirementComponentsPage.table]);

    const afterCount = (await isVisible(requirementComponentsPage.noRecords)) ? 0 : await getRecordsCount(requirementComponentsPage.table);
    expect(afterCount).to.eq(beforeRecordsCount);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
