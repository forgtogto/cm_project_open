import { browser, element, by } from 'protractor';

import NavBarPage from './../../page-objects/navbar-page';
import SignInPage from './../../page-objects/signin-page';
import PlantsComponentsPage, { PlantsDeleteDialog } from './plants.page-object';
import PlantsUpdatePage from './plants-update.page-object';
import {
  waitUntilDisplayed,
  waitUntilAnyDisplayed,
  click,
  getRecordsCount,
  waitUntilHidden,
  waitUntilCount,
  isVisible
} from '../../util/utils';

const expect = chai.expect;

describe('Plants e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let plantsComponentsPage: PlantsComponentsPage;
  let plantsUpdatePage: PlantsUpdatePage;
  let plantsDeleteDialog: PlantsDeleteDialog;
  let beforeRecordsCount = 0;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.waitUntilDisplayed();

    await signInPage.username.sendKeys('admin');
    await signInPage.password.sendKeys('admin');
    await signInPage.loginButton.click();
    await signInPage.waitUntilHidden();
    await waitUntilDisplayed(navBarPage.entityMenu);
    await waitUntilDisplayed(navBarPage.adminMenu);
    await waitUntilDisplayed(navBarPage.accountMenu);
  });

  it('should load Plants', async () => {
    await navBarPage.getEntityPage('plants');
    plantsComponentsPage = new PlantsComponentsPage();
    expect(await plantsComponentsPage.title.getText()).to.match(/Plants/);

    expect(await plantsComponentsPage.createButton.isEnabled()).to.be.true;
    await waitUntilAnyDisplayed([plantsComponentsPage.noRecords, plantsComponentsPage.table]);

    beforeRecordsCount = (await isVisible(plantsComponentsPage.noRecords)) ? 0 : await getRecordsCount(plantsComponentsPage.table);
  });

  it('should load create Plants page', async () => {
    await plantsComponentsPage.createButton.click();
    plantsUpdatePage = new PlantsUpdatePage();
    expect(await plantsUpdatePage.getPageTitle().getText()).to.match(/Create or edit a Plants/);
    await plantsUpdatePage.cancel();
  });

  it('should create and save Plants', async () => {
    await plantsComponentsPage.createButton.click();
    await plantsUpdatePage.setCodeInput('code');
    expect(await plantsUpdatePage.getCodeInput()).to.match(/code/);
    await plantsUpdatePage.setNameInput('name');
    expect(await plantsUpdatePage.getNameInput()).to.match(/name/);
    await plantsUpdatePage.setCommentInput('comment');
    expect(await plantsUpdatePage.getCommentInput()).to.match(/comment/);
    await waitUntilDisplayed(plantsUpdatePage.saveButton);
    await plantsUpdatePage.save();
    await waitUntilHidden(plantsUpdatePage.saveButton);
    expect(await isVisible(plantsUpdatePage.saveButton)).to.be.false;

    expect(await plantsComponentsPage.createButton.isEnabled()).to.be.true;

    await waitUntilDisplayed(plantsComponentsPage.table);

    await waitUntilCount(plantsComponentsPage.records, beforeRecordsCount + 1);
    expect(await plantsComponentsPage.records.count()).to.eq(beforeRecordsCount + 1);
  });

  it('should delete last Plants', async () => {
    const deleteButton = plantsComponentsPage.getDeleteButton(plantsComponentsPage.records.last());
    await click(deleteButton);

    plantsDeleteDialog = new PlantsDeleteDialog();
    await waitUntilDisplayed(plantsDeleteDialog.deleteModal);
    expect(await plantsDeleteDialog.getDialogTitle().getAttribute('id')).to.match(/iplantNestApp.plants.delete.question/);
    await plantsDeleteDialog.clickOnConfirmButton();

    await waitUntilHidden(plantsDeleteDialog.deleteModal);

    expect(await isVisible(plantsDeleteDialog.deleteModal)).to.be.false;

    await waitUntilAnyDisplayed([plantsComponentsPage.noRecords, plantsComponentsPage.table]);

    const afterCount = (await isVisible(plantsComponentsPage.noRecords)) ? 0 : await getRecordsCount(plantsComponentsPage.table);
    expect(afterCount).to.eq(beforeRecordsCount);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
