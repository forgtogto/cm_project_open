import { browser, element, by } from 'protractor';

import NavBarPage from './../../page-objects/navbar-page';
import SignInPage from './../../page-objects/signin-page';
import DepartmentCodeComponentsPage, { DepartmentCodeDeleteDialog } from './department-code.page-object';
import DepartmentCodeUpdatePage from './department-code-update.page-object';
import {
  waitUntilDisplayed,
  waitUntilAnyDisplayed,
  click,
  getRecordsCount,
  waitUntilHidden,
  waitUntilCount,
  isVisible
} from '../../util/utils';

const expect = chai.expect;

describe('DepartmentCode e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let departmentCodeComponentsPage: DepartmentCodeComponentsPage;
  let departmentCodeUpdatePage: DepartmentCodeUpdatePage;
  let departmentCodeDeleteDialog: DepartmentCodeDeleteDialog;
  let beforeRecordsCount = 0;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.waitUntilDisplayed();

    await signInPage.username.sendKeys('admin');
    await signInPage.password.sendKeys('admin');
    await signInPage.loginButton.click();
    await signInPage.waitUntilHidden();
    await waitUntilDisplayed(navBarPage.entityMenu);
    await waitUntilDisplayed(navBarPage.adminMenu);
    await waitUntilDisplayed(navBarPage.accountMenu);
  });

  it('should load DepartmentCodes', async () => {
    await navBarPage.getEntityPage('department-code');
    departmentCodeComponentsPage = new DepartmentCodeComponentsPage();
    expect(await departmentCodeComponentsPage.title.getText()).to.match(/Department Codes/);

    expect(await departmentCodeComponentsPage.createButton.isEnabled()).to.be.true;
    await waitUntilAnyDisplayed([departmentCodeComponentsPage.noRecords, departmentCodeComponentsPage.table]);

    beforeRecordsCount = (await isVisible(departmentCodeComponentsPage.noRecords))
      ? 0
      : await getRecordsCount(departmentCodeComponentsPage.table);
  });

  it('should load create DepartmentCode page', async () => {
    await departmentCodeComponentsPage.createButton.click();
    departmentCodeUpdatePage = new DepartmentCodeUpdatePage();
    expect(await departmentCodeUpdatePage.getPageTitle().getText()).to.match(/Create or edit a DepartmentCode/);
    await departmentCodeUpdatePage.cancel();
  });

  it('should create and save DepartmentCodes', async () => {
    await departmentCodeComponentsPage.createButton.click();
    await departmentCodeUpdatePage.setCodeInput('code');
    expect(await departmentCodeUpdatePage.getCodeInput()).to.match(/code/);
    await departmentCodeUpdatePage.setCommentInput('comment');
    expect(await departmentCodeUpdatePage.getCommentInput()).to.match(/comment/);
    await waitUntilDisplayed(departmentCodeUpdatePage.saveButton);
    await departmentCodeUpdatePage.save();
    await waitUntilHidden(departmentCodeUpdatePage.saveButton);
    expect(await isVisible(departmentCodeUpdatePage.saveButton)).to.be.false;

    expect(await departmentCodeComponentsPage.createButton.isEnabled()).to.be.true;

    await waitUntilDisplayed(departmentCodeComponentsPage.table);

    await waitUntilCount(departmentCodeComponentsPage.records, beforeRecordsCount + 1);
    expect(await departmentCodeComponentsPage.records.count()).to.eq(beforeRecordsCount + 1);
  });

  it('should delete last DepartmentCode', async () => {
    const deleteButton = departmentCodeComponentsPage.getDeleteButton(departmentCodeComponentsPage.records.last());
    await click(deleteButton);

    departmentCodeDeleteDialog = new DepartmentCodeDeleteDialog();
    await waitUntilDisplayed(departmentCodeDeleteDialog.deleteModal);
    expect(await departmentCodeDeleteDialog.getDialogTitle().getAttribute('id')).to.match(/iplantNestApp.departmentCode.delete.question/);
    await departmentCodeDeleteDialog.clickOnConfirmButton();

    await waitUntilHidden(departmentCodeDeleteDialog.deleteModal);

    expect(await isVisible(departmentCodeDeleteDialog.deleteModal)).to.be.false;

    await waitUntilAnyDisplayed([departmentCodeComponentsPage.noRecords, departmentCodeComponentsPage.table]);

    const afterCount = (await isVisible(departmentCodeComponentsPage.noRecords))
      ? 0
      : await getRecordsCount(departmentCodeComponentsPage.table);
    expect(afterCount).to.eq(beforeRecordsCount);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
