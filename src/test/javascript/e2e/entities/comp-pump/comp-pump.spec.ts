import { browser, element, by } from 'protractor';

import NavBarPage from './../../page-objects/navbar-page';
import SignInPage from './../../page-objects/signin-page';
import CompPumpComponentsPage, { CompPumpDeleteDialog } from './comp-pump.page-object';
import CompPumpUpdatePage from './comp-pump-update.page-object';
import {
  waitUntilDisplayed,
  waitUntilAnyDisplayed,
  click,
  getRecordsCount,
  waitUntilHidden,
  waitUntilCount,
  isVisible
} from '../../util/utils';

const expect = chai.expect;

describe('CompPump e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let compPumpComponentsPage: CompPumpComponentsPage;
  let compPumpUpdatePage: CompPumpUpdatePage;
  let compPumpDeleteDialog: CompPumpDeleteDialog;
  let beforeRecordsCount = 0;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.waitUntilDisplayed();

    await signInPage.username.sendKeys('admin');
    await signInPage.password.sendKeys('admin');
    await signInPage.loginButton.click();
    await signInPage.waitUntilHidden();
    await waitUntilDisplayed(navBarPage.entityMenu);
    await waitUntilDisplayed(navBarPage.adminMenu);
    await waitUntilDisplayed(navBarPage.accountMenu);
  });

  it('should load CompPumps', async () => {
    await navBarPage.getEntityPage('comp-pump');
    compPumpComponentsPage = new CompPumpComponentsPage();
    expect(await compPumpComponentsPage.title.getText()).to.match(/Comp Pumps/);

    expect(await compPumpComponentsPage.createButton.isEnabled()).to.be.true;
    await waitUntilAnyDisplayed([compPumpComponentsPage.noRecords, compPumpComponentsPage.table]);

    beforeRecordsCount = (await isVisible(compPumpComponentsPage.noRecords)) ? 0 : await getRecordsCount(compPumpComponentsPage.table);
  });

  it('should load create CompPump page', async () => {
    await compPumpComponentsPage.createButton.click();
    compPumpUpdatePage = new CompPumpUpdatePage();
    expect(await compPumpUpdatePage.getPageTitle().getText()).to.match(/Create or edit a CompPump/);
    await compPumpUpdatePage.cancel();
  });

  it('should create and save CompPumps', async () => {
    await compPumpComponentsPage.createButton.click();
    await compPumpUpdatePage.setDePressureInput('dePressure');
    expect(await compPumpUpdatePage.getDePressureInput()).to.match(/dePressure/);
    await compPumpUpdatePage.setOpPressureInput('opPressure');
    expect(await compPumpUpdatePage.getOpPressureInput()).to.match(/opPressure/);
    await compPumpUpdatePage.setQuantityInput('quantity');
    expect(await compPumpUpdatePage.getQuantityInput()).to.match(/quantity/);
    await compPumpUpdatePage.equipmentCodeSelectLastOption();
    await compPumpUpdatePage.plantSystemSelectLastOption();
    await waitUntilDisplayed(compPumpUpdatePage.saveButton);
    await compPumpUpdatePage.save();
    await waitUntilHidden(compPumpUpdatePage.saveButton);
    expect(await isVisible(compPumpUpdatePage.saveButton)).to.be.false;

    expect(await compPumpComponentsPage.createButton.isEnabled()).to.be.true;

    await waitUntilDisplayed(compPumpComponentsPage.table);

    await waitUntilCount(compPumpComponentsPage.records, beforeRecordsCount + 1);
    expect(await compPumpComponentsPage.records.count()).to.eq(beforeRecordsCount + 1);
  });

  it('should delete last CompPump', async () => {
    const deleteButton = compPumpComponentsPage.getDeleteButton(compPumpComponentsPage.records.last());
    await click(deleteButton);

    compPumpDeleteDialog = new CompPumpDeleteDialog();
    await waitUntilDisplayed(compPumpDeleteDialog.deleteModal);
    expect(await compPumpDeleteDialog.getDialogTitle().getAttribute('id')).to.match(/iplantNestApp.compPump.delete.question/);
    await compPumpDeleteDialog.clickOnConfirmButton();

    await waitUntilHidden(compPumpDeleteDialog.deleteModal);

    expect(await isVisible(compPumpDeleteDialog.deleteModal)).to.be.false;

    await waitUntilAnyDisplayed([compPumpComponentsPage.noRecords, compPumpComponentsPage.table]);

    const afterCount = (await isVisible(compPumpComponentsPage.noRecords)) ? 0 : await getRecordsCount(compPumpComponentsPage.table);
    expect(afterCount).to.eq(beforeRecordsCount);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
