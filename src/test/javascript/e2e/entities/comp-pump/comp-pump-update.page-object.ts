import { element, by, ElementFinder } from 'protractor';

export default class CompPumpUpdatePage {
  pageTitle: ElementFinder = element(by.id('iplantNestApp.compPump.home.createOrEditLabel'));
  saveButton: ElementFinder = element(by.id('save-entity'));
  cancelButton: ElementFinder = element(by.id('cancel-save'));
  dePressureInput: ElementFinder = element(by.css('input#comp-pump-dePressure'));
  opPressureInput: ElementFinder = element(by.css('input#comp-pump-opPressure'));
  quantityInput: ElementFinder = element(by.css('input#comp-pump-quantity'));
  equipmentCodeSelect: ElementFinder = element(by.css('select#comp-pump-equipmentCode'));
  plantSystemSelect: ElementFinder = element(by.css('select#comp-pump-plantSystem'));

  getPageTitle() {
    return this.pageTitle;
  }

  async setDePressureInput(dePressure) {
    await this.dePressureInput.sendKeys(dePressure);
  }

  async getDePressureInput() {
    return this.dePressureInput.getAttribute('value');
  }

  async setOpPressureInput(opPressure) {
    await this.opPressureInput.sendKeys(opPressure);
  }

  async getOpPressureInput() {
    return this.opPressureInput.getAttribute('value');
  }

  async setQuantityInput(quantity) {
    await this.quantityInput.sendKeys(quantity);
  }

  async getQuantityInput() {
    return this.quantityInput.getAttribute('value');
  }

  async equipmentCodeSelectLastOption() {
    await this.equipmentCodeSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async equipmentCodeSelectOption(option) {
    await this.equipmentCodeSelect.sendKeys(option);
  }

  getEquipmentCodeSelect() {
    return this.equipmentCodeSelect;
  }

  async getEquipmentCodeSelectedOption() {
    return this.equipmentCodeSelect.element(by.css('option:checked')).getText();
  }

  async plantSystemSelectLastOption() {
    await this.plantSystemSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async plantSystemSelectOption(option) {
    await this.plantSystemSelect.sendKeys(option);
  }

  getPlantSystemSelect() {
    return this.plantSystemSelect;
  }

  async getPlantSystemSelectedOption() {
    return this.plantSystemSelect.element(by.css('option:checked')).getText();
  }

  async save() {
    await this.saveButton.click();
  }

  async cancel() {
    await this.cancelButton.click();
  }

  getSaveButton() {
    return this.saveButton;
  }
}
