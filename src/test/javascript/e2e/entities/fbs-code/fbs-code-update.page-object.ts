import { element, by, ElementFinder } from 'protractor';

export default class FbsCodeUpdatePage {
  pageTitle: ElementFinder = element(by.id('iplantNestApp.fbsCode.home.createOrEditLabel'));
  saveButton: ElementFinder = element(by.id('save-entity'));
  cancelButton: ElementFinder = element(by.id('cancel-save'));
  codeInput: ElementFinder = element(by.css('input#fbs-code-code'));
  fbsTypeInput: ElementFinder = element(by.css('input#fbs-code-fbsType'));
  commentInput: ElementFinder = element(by.css('textarea#fbs-code-comment'));

  getPageTitle() {
    return this.pageTitle;
  }

  async setCodeInput(code) {
    await this.codeInput.sendKeys(code);
  }

  async getCodeInput() {
    return this.codeInput.getAttribute('value');
  }

  async setFbsTypeInput(fbsType) {
    await this.fbsTypeInput.sendKeys(fbsType);
  }

  async getFbsTypeInput() {
    return this.fbsTypeInput.getAttribute('value');
  }

  async setCommentInput(comment) {
    await this.commentInput.sendKeys(comment);
  }

  async getCommentInput() {
    return this.commentInput.getAttribute('value');
  }

  async save() {
    await this.saveButton.click();
  }

  async cancel() {
    await this.cancelButton.click();
  }

  getSaveButton() {
    return this.saveButton;
  }
}
