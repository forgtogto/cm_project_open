import { browser, element, by } from 'protractor';

import NavBarPage from './../../page-objects/navbar-page';
import SignInPage from './../../page-objects/signin-page';
import FbsCodeComponentsPage, { FbsCodeDeleteDialog } from './fbs-code.page-object';
import FbsCodeUpdatePage from './fbs-code-update.page-object';
import {
  waitUntilDisplayed,
  waitUntilAnyDisplayed,
  click,
  getRecordsCount,
  waitUntilHidden,
  waitUntilCount,
  isVisible
} from '../../util/utils';

const expect = chai.expect;

describe('FbsCode e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let fbsCodeComponentsPage: FbsCodeComponentsPage;
  let fbsCodeUpdatePage: FbsCodeUpdatePage;
  let fbsCodeDeleteDialog: FbsCodeDeleteDialog;
  let beforeRecordsCount = 0;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.waitUntilDisplayed();

    await signInPage.username.sendKeys('admin');
    await signInPage.password.sendKeys('admin');
    await signInPage.loginButton.click();
    await signInPage.waitUntilHidden();
    await waitUntilDisplayed(navBarPage.entityMenu);
    await waitUntilDisplayed(navBarPage.adminMenu);
    await waitUntilDisplayed(navBarPage.accountMenu);
  });

  it('should load FbsCodes', async () => {
    await navBarPage.getEntityPage('fbs-code');
    fbsCodeComponentsPage = new FbsCodeComponentsPage();
    expect(await fbsCodeComponentsPage.title.getText()).to.match(/Fbs Codes/);

    expect(await fbsCodeComponentsPage.createButton.isEnabled()).to.be.true;
    await waitUntilAnyDisplayed([fbsCodeComponentsPage.noRecords, fbsCodeComponentsPage.table]);

    beforeRecordsCount = (await isVisible(fbsCodeComponentsPage.noRecords)) ? 0 : await getRecordsCount(fbsCodeComponentsPage.table);
  });

  it('should load create FbsCode page', async () => {
    await fbsCodeComponentsPage.createButton.click();
    fbsCodeUpdatePage = new FbsCodeUpdatePage();
    expect(await fbsCodeUpdatePage.getPageTitle().getText()).to.match(/Create or edit a FbsCode/);
    await fbsCodeUpdatePage.cancel();
  });

  it('should create and save FbsCodes', async () => {
    await fbsCodeComponentsPage.createButton.click();
    await fbsCodeUpdatePage.setCodeInput('code');
    expect(await fbsCodeUpdatePage.getCodeInput()).to.match(/code/);
    await fbsCodeUpdatePage.setFbsTypeInput('fbsType');
    expect(await fbsCodeUpdatePage.getFbsTypeInput()).to.match(/fbsType/);
    await fbsCodeUpdatePage.setCommentInput('comment');
    expect(await fbsCodeUpdatePage.getCommentInput()).to.match(/comment/);
    await waitUntilDisplayed(fbsCodeUpdatePage.saveButton);
    await fbsCodeUpdatePage.save();
    await waitUntilHidden(fbsCodeUpdatePage.saveButton);
    expect(await isVisible(fbsCodeUpdatePage.saveButton)).to.be.false;

    expect(await fbsCodeComponentsPage.createButton.isEnabled()).to.be.true;

    await waitUntilDisplayed(fbsCodeComponentsPage.table);

    await waitUntilCount(fbsCodeComponentsPage.records, beforeRecordsCount + 1);
    expect(await fbsCodeComponentsPage.records.count()).to.eq(beforeRecordsCount + 1);
  });

  it('should delete last FbsCode', async () => {
    const deleteButton = fbsCodeComponentsPage.getDeleteButton(fbsCodeComponentsPage.records.last());
    await click(deleteButton);

    fbsCodeDeleteDialog = new FbsCodeDeleteDialog();
    await waitUntilDisplayed(fbsCodeDeleteDialog.deleteModal);
    expect(await fbsCodeDeleteDialog.getDialogTitle().getAttribute('id')).to.match(/iplantNestApp.fbsCode.delete.question/);
    await fbsCodeDeleteDialog.clickOnConfirmButton();

    await waitUntilHidden(fbsCodeDeleteDialog.deleteModal);

    expect(await isVisible(fbsCodeDeleteDialog.deleteModal)).to.be.false;

    await waitUntilAnyDisplayed([fbsCodeComponentsPage.noRecords, fbsCodeComponentsPage.table]);

    const afterCount = (await isVisible(fbsCodeComponentsPage.noRecords)) ? 0 : await getRecordsCount(fbsCodeComponentsPage.table);
    expect(afterCount).to.eq(beforeRecordsCount);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
