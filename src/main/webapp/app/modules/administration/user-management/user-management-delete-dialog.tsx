import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { RouteComponentProps } from 'react-router-dom';
import { Button, Modal, ModalBody, ModalFooter, ModalHeader } from 'reactstrap';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { deleteUser, getUser } from './user-management.reducer';
import { IRootState } from 'app/shared/reducers';

export interface IUserManagementDeleteDialogProps extends StateProps, DispatchProps, RouteComponentProps<{ login: string }> {}

export const UserManagementDeleteDialog = (props: IUserManagementDeleteDialogProps) => {
  useEffect(() => {
    props.getUser(props.match.params.login);
  }, []);

  const handleClose = event => {
    event.stopPropagation();
    props.history.push('/admin/user-management');
  };

  const confirmDelete = event => {
    props.deleteUser(props.user.login);
    handleClose(event);
  };

  const { user } = props;

  return (
    <Modal isOpen toggle={handleClose}>
        <div className="modal-header">
            <h5 className="modal-title" id="exampleModalLabel">
                Delete? Really?
            </h5>
            <button onClick={handleClose} type="button" className="btn btn btn-link">
                <FontAwesomeIcon icon="times-circle" />
            </button>
        </div>
        <ModalBody id="iplantNestApp.plants.delete.question">Are you sure you want to delete this User?</ModalBody>
        <ModalFooter>
            <button type="button" className="btn bg-gradient-dark btn-sm" onClick={handleClose} data-bs-dismiss="modal">
                <FontAwesomeIcon icon="ban" /> &nbsp; Cancel
            </button>
            <button type="button" className="btn bg-gradient-primary btn-sm" onClick={confirmDelete}>
                <FontAwesomeIcon icon="trash" />
                &nbsp; Delete
            </button>
        </ModalFooter>
    </Modal>
  );
};

const mapStateToProps = (storeState: IRootState) => ({
  user: storeState.userManagement.user
});

const mapDispatchToProps = { getUser, deleteUser };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(UserManagementDeleteDialog);
