import React, {useEffect} from 'react';
import {connect} from 'react-redux';
import {Link, RouteComponentProps} from 'react-router-dom';
import {Badge} from 'reactstrap';
import {TextFormat} from 'react-jhipster';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';

import {APP_DATE_FORMAT} from 'app/config/constants';

import {getUser} from './user-management.reducer';
import {IRootState} from 'app/shared/reducers';

export interface IUserManagementDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ login: string }> {
}

export const UserManagementDetail = (props: IUserManagementDetailProps) => {
    useEffect(() => {
        props.getUser(props.match.params.login);
    }, []);

    const {user} = props;

    return (
        <section>
            <div className='container py-4 mt-6'>
                <div className='row'>
                    <div className='col-lg-12 mx-auto d-flex justify-content-center flex-column'>
                        <div className='card d-flex justify-content-center p-4 shadow-lg'>
                            <div className=' row'>
                                <div className='col-lg-8 text-left'>
                                    <h3 className='text-gradient text-primary'>User Details</h3>
                                    <p className='mb-0 text-sm'>이 페이지는 {user.login} 계정의 세부정보를 확인 할 수 있습니다.</p>
                                    <br/>
                                </div>
                                <hr/>
                                <nav>
                                    <div className='nav nav-tabs text-sm' id='nav-tab' role='tablist'>
                                        <button className='nav-link active text-primary font-weight-bold'
                                                id='nav-home-tab' data-bs-toggle='tab'
                                                data-bs-target='#nav-home' type='button' role='tab'
                                                aria-controls='nav-home' aria-selected='true'>Details
                                        </button>
                                        <button className='nav-link text-primary font-weight-bold' id='nav-profile-tab'
                                                data-bs-toggle='tab'
                                                data-bs-target='#nav-profile' type='button' role='tab'
                                                aria-controls='nav-profile' aria-selected='false'>History
                                        </button>
                                        <button className='nav-link text-primary font-weight-bold' id='nav-contact-tab'
                                                data-bs-toggle='tab'
                                                data-bs-target='#nav-contact' type='button' role='tab'
                                                aria-controls='nav-contact' aria-selected='false'>Etc.
                                        </button>
                                    </div>
                                </nav>
                                <div className='tab-content' id='nav-tabContent'>
                                    <div className='tab-pane fade show active' id='nav-home' role='tabpanel'
                                         aria-labelledby='nav-home-tab'>

                                        <div className='card card-plain pt-lg-3 text-sm'>
                                            <dl className='p-3'>
                                                <dt>
                                                    <span id='comment'>아이디</span>
                                                </dt>
                                                <dd>
                                                    <span>{user.login}</span>&nbsp;
                                                    {user.activated ? <Badge color='success'>Activated</Badge> :
                                                        <Badge color='danger'>Deactivated</Badge>}
                                                </dd>
                                                <dt>
                                                    <span id='name'>이름</span>
                                                </dt>
                                                <dd>{user.name}</dd>
                                                <dt>
                                                    <span id='address'>주소</span>
                                                </dt>
                                                <dd>{user.address}</dd>
                                                <dt>
                                                    <span id='birth'>생년월일</span>
                                                </dt>
                                                <dd>{user.birth}</dd>
                                                <dt>
                                                    <span id='phone'>연락처</span>
                                                </dt>
                                                <dd>{user.phone}</dd>
                                                <dt>
                                                    <span id='department'>부서</span>
                                                </dt>
                                                <dd>{user.department}</dd>
                                                <dt>
                                                    <span id='email'>이메일</span>
                                                </dt>
                                                <dd>{user.email}</dd>

                                            </dl>
                                        </div>
                                    </div>
                                    <div className='tab-pane fade' id='nav-profile' role='tabpanel'
                                         aria-labelledby='nav-profile-tab'>

                                        <div className='card card-plain pt-lg-3 text-sm'>
                                            <dl className='p-3'>
                                                <dt>
                                                    <span id='createdBy'>Created By</span>
                                                </dt>
                                                <dd>{user.createdBy}</dd>
                                                <dt>
                                                    <span id='createdDate'>created Date</span>
                                                </dt>
                                                <dd>
                                                    <TextFormat value={user.createdDate} type='date'
                                                                format={APP_DATE_FORMAT} blankOnInvalid/>
                                                </dd>
                                                <dt>
                                                    <span id='lastModifiedBy'>Last Modified By</span>
                                                </dt>
                                                <dd>{user.lastModifiedBy}</dd>
                                                <dt>
                                                    <span id='lastModifiedDate'>Last Modified Date</span>
                                                </dt>
                                                <dd>
                                                    <TextFormat value={user.lastModifiedDate} type='date'
                                                                format={APP_DATE_FORMAT} blankOnInvalid/>
                                                </dd>
                                                <dt>Profiles</dt>
                                                <dd>
                                                    <ul className='list-unstyled'>
                                                        {user.authorities
                                                            ? user.authorities.map((authority, i) => (
                                                                <li key={`user-auth-${i}`}>
                                                                    <Badge color='info'>{authority}</Badge>
                                                                </li>
                                                            ))
                                                            : null}
                                                    </ul>
                                                </dd>
                                            </dl>
                                        </div>
                                    </div>
                                    <div className='tab-pane fade' id='nav-contact' role='tabpanel'
                                         aria-labelledby='nav-contact-tab'>
                                        <div className='p-3 text-sm'>
                                            TDB
                                        </div>
                                    </div>
                                </div>

                                <div className='pt-lg-3'>
                                    <Link to='/admin/user-management' type='button'
                                          className='btn bg-gradient-light btn-sm '>
                                        <FontAwesomeIcon icon='arrow-left'/> Back
                                    </Link>
                                    &nbsp;
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    );
};

const mapStateToProps = (storeState: IRootState) => ({
    user: storeState.userManagement.user
});

const mapDispatchToProps = {getUser};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(UserManagementDetail);
