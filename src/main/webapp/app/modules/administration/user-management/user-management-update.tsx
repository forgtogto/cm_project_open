import React, {useEffect, useState} from 'react';
import {connect} from 'react-redux';
import {Link, RouteComponentProps} from 'react-router-dom';
import {Button, Col, Label, Row} from 'reactstrap';
import {AvField, AvForm, AvGroup, AvInput} from 'availity-reactstrap-validation';

import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';

import {createUser, getRoles, getUser, reset, updateUser} from './user-management.reducer';
import {IRootState} from 'app/shared/reducers';

export interface IUserManagementUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ login: string }> {
}

export const UserManagementUpdate = (props: IUserManagementUpdateProps) => {
    const [isNew, setIsNew] = useState(!props.match.params || !props.match.params.login);

    useEffect(() => {
        if (isNew) {
            props.reset();
        } else {
            props.getUser(props.match.params.login);
        }
        props.getRoles();
        return () => props.reset();
    }, []);

    const handleClose = () => {
        props.history.push('/admin/user-management');
    };

    const saveUser = (event, values) => {
        if (isNew) {
            props.createUser(values);
        } else {
            props.updateUser(values);
        }
        handleClose();
    };

    const isInvalid = false;
    const {user, loading, updating, roles} = props;

    return (

        <section>
            <div className="container py-4 mt-6">
                <div className="row">
                    <div className="col-lg-12 mx-auto d-flex justify-content-center flex-column">
                        <div className="card d-flex justify-content-center p-4 shadow-lg">
                            <div className=" row">
                                <div className="col-lg-8 text-left">
                                    <h3 className="text-gradient text-primary">User</h3>
                                    <p className="mb-0 text-sm">이 페이지는 User를 생성 및 수정 할 수 있습니다.</p>
                                    <br/>
                                </div>
                                <hr/>
                                <div className="card card-plain">
                                    {loading ? (
                                        <p>Loading...</p>
                                    ) : (
                                        <AvForm model={isNew ? {} : user} onValidSubmit={saveUser}>

                                            {/*
                          {!isNew ? (
                            <AvGroup>
                              <Label for="plants-id">ID</Label>
                              <AvInput id="plants-id" type="text" className="form-control" name="id" required readOnly />
                            </AvGroup>
                          ) : null}
                         */}
                                            <AvGroup>
                                                <Label id="codeLabel" for="login">
                                                    아이디
                                                </Label>
                                                <AvField
                                                    type="text"
                                                    className="form-control"
                                                    name="login"
                                                    validate={{
                                                        required: {
                                                            value: true,
                                                            errorMessage: 'Your username is required.'
                                                        },
                                                        pattern: {
                                                            value: '^[_.@A-Za-z0-9-]*$',
                                                            errorMessage: 'Your username can only contain letters and digits.'
                                                        },
                                                        minLength: {
                                                            value: 1,
                                                            errorMessage: 'Your username is required to be at least 1 character.'
                                                        },
                                                        maxLength: {
                                                            value: 50,
                                                            errorMessage: 'Your username cannot be longer than 50 characters.'
                                                        }
                                                    }}
                                                    value={user.login}
                                                />
                                            </AvGroup>
                                            <AvGroup>
                                                <Label id="codeLabel" for="name">
                                                    이름
                                                </Label>
                                                <AvField
                                                    type="text"
                                                    className="form-control"
                                                    name="name"
                                                    validate={{
                                                        maxLength: {
                                                            value: 50,
                                                            errorMessage: 'This field cannot be longer than 50 characters.'
                                                        }
                                                    }}
                                                    value={user.name}
                                                />
                                            </AvGroup>
                                            <AvGroup>
                                                <Label id="codeLabel" for="email">
                                                    이메일
                                                </Label>
                                                <AvField
                                                    name="email"
                                                    className="form-control"
                                                    type="email"
                                                    validate={{
                                                        required: {
                                                            value: true,
                                                            errorMessage: 'Your email is required.'
                                                        },
                                                        email: {
                                                            errorMessage: 'Your email is invalid.'
                                                        },
                                                        minLength: {
                                                            value: 5,
                                                            errorMessage: 'Your email is required to be at least 5 characters.'
                                                        },
                                                        maxLength: {
                                                            value: 254,
                                                            errorMessage: 'Your email cannot be longer than 50 characters.'
                                                        }
                                                    }}
                                                    value={user.email}
                                                />
                                            </AvGroup>

                                            <AvGroup>
                                                <Label for="authorities">권한</Label>
                                                <AvInput type="select" className="form-control" name="authorities"
                                                         value={user.authorities} multiple>
                                                    {roles.map(role => (
                                                        <option value={role} key={role}>
                                                            {role}
                                                        </option>
                                                    ))}
                                                </AvInput>
                                            </AvGroup>
                                            <AvGroup check>
                                                <Label>
                                                    <AvInput type="checkbox" name="activated" value={user.activated}
                                                             checked={user.activated} disabled={!user.id}/> 활성화 여부
                                                </Label>

                                            </AvGroup>
                                            <br/>

                                            <Button className="btn bg-gradient-light btn-sm" tag={Link}
                                                    id="cancel-save" to="/admin/user-management" replace>
                                                <FontAwesomeIcon icon="arrow-left"/>
                                                &nbsp;
                                                <span className="d-none d-md-inline">Back</span>
                                            </Button>
                                            &nbsp;
                                            <Button className="btn bg-gradient-dark btn-sm" type="submit"
                                                    disabled={isInvalid || updating}>
                                                <FontAwesomeIcon icon="save"/>
                                                &nbsp; Save
                                            </Button>
                                        </AvForm>
                                    )}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    );
};

const mapStateToProps = (storeState: IRootState) => ({
    user: storeState.userManagement.user,
    roles: storeState.userManagement.authorities,
    loading: storeState.userManagement.loading,
    updating: storeState.userManagement.updating
});

const mapDispatchToProps = {getUser, getRoles, updateUser, createUser, reset};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(UserManagementUpdate);
