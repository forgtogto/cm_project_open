import React, {useState, useEffect} from 'react';
import {connect} from 'react-redux';
import {Link, RouteComponentProps} from 'react-router-dom';
import {Button, Table, Row, Badge} from 'reactstrap';
import {TextFormat, JhiPagination, JhiItemCount, getSortState} from 'react-jhipster';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';

import {APP_DATE_FORMAT} from 'app/config/constants';
import {ITEMS_PER_PAGE} from 'app/shared/util/pagination.constants';
import {getUsers, updateUser} from './user-management.reducer';
import {IRootState} from 'app/shared/reducers';

export interface IUserManagementProps extends StateProps, DispatchProps, RouteComponentProps<{}> {
}

export const UserManagement = (props: IUserManagementProps) => {
    const [pagination, setPagination] = useState(getSortState(props.location, ITEMS_PER_PAGE));

    useEffect(() => {
        props.getUsers(pagination.activePage - 1, pagination.itemsPerPage, `${pagination.sort},${pagination.order}`);
        props.history.push(`${props.location.pathname}?page=${pagination.activePage}&sort=${pagination.sort},${pagination.order}`);
    }, [pagination]);

    const sort = p => () =>
        setPagination({
            ...pagination,
            order: pagination.order === 'asc' ? 'desc' : 'asc',
            sort: p
        });

    const handlePagination = currentPage =>
        setPagination({
            ...pagination,
            activePage: currentPage
        });

    const toggleActive = user => () =>
        props.updateUser({
            ...user,
            activated: !user.activated
        });

    const {users, account, match, totalItems} = props;
    return (

        <section>
            <div className="container py-4 mt-6">
                <div className="row">
                    <div className="col-lg-12 mx-auto d-flex justify-content-center flex-column">
                        <div className="card d-flex justify-content-center p-4 shadow-lg">
                            <div className=" row">
                                <div className="col-lg-8 text-left">
                                    <h3 className="text-gradient text-primary">Users</h3>
                                    <p className="mb-0 text-sm">이곳엔 사용자 관리의 내용을 볼 수 있습니다.</p>
                                    <br/>
                                </div>
                                <div className="col-lg-4 pt-2 text-lg-right">
                                    <Link to={`${match.url}/new`}
                                          className="btn btn-primary float-right jh-create-entity "
                                          id="jh-create-entity">
                                        <FontAwesomeIcon icon="plus"/>
                                        &nbsp; Create a new user
                                    </Link>
                                </div>
                                <hr/>
                            </div>
                            <div className="card card-plain">
                                <form role="form" id="contact-form" method="post" autoComplete="off">
                                    <div className="card-body pb-2 pt-lg-0">
                                        <div className="table-responsive">
                                            <table className="table">
                                                <thead>
                                                <tr>
                                                    <th className="text-center text-uppercase text-secondary  text-sm font-weight-bolder opacity-7 col-1"
                                                        onClick={sort('login')}>
                                                        Login
                                                        <FontAwesomeIcon icon="sort"/>
                                                    </th>
                                                    <th className="text-center text-uppercase text-secondary  text-sm font-weight-bolder opacity-7 col-1"
                                                        onClick={sort('email')}>
                                                        Email
                                                        <FontAwesomeIcon icon="sort"/>
                                                    </th>
                                                    <th/>
                                                    <th className="text-center text-uppercase text-secondary  text-sm font-weight-bolder opacity-7 col-1">Profiles</th>

                                                    <th className="text-center text-uppercase text-secondary  text-sm font-weight-bolder opacity-7 col-2"
                                                        onClick={sort('lastModifiedBy')}>
                                                        Last Modified By
                                                        <FontAwesomeIcon icon="sort"/>
                                                    </th>

                                                    <th/>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                {users.map((user, i) => (
                                                    <tr id={user.login} key={`user-${i}`}>
                                                        <td  className="align-middle text-center text-sm">{user.login}</td>
                                                        <td  className="align-middle text-left text-sm">{user.email}</td>
                                                        <td  className="align-middle text-center text-sm">
                                                            {user.activated ? (
                                                                <button type="button"
                                                                        className="btn bg-gradient-success btn-sm mb-0"
                                                                        onClick={toggleActive(user)}>
                                                                    {' '}
                                                                    Activated{' '}
                                                                </button>
                                                            ) : (
                                                                <button type="button"
                                                                        className="btn bg-gradient-warning btn-sm mb-0"
                                                                        onClick={toggleActive(user)}>
                                                                    {' '}
                                                                    Deactivated{' '}
                                                                </button>
                                                            )}
                                                        </td>
                                                        <td  className="align-middle text-left text-sm">
                                                            {user.authorities
                                                                ? user.authorities.map((authority, j) => <div
                                                                    key={`user-auth-${i}-${j}`}>{authority}</div>)
                                                                : null}
                                                        </td>
                                                        <td>
                                                            <TextFormat value={user.lastModifiedDate} type="date"
                                                                        format={APP_DATE_FORMAT} blankOnInvalid/>
                                                        </td>
                                                        <td className="text-right">
                                                            <div
                                                                className="btn-group flex-btn-group-container w-auto me-2 ">
                                                                <Link to={`${match.url}/${user.login}`} type="button"
                                                                      className="btn bg-gradient-secondary btn-sm mb-0">
                                                                    <FontAwesomeIcon icon="eye"/> View
                                                                </Link>
                                                                <Link
                                                                    to={`${match.url}/${user.login}/edit`}
                                                                    type="button"
                                                                    className="btn bg-gradient-light btn-sm mb-0"
                                                                >
                                                                    <FontAwesomeIcon icon="pencil-alt"/> Edit
                                                                </Link>
                                                                {account.login !== user.login ? (
                                                                    <Link
                                                                        to={`${match.url}/${user.login}/delete`}
                                                                        type="button"
                                                                        className="btn bg-gradient-dark btn-sm mb-0"
                                                                    >
                                                                        <FontAwesomeIcon icon="trash"/> Delete
                                                                    </Link>
                                                                ) : (
                                                                    <button type="button"
                                                                            className="btn bg-gradient-dark btn-sm mb-0"
                                                                            disabled>
                                                                        <FontAwesomeIcon icon="trash"/> Delete
                                                                    </button>
                                                                )}
                                                            </div>
                                                        </td>
                                                    </tr>
                                                ))}
                                                </tbody>
                                            </table>
                                        </div>
                                        <div className="text-sm align-items-center">
                                            <div className={users && users.length > 0 ? '' : 'd-none'}>
                                                <JhiItemCount
                                                    page={pagination.activePage}
                                                    total={totalItems}
                                                    itemsPerPage={pagination.itemsPerPage}
                                                    i18nEnabled
                                                />
                                                <JhiPagination
                                                    activePage={pagination.activePage}
                                                    onSelect={handlePagination}
                                                    maxButtons={5}
                                                    itemsPerPage={pagination.itemsPerPage}
                                                    totalItems={props.totalItems}
                                                />
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    );
};

const mapStateToProps = (storeState: IRootState) => ({
    users: storeState.userManagement.users,
    totalItems: storeState.userManagement.totalItems,
    account: storeState.authentication.account
});

const mapDispatchToProps = {getUsers, updateUser};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(UserManagement);
