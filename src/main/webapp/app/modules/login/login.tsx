import React from 'react';
import { connect } from 'react-redux';
import { Link, Redirect, RouteComponentProps } from 'react-router-dom';
import { AvField, AvForm, AvInput } from 'availity-reactstrap-validation';

import { IRootState } from 'app/shared/reducers';
import { login } from 'app/shared/reducers/authentication';

export interface ILoginProps extends StateProps, DispatchProps, RouteComponentProps<{}> {}

export const Login = (props: ILoginProps) => {
  const handleLogin = (username, password, rememberMe = false) => props.login(username, password, rememberMe);

  const { location, isAuthenticated } = props;

  const { from } = (location.state as any) || { from: { pathname: '/', search: location.search } };
  if (isAuthenticated) {
    return <Redirect to={from} />;
  }
  const handleSubmit = (event, errors, { username, password, rememberMe }) => {
    handleLogin(username, password, rememberMe);
  };
  return (
    <section>
      <div className="page-header section-height-100">
        <div className="container">
          <img
            className="position-absolute fixed-top ms-auto w-50 h-100 z-index-0 d-none d-sm-none d-md-block border-radius-section border-top-end-radius-0 border-top-start-radius-0 border-bottom-end-radius-0"
            src="../../../content/img/nastuh.jpg"
          />
          <div className="row">
            <div className="col-xl-4 col-lg-5 col-md-7 d-flex flex-column mx-lg-0 mx-auto">
              <div className="card card-plain">
                <div className="card-header pb-0 text-left">
                  <h4 className="font-weight-bolder text-primary">Sign In</h4>
                  <p className="mb-0">Enter your email and password to sign in</p>
                </div>
                <div className="card-body">
                  <AvForm onSubmit={handleSubmit}>
                    {props.loginError ? (
                      <div className="alert alert-danger text-white font-weight-bold" role="alert">
                        <strong>Failed to sign in!</strong> Please check your credentials and try again.
                      </div>
                    ) : null}
                    <AvField
                      className="form-control form-control-lg"
                      name="username"
                      label="Username"
                      placeholder="Your username"
                      required
                      errorMessage="Username cannot be empty!"
                      autoFocus
                    />
                    <AvField
                      className="form-control form-control-lg"
                      name="password"
                      type="password"
                      label="Password"
                      placeholder="Your password"
                      required
                      errorMessage="Password cannot be empty!"
                    />

                    <div className="form-check form-switch">
                      <AvInput className="form-check-input" type="checkbox" name="rememberMe" />
                      <label className="form-check-label" htmlFor="rememberMe">
                        Remember me
                      </label>
                    </div>

                    <div className="text-center">
                      <button type="submit" className="btn btn-lg bg-gradient-primary btn-lg w-100 mt-4 mb-0">
                        Sign in
                      </button>
                    </div>
                    <br />
                    <div className="card-footer text-center pt-0 px-lg-2 px-1">
                      <p className="mb-0 text-sm mx-auto">
                        Dont have an account?
                        <Link to="/account/register" className="text-primary text-gradient font-weight-bold">
                          &nbsp;Sign up
                        </Link>
                      </p>
                    </div>
                    <div className="card-footer text-center pt-0 px-lg-2 px-1">
                      <p className="mb-4 text-sm mx-auto">
                        Did you forget your password?
                        <Link to="/account/reset/request" className="text-primary text-gradient font-weight-bold">
                          &nbsp;Click here!
                        </Link>
                      </p>
                    </div>
                  </AvForm>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

const mapStateToProps = ({ authentication }: IRootState) => ({
  isAuthenticated: authentication.isAuthenticated,
  loginError: authentication.loginError
});

const mapDispatchToProps = { login };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(Login);
