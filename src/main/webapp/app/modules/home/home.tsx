import React from 'react';
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';

export type IHomeProp = StateProps;

export const Home = (props: IHomeProp) => {
    const {account} = props;
    const backgroundImage = `url(../../../../content/img/curved-images/curved11.jpg)`;

    return (
        <header>
            <div className="page-header section-height-100">
                <div className="oblique position-absolute top-0 h-100 d-md-block d-none">
                    <div className="oblique-image bg-cover position-absolute fixed-top ms-auto h-100 z-index-0 ms-n6 "
                         style={{backgroundImage}}/>
                </div>
                <div className="container-fluid m-lg-5 ">
                    <div className="row">
                        <div className="col-lg-6 col-md-7 d-flex justify-content-center flex-column">
                            <h1 className="text-gradient text-primary">CM Prototype System</h1>
                            <h2 className="mb-4"> Jeongkwon Seo | Portfolio</h2>
                            <h4> 설명: CM(Configuration & Management) 관리를 위한 <br/> 프로젝트로서
                                항목들의 관계 설정 및 히스토리 관리에 대한 서비스를 제공합니다.
                            </h4>
                            <h5>
                                <hr/>
                                <br/>Back end : NestJS로 서버 구성을 하였습니다.
                                <br/>Front end : 최신 Bootstrap5를 React에 적용하였습니다.
                                <br/>Skills: Jhipster, NestJS, React, Bootstrap5
                            </h5>
                            <hr/>
                            {account && account.login ? (
                                <div>
                                    <p className="lead pe-5 me-5">{account.login}님 로그인 하신 것을 환영합니다.</p>
                                </div>
                            ) : (
                                <div>
                                    <p className="lead pe-5 me-5">환영합니다. 로그인 이후 사용 할 수 있습니다. </p>
                                    <div className="buttons">
                                        <Link to={'/login'} type="button" className="btn bg-gradient-primary mt-4">
                                            Login
                                        </Link>
                                    </div>
                                </div>
                            )}
                        </div>
                    </div>
                </div>
            </div>
        </header>
    );
};

const mapStateToProps = storeState => ({
    account: storeState.authentication.account,
    isAuthenticated: storeState.authentication.isAuthenticated
});

type StateProps = ReturnType<typeof mapStateToProps>;

export default connect(mapStateToProps)(Home);
