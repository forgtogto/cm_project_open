import React, { useEffect, useState } from 'react';
import { Button } from 'reactstrap';
import { connect } from 'react-redux';

import { AvField, AvForm } from 'availity-reactstrap-validation';

import { IRootState } from 'app/shared/reducers';
import { getSession } from 'app/shared/reducers/authentication';
import { reset, saveAccountSettings } from './settings.reducer';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { RequesterGroup } from 'app/shared/model/enumerations/requester-group.model';
import { ageCal, convertBirth } from 'app/shared/util/date-utils';

import DaumPostcode from 'react-daum-postcode';

export interface IUserSettingsProps extends StateProps, DispatchProps {
}

export const SettingsPage = (props: IUserSettingsProps) => {
    useEffect(() => {
        props.getSession();
        return () => {
            props.reset();
        };
    }, []);

    const [isAddress, setIsAddress] = useState('');

    if (isAddress !== '') {
        props.account.address = isAddress;
    }

    const handleValidSubmit = (event, values) => {
        const account = {
            ...props.account,
            ...values
        };

        props.saveAccountSettings(account);
        event.persist();
    };

    const handleComplete = (data) => {
        let fullAddress = data.address;
        let extraAddress = '';

        if (data.addressType === 'R') {
            if (data.bname !== '') {
                extraAddress += data.bname;
            }
            if (data.buildingName !== '') {
                extraAddress += (extraAddress !== '' ? `, ${data.buildingName}` : data.buildingName);
            }
            fullAddress += (extraAddress !== '' ? ` (${extraAddress})` : '');

            setIsAddress(fullAddress);
        }
    };
    const getSettingForm = () => {
        return (
            <AvForm id='settings-form' onValidSubmit={handleValidSubmit}>
                <div className='row'>
                    <div className='col-lg-6'>
                        {/* Name */}
                        <AvField
                            className='form-control'
                            name='name'
                            label='사용자 이름'
                            id='name'
                            disabled
                            validate={{
                                required: { value: true, errorMessage: 'Your name is required.' },
                                minLength: {
                                    value: 1,
                                    errorMessage: 'Your name is required to be at least 1 character'
                                },
                                maxLength: { value: 50, errorMessage: 'Your name cannot be longer than 50 characters' }
                            }}
                            value={props.account.name}
                        />
                        {/* Age  */}
                        <AvField
                            className='form-control'
                            name='birthAndAge'
                            label='나이'
                            id='birth'
                            disabled
                            value={convertBirth(props.account.birth) + ' (만' + ageCal(props.account.birth) + '세)'}
                        />
                        {/* Phone  */}
                        <AvField
                            className='form-control'
                            name='phone'
                            label='연락처'
                            id='phone'
                            value={props.account.phone}
                        />
                        {/* Email */}
                        <AvField
                            name='email'
                            label='이메일'
                            placeholder={'Your email'}
                            type='email'
                            validate={{
                                required: { value: true, errorMessage: 'Your email is required.' },
                                minLength: {
                                    value: 5,
                                    errorMessage: 'Your email is required to be at least 5 characters.'
                                },
                                maxLength: {
                                    value: 254,
                                    errorMessage: 'Your email cannot be longer than 50 characters.'
                                }
                            }}
                            value={props.account.email}
                        />
                        {/* Department  */}
                        <AvField
                            label='소속 부서'
                            id='department'
                            type='select'
                            className='form-select'
                            name='department'
                            value={(props.account.department) || '건축 부서'}
                        >
                            {Object.entries(RequesterGroup).map((key, index) =>
                                <option key={key[0].valueOf()} value={key[1].valueOf()}>
                                    {key[1].valueOf()}
                                </option>
                            )}
                        </AvField>
                    </div>
                    <div className='col-lg-6'>
                        {/* Address  */}
                        <AvField
                            className='form-control'
                            name='address'
                            label='주소'
                            id='address'
                            value={props.account.address}
                        />
                        <div className='accordion accordion-flush' id='addressAccordion'>
                            <div className='accordion-item'>
                                <h2 className='accordion-header' id='flush-headingOne'>
                                    <button className=' collapsed btn bg-gradient-secondary btn-sm text-xs' type='button'
                                            data-bs-toggle='collapse'
                                            data-bs-target='#flush-collapseOne' aria-expanded='false'
                                            aria-controls='flush-collapseOne'>
                                       주소 검색
                                    </button>
                                </h2>
                                <div id='flush-collapseOne' className='accordion-collapse collapse'
                                     aria-labelledby='flush-headingOne' data-bs-parent='#addressAccordion'>
                                    <DaumPostcode
                                        onComplete={handleComplete}
                                        style={{ height: '280px'}}
                                        autoClose={true}
                                    />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <Button className='btn bg-gradient-dark btn-sm mt-4 mb-3' type='submit'>
                    <FontAwesomeIcon icon='save' />
                    &nbsp; Save
                </Button>
            </AvForm>
        );
    };

    return (
        <section>
            <div className='container py-4 mt-6'>
                <div className='row'>
                    <div className='col-lg-12 mx-auto d-flex justify-content-center flex-column'>
                        <div className='card d-flex justify-content-center p-4 shadow-lg mb-7'>
                            <div className=' row'>
                                <div className='col-lg-8 text-left'>
                                    <h4 className='text-gradient text-primary'>사용자 정보</h4>
                                    <p className='mb-0 text-sm'><span
                                        className='text-primary'>{props.account.name}</span>님의 정보를 변경하세요.</p>
                                    <br />
                                </div>
                                <hr />
                                <div className='card card-plain'>{getSettingForm()}</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    );
};

const mapStateToProps = ({ authentication }: IRootState) => ({
    account: authentication.account,
    isAuthenticated: authentication.isAuthenticated
});

const mapDispatchToProps = { getSession, saveAccountSettings, reset };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(SettingsPage);
