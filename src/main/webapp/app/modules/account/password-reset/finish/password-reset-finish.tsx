import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import { AvField, AvForm } from 'availity-reactstrap-validation';
import { getUrlParameter } from 'react-jhipster';
import { RouteComponentProps } from 'react-router-dom';

import { handlePasswordResetFinish, reset } from '../password-reset.reducer';
import PasswordStrengthBar from 'app/shared/layout/password/password-strength-bar';

export interface IPasswordResetFinishProps extends DispatchProps, RouteComponentProps<{ key: string }> {}

export const PasswordResetFinishPage = (props: IPasswordResetFinishProps) => {
  const [password, setPassword] = useState('');
  const [key] = useState(getUrlParameter('key', props.location.search));

  useEffect(() => () => props.reset(), []);

  const handleValidSubmit = (event, values) => props.handlePasswordResetFinish(key, values.newPassword);

  const updatePassword = event => setPassword(event.target.value);

  const getResetForm = () => {
    return (
      <AvForm onValidSubmit={handleValidSubmit}>
        <AvField
          className="form-control form-control-lg"
          name="newPassword"
          label="New password"
          placeholder={'New password'}
          type="password"
          validate={{
            required: { value: true, errorMessage: 'Your password is required.' },
            minLength: { value: 4, errorMessage: 'Your password is required to be at least 4 characters.' },
            maxLength: { value: 50, errorMessage: 'Your password cannot be longer than 50 characters.' }
          }}
          onChange={updatePassword}
        />
        <PasswordStrengthBar password={password} />
        <AvField
          className="form-control form-control-lg"
          name="confirmPassword"
          label="New password confirmation"
          placeholder="Confirm the new password"
          type="password"
          validate={{
            required: { value: true, errorMessage: 'Your confirmation password is required.' },
            minLength: {
              value: 4,
              errorMessage: 'Your confirmation password is required to be at least 4 characters.'
            },
            maxLength: {
              value: 50,
              errorMessage: 'Your confirmation password cannot be longer than 50 characters.'
            },
            match: { value: 'newPassword', errorMessage: 'The password and its confirmation do not match!' }
          }}
        />
        <div className="col-md-12 text-left">
          <button type="submit" className="btn bg-gradient-primary mt-3 mb-0">
            Validate new password
          </button>
        </div>
      </AvForm>
    );
  };

  return (
    <section>
      <div className="page-header section-height-100">
        <div className="container">
          <div className="row">
            <div>
              <img
                className="position-absolute fixed-top ms-auto w-40 h-100 z-index-0 d-none d-sm-none d-md-block border-radius-section border-top-end-radius-0 border-top-start-radius-0 border-bottom-end-radius-0"
                src="../../../../../content/img/curved-images/curved2.jpg"
              />
            </div>
            <div className="col-xl-4 col-lg-5 col-md-7 d-flex flex-column mx-lg-0 mx-auto">
              <div className="card card-plain">
                <div className="card-header pb-0 text-left">
                  <h4 className="font-weight-bolder text-primary">Reset password</h4>
                  <p className="mb-0">Change your password!</p>
                </div>
                <div className="card-body">{key ? getResetForm() : null}</div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

const mapDispatchToProps = { handlePasswordResetFinish, reset };

type DispatchProps = typeof mapDispatchToProps;

export default connect(null, mapDispatchToProps)(PasswordResetFinishPage);
