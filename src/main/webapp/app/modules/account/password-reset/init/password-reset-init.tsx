import React from 'react';

import { connect } from 'react-redux';
import { AvField, AvForm } from 'availity-reactstrap-validation';
import { handlePasswordResetInit, reset } from '../password-reset.reducer';

export type IPasswordResetInitProps = DispatchProps;

export class PasswordResetInit extends React.Component<IPasswordResetInitProps> {
  componentWillUnmount() {
    this.props.reset();
  }

  handleValidSubmit = (event, values) => {
    this.props.handlePasswordResetInit(values.email);
    event.preventDefault();
  };

  render() {
    return (
      <section>
        <div className="page-header section-height-100">
          <div className="container">
            <div className="row">
              <div>
                <img
                  className="position-absolute fixed-top ms-auto w-40 h-100 z-index-0 d-none d-sm-none d-md-block border-radius-section border-top-end-radius-0 border-top-start-radius-0 border-bottom-end-radius-0"
                  src="../../../../../content/img/curved-images/curved2.jpg"
                />
              </div>
              <div className="col-xl-4 col-lg-5 col-md-7 d-flex flex-column mx-lg-0 mx-auto">
                <div className="card card-plain">
                  <div className="card-header pb-0 text-left">
                    <h4 className="font-weight-bolder text-primary">Reset your password</h4>
                    <p className="mb-0">Enter the email address you used to register</p>
                  </div>
                  <div className="card-body">
                    <AvForm onValidSubmit={this.handleValidSubmit}>
                      <AvField
                        className="form-control form-control-lg"
                        name="email"
                        label="Email"
                        placeholder={'Your email'}
                        type="email"
                        validate={{
                          required: { value: true, errorMessage: 'Your email is required.' },
                          minLength: {
                            value: 5,
                            errorMessage: 'Your email is required to be at least 5 characters.'
                          },
                          maxLength: {
                            value: 254,
                            errorMessage: 'Your email cannot be longer than 50 characters.'
                          }
                        }}
                      />
                      <div className="col-md-12 text-left">
                        <button type="submit" className="btn bg-gradient-primary mt-3 mb-0">
                          Reset password
                        </button>
                      </div>
                    </AvForm>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    );
  }
}

const mapDispatchToProps = { handlePasswordResetInit, reset };

type DispatchProps = typeof mapDispatchToProps;

export default connect(null, mapDispatchToProps)(PasswordResetInit);
