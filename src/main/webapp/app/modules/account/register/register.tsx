import React, { useEffect, useState } from 'react';

import { connect } from 'react-redux';
import { AvField, AvForm } from 'availity-reactstrap-validation';

import PasswordStrengthBar from 'app/shared/layout/password/password-strength-bar';
import { handleRegister, reset } from './register.reducer';

export type IRegisterProps = DispatchProps;

export const RegisterPage = (props: IRegisterProps) => {
    const [password, setPassword] = useState('');

    useEffect(() => () => props.reset(), []);

    const handleValidSubmit = (event, values) => {
        props.handleRegister(values.username, values.email, values.name, values.birth, values.firstPassword);
        event.preventDefault();
    };

    const updatePassword = event => setPassword(event.target.value);

    const getRegisterForm = () => {
        return (
            <AvForm id='register-form' onValidSubmit={handleValidSubmit}>
                <div className='row'>
                    <div className='col-md-6'>
                        <AvField
                            className='form-control form-control-lg'
                            name='username'
                            label='로그인 아이디'
                            placeholder={'Your ID for login'}
                            validate={{
                                required: {
                                    value: true,
                                    errorMessage: 'Your login ID is required.'
                                },
                                pattern: {
                                    value: '^[_.@A-Za-z0-9-]*$',
                                    errorMessage: 'Your username can only contain letters and digits.'
                                },
                                minLength: {
                                    value: 1,
                                    errorMessage: 'Your username is required to be at least 1 character.'
                                },
                                maxLength: {
                                    value: 50,
                                    errorMessage: 'Your username cannot be longer than 50 characters.'
                                }
                            }}
                        />
                    </div>
                    <div className='col-md-6 ps-md-2'>
                        <AvField
                            className='form-control form-control-lg'
                            name='email'
                            label='이메일'
                            placeholder={'Your email'}
                            type='email'
                            validate={{
                                required: { value: true, errorMessage: 'Your email is required.' },
                                minLength: {
                                    value: 5,
                                    errorMessage: 'Your email is required to be at least 5 characters.'
                                },
                                maxLength: {
                                    value: 254,
                                    errorMessage: 'Your email cannot be longer than 50 characters.'
                                }
                            }}
                        />
                    </div>
                </div>
                <div className='row'>
                    <div className='col-md-6'>
                        <AvField
                            className='form-control form-control-lg'
                            name='name'
                            label='사용자 이름'
                            placeholder={'Your name'}
                            validate={{
                                required: { value: true, errorMessage: 'Your name is required.' },
                                minLength: {
                                    value: 1,
                                    errorMessage: 'Your name is required to be at least 1 character.'
                                },
                                maxLength: {
                                    value: 10,
                                    errorMessage: 'Your name cannot be longer than 10 characters.'
                                }
                            }}
                        />
                    </div>
                    <div className='col-md-6 ps-md-2'>
                        <AvField
                            className='form-control form-control-lg'
                            name='birth'
                            label='생년월일'
                            placeholder={'Your Birth'}
                            type='date'
                            validate={{
                                required: { value: true, errorMessage: 'Your birth is required.' }
                            }}
                        />
                    </div>
                </div>
                <AvField
                    className='form-control form-control-lg'
                    name='firstPassword'
                    label='비밀번호'
                    placeholder={'New password'}
                    type='password'
                    onChange={updatePassword}
                    validate={{
                        required: {
                            value: true,
                            errorMessage: 'Your password is required.'
                        },
                        minLength: {
                            value: 4,
                            errorMessage: 'Your password is required to be at least 4 characters.'
                        },
                        maxLength: {
                            value: 50,
                            errorMessage: 'Your password cannot be longer than 50 characters.'
                        }
                    }}
                />
                <PasswordStrengthBar password={password} />
                <AvField
                    className='form-control form-control-lg'
                    name='secondPassword'
                    label='비밀번호 확인'
                    placeholder='Confirm the new password'
                    type='password'
                    validate={{
                        required: {
                            value: true,
                            errorMessage: 'Your confirmation password is required.'
                        },
                        minLength: {
                            value: 4,
                            errorMessage: 'Your confirmation password is required to be at least 4 characters.'
                        },
                        maxLength: {
                            value: 50,
                            errorMessage: 'Your confirmation password cannot be longer than 50 characters.'
                        },
                        match: {
                            value: 'firstPassword',
                            errorMessage: 'The password and its confirmation do not match!'
                        }
                    }}
                />
                <div className='col-md-12 text-center'>
                    <button type='submit' id='register-submit'
                            className='btn bg-gradient-primary mt-3 mb-0'>
                        Register
                    </button>
                </div>
            </AvForm>
        );
    };

    return (
        <section>
            <div className='page-header section-height-85'>
                <div className='container'>
                    <div className='row'>
                        <div
                            className='col-6 d-lg-flex d-none h-100 my-auto pe-0 position-absolute top-0 end-0 text-center justify-content-center flex-column'>
                            <div
                                className='position-relative bg-gradient-primary h-100 m-3 px-7 border-radius-lg d-flex flex-column justify-content-center'>
                                <img
                                    src='../../../../content/img/shapes/pattern-lines.svg'
                                    alt='pattern-lines'
                                    className='position-absolute opacity-4 start-0'
                                />
                                <div className='position-relative'>
                                    <img className='max-width-500 w-100 position-relative z-index-2'
                                         src='../../../../content/img/illustrations/chat.png' />
                                </div>
                                <h4 className='mt-5 text-white font-weight-bolder'>WELCOME! CM PROTOTYPE SYSTEM</h4>
                                <p className='text-white'>
                                    개인정보 수집에 동의해주세요. <br />
                                    The more effortless the writing looks, the more effort the writer actually put into
                                    the process.
                                </p>
                            </div>
                        </div>

                        <div className='col-lg-6 d-flex justify-content-center flex-column'>
                            <div
                                className='card d-flex blur justify-content-center p-4 shadow-lg my-sm-0 my-sm-6 mt-8 mb-5'>
                                <div className='text-center'>
                                    <h4 className='text-gradient text-primary'>Registration</h4>
                                    <p className='mb-0'>Welcome to CM Prototype System. Would you like to become a
                                        member?</p>
                                </div>
                                <hr />
                                {getRegisterForm()}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    );
};

const mapDispatchToProps = { handleRegister, reset };
type DispatchProps = typeof mapDispatchToProps;

export default connect(null, mapDispatchToProps)(RegisterPage);
