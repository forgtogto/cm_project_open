import axios from 'axios';

import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

export const ACTION_TYPES = {
    CREATE_ACCOUNT: 'register/CREATE_ACCOUNT',
    RESET: 'register/RESET'
};

const initialState = {
    loading: false,
    registrationSuccess: false,
    registrationFailure: false,
    errorMessage: null
};

export type RegisterState = Readonly<typeof initialState>;

// Reducer
export default (state: RegisterState = initialState, action): RegisterState => {
    switch (action.type) {
        case REQUEST(ACTION_TYPES.CREATE_ACCOUNT):
            return {
                ...state,
                loading: true
            };
        case FAILURE(ACTION_TYPES.CREATE_ACCOUNT):
            return {
                ...initialState,
                registrationFailure: true,
                errorMessage: action.payload.response.data.errorKey
            };
        case SUCCESS(ACTION_TYPES.CREATE_ACCOUNT):
            return {
                ...initialState,
                registrationSuccess: true
            };
        case ACTION_TYPES.RESET:
            return {
                ...initialState
            };
        default:
            return state;
    }
};

// Actions
export const handleRegister = (login, email, name, birth, password, langKey = 'en') => ({
    type: ACTION_TYPES.CREATE_ACCOUNT,
    payload: axios.post('api/register', { login, email, name, birth, password, langKey }),
    meta: {
        successMessage: '😀 회원가입 되었습니다. 계정 활성화를 위해 이메일을 확인하세요.'
    }
});

export const reset = () => ({
    type: ACTION_TYPES.RESET
});
