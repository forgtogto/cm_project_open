import React, { useEffect, useState } from 'react';

import { connect } from 'react-redux';
import { AvField, AvForm } from 'availity-reactstrap-validation';
import { Button } from 'reactstrap';

import { IRootState } from 'app/shared/reducers';
import { getSession } from 'app/shared/reducers/authentication';
import PasswordStrengthBar from 'app/shared/layout/password/password-strength-bar';
import { reset, savePassword } from './password.reducer';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

export interface IUserPasswordProps extends StateProps, DispatchProps {}

export const PasswordPage = (props: IUserPasswordProps) => {
  const [password, setPassword] = useState('');

  useEffect(() => {
    props.reset();
    props.getSession();
    return () => props.reset();
  }, []);

  const handleValidSubmit = (event, values) => {
    props.savePassword(values.currentPassword, values.newPassword);
  };

  const updatePassword = event => setPassword(event.target.value);

  const getPasswordForm = () => {
    return (
      <AvForm id="password-form" onValidSubmit={handleValidSubmit}>
        <AvField
          name="currentPassword"
          label="현재 비밀번호"
          placeholder={'Current password'}
          type="password"
          validate={{
            required: { value: true, errorMessage: 'Your password is required.' }
          }}
        />
        <AvField
          name="newPassword"
          label="새 비밀번호"
          placeholder={'New password'}
          type="password"
          validate={{
            required: { value: true, errorMessage: 'Your password is required.' },
            minLength: { value: 4, errorMessage: 'Your password is required to be at least 4 characters.' },
            maxLength: { value: 50, errorMessage: 'Your password cannot be longer than 50 characters.' }
          }}
          onChange={updatePassword}
        />
        <PasswordStrengthBar password={password} />
        <AvField
          name="confirmPassword"
          label="새 비밀번호 확인"
          placeholder="Confirm the new password"
          type="password"
          validate={{
            required: {
              value: true,
              errorMessage: 'Your confirmation password is required.'
            },
            minLength: {
              value: 4,
              errorMessage: 'Your confirmation password is required to be at least 4 characters.'
            },
            maxLength: {
              value: 50,
              errorMessage: 'Your confirmation password cannot be longer than 50 characters.'
            },
            match: {
              value: 'newPassword',
              errorMessage: 'The password and its confirmation do not match!'
            }
          }}
        />
        <Button className="btn bg-gradient-dark btn-sm" type="submit">
          <FontAwesomeIcon icon="save" />
          &nbsp; Save
        </Button>
      </AvForm>
    );
  };

  return (
    <section>
      <div className="container py-4 mt-6">
        <div className="row">
          <div className="col-lg-12 mx-auto d-flex justify-content-center flex-column">
            <div className="card d-flex justify-content-center p-4 shadow-lg">
              <div className=" row">
                <div className="col-lg-8 text-left">
                  <h4 className="text-gradient text-primary">패스워드 변경</h4>
                  <p className="mb-0 text-sm"><span className='text-primary'>{props.account.name}</span>님의 패스워드를 변경하세요.</p>
                  <br />
                </div>
                <hr />
                <div className="card card-plain">{getPasswordForm()}</div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

const mapStateToProps = ({ authentication }: IRootState) => ({
  account: authentication.account,
  isAuthenticated: authentication.isAuthenticated
});

const mapDispatchToProps = { getSession, savePassword, reset };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(PasswordPage);
