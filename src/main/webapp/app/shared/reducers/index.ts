import { combineReducers } from 'redux';
import { loadingBarReducer as loadingBar } from 'react-redux-loading-bar';

import authentication, { AuthenticationState } from './authentication';
import applicationProfile, { ApplicationProfileState } from './application-profile';

import administration, { AdministrationState } from 'app/modules/administration/administration.reducer';
import userManagement, { UserManagementState } from 'app/modules/administration/user-management/user-management.reducer';
import register, { RegisterState } from 'app/modules/account/register/register.reducer';
import activate, { ActivateState } from 'app/modules/account/activate/activate.reducer';
import password, { PasswordState } from 'app/modules/account/password/password.reducer';
import settings, { SettingsState } from 'app/modules/account/settings/settings.reducer';
import passwordReset, { PasswordResetState } from 'app/modules/account/password-reset/password-reset.reducer';
// prettier-ignore
import content, {
  ContentState
} from 'app/entities/content/content.reducer';
// prettier-ignore
import plants, {
  PlantsState
} from 'app/entities/plants/plants.reducer';
// prettier-ignore
import plantSystem, {
  PlantSystemState
} from 'app/entities/plant-system/plant-system.reducer';
// prettier-ignore
import requirement, {
  RequirementState
} from 'app/entities/requirement/requirement.reducer';
// prettier-ignore
import departmentCode, {
  DepartmentCodeState
} from 'app/entities/department-code/department-code.reducer';
// prettier-ignore
import equipmentCode, {
  EquipmentCodeState
} from 'app/entities/equipment-code/equipment-code.reducer';
// prettier-ignore
import fbsCode, {
  FbsCodeState
} from 'app/entities/fbs-code/fbs-code.reducer';
// prettier-ignore
import compPipe, {
  CompPipeState
} from 'app/entities/comp-pipe/comp-pipe.reducer';
// prettier-ignore
import compPump, {
  CompPumpState
} from 'app/entities/comp-pump/comp-pump.reducer';
/* jhipster-needle-add-reducer-import - JHipster will add reducer here */

export interface IRootState {
  readonly authentication: AuthenticationState;
  readonly applicationProfile: ApplicationProfileState;
  readonly administration: AdministrationState;
  readonly userManagement: UserManagementState;
  readonly register: RegisterState;
  readonly activate: ActivateState;
  readonly passwordReset: PasswordResetState;
  readonly password: PasswordState;
  readonly settings: SettingsState;
  readonly content: ContentState;
  readonly plants: PlantsState;
  readonly plantSystem: PlantSystemState;
  readonly requirement: RequirementState;
  readonly departmentCode: DepartmentCodeState;
  readonly equipmentCode: EquipmentCodeState;
  readonly fbsCode: FbsCodeState;
  readonly compPipe: CompPipeState;
  readonly compPump: CompPumpState;
  /* jhipster-needle-add-reducer-type - JHipster will add reducer type here */
  readonly loadingBar: any;
}

const rootReducer = combineReducers<IRootState>({
  authentication,
  applicationProfile,
  administration,
  userManagement,
  register,
  activate,
  passwordReset,
  password,
  settings,
  content,
  plants,
  plantSystem,
  requirement,
  departmentCode,
  equipmentCode,
  fbsCode,
  compPipe,
  compPump,
  /* jhipster-needle-add-reducer-combine - JHipster will add reducer here */
  loadingBar
});

export default rootReducer;
