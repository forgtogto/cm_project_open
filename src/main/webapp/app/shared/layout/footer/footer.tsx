import React from 'react';

const Footer = () => (
  <footer className="footer py-2 bg-gradient-faded-dark position-relative overflow-hidden">
    <img
      src="../../../../content/img/shapes/waves-white.svg"
      alt="pattern-lines"
      className="position-absolute start-0 top-0 w-100 opacity-6"
    />
    <div className="container">
      <div className="row">
        <div className="col-lg-4 me-auto mb-lg-0 mb-2 text-lg-left text-center">
          <h6 className="text-white font-weight-bolder text-uppercase mb-lg-2  ">CM Prototype System</h6>
          <ul className="nav flex-row ms-n3 justify-content-lg-start justify-content-center mb-2 mt-sm-0">
            <li className="nav-item">
              <a className="nav-link text-white opacity-8" href="https://www.creative-tim.com">
                Home
              </a>
            </li>
            <li className="nav-item">
              <a className="nav-link text-white opacity-8" href="https://www.creative-tim.com/presentation">
                About
              </a>
            </li>
            <li className="nav-item">
              <a className="nav-link text-white opacity-8" href="https://www.creative-tim.com/blog">
                Blog
              </a>
            </li>
            <li className="nav-item">
              <a className="nav-link text-white opacity-8" href="https://www.creative-tim.com">
                Services
              </a>
            </li>
          </ul>
          <p className="text-sm text-white opacity-8 mb-0">Copyright © 2021 JK Seo. All rights reserved.</p>
        </div>
        <div className="col-lg-6 ms-auto text-lg-right text-center">
          <p className="mt-2 mb-2 text-lg text-white font-weight-bold">
            “If you listen to your fears, you will die never knowing what a great person you might have been.” - Robert Schuller
          </p>
        </div>
      </div>
    </div>
  </footer>
);

export default Footer;
