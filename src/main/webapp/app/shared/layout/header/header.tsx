import React from 'react';
import { NavLink as Link } from 'react-router-dom';
import { Home } from './header-components';
import { MenuService } from 'app/shared/layout/menus/menu-service';

export interface IHeaderProps {
  isAuthenticated: boolean;
  isAdmin: boolean;
}

const Header = (props: IHeaderProps) => {
  const height = '100vh ! important';

  return (
    <>
      <div className="bg-gradient-primary" style={{ height }}>
        <div className="container position-sticky z-index-sticky top-0">
          <div className="row">
            <div className="col-12">
              <nav className="navbar navbar-expand-lg  blur blur-rounded top-0 z-index-fixed shadow position-absolute my-3 py-2 start-0 end-0 mx-4">
                <div className="container-fluid">
                  <Link to={'/'} className="navbar-brand font-weight-bolder ms-sm-3">
                    CM Prototype System
                  </Link>

                  <button
                    className="navbar-toggler shadow-none ms-2"
                    type="button"
                    data-bs-toggle="collapse"
                    data-bs-target="#navigation"
                    aria-controls="navigation"
                    aria-expanded="false"
                    aria-label="Toggle navigation"
                  >
                    <span className="navbar-toggler-icon mt-2">
                      <span className="navbar-toggler-bar bar1" />
                      <span className="navbar-toggler-bar bar2" />
                      <span className="navbar-toggler-bar bar3" />
                    </span>
                  </button>

                  <div className="collapse navbar-collapse pt-3 pb-2 py-lg-0" id="navigation">
                    <ul className="navbar-nav navbar-nav-hover ms-lg-12 ps-lg-5 w-100">
                      <li className="nav-item ms-lg-auto" />

                      <li className="nav-item dropdown mx-2">
                        <Home />
                      </li>

                      <li className="nav-item dropdown dropdown-hover mx-2">
                        {props.isAuthenticated && <MenuService name="serviceMenu" title="Service" />}
                      </li>

                        <li className="nav-item dropdown dropdown-hover mx-2">
                            {props.isAuthenticated && <MenuService name="codeMenu" title="Codes" />}
                        </li>

                      <li className="nav-item dropdown dropdown-hover mx-2">
                        {props.isAuthenticated && props.isAdmin && <MenuService name="adminMenu" title="Admin" />}
                      </li>

                      <li className="nav-item dropdown dropdown-hover mx-2">
                        {props.isAuthenticated && <MenuService name="accountMenu" title="My Page" />}
                      </li>

                      <li className="nav-item my-auto ms-3 ms-lg-0">
                        {props.isAuthenticated ? (
                          <Link to={`/logout`} className="btn btn-sm  bg-gradient-primary  btn-round mb-0 me-1 mt-2 mt-md-0">
                            Sign Out
                          </Link>
                        ) : (
                          <Link to={`/login`} className="btn btn-sm  bg-gradient-primary  btn-round mb-0 me-1 mt-2 mt-md-0">
                            Sign In
                          </Link>
                        )}
                      </li>
                    </ul>
                  </div>
                </div>
              </nav>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default Header;
