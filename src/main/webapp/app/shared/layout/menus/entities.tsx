import React from 'react';
import MenuItem from 'app/shared/layout/menus/menu-item';
import { NavDropdown } from './menu-components';

export const EntitiesMenu = props => (
  <NavDropdown icon="th-list" name="Entities" id="entity-menu" style={{ maxHeight: '80vh', overflow: 'auto' }}>
    <MenuItem icon="asterisk" to="/content">
      Content
    </MenuItem>
    <MenuItem icon="asterisk" to="/plants">
      Plants
    </MenuItem>
    <MenuItem icon="asterisk" to="/plant-system">
      Plant System
    </MenuItem>
    <MenuItem icon="asterisk" to="/requirement">
      Requirement
    </MenuItem>
    <MenuItem icon="asterisk" to="/department-code">
      Department Code
    </MenuItem>
    <MenuItem icon="asterisk" to="/equipment-code">
      Equipment Code
    </MenuItem>
    <MenuItem icon="asterisk" to="/fbs-code">
      Fbs Code
    </MenuItem>
    <MenuItem icon="asterisk" to="/comp-pipe">
      Comp Pipe
    </MenuItem>
    <MenuItem icon="asterisk" to="/comp-pump">
      Comp Pump
    </MenuItem>
    {/* jhipster-needle-add-entity-to-menu - JHipster will add entities to the menu here */}
  </NavDropdown>
);
