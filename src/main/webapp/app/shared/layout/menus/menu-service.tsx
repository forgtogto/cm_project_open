import { NavLink as Link } from 'react-router-dom';
import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

const serviceMenu = (
    <>
        <Link className='dropdown-item border-radius-md' to={`/plants`}>
            <FontAwesomeIcon icon='box' />
            &nbsp;&nbsp;Plants
        </Link>
        <Link className='dropdown-item border-radius-md' to={`/plant-system`}>
            <FontAwesomeIcon icon='boxes' />
            &nbsp;&nbsp;Plant System
        </Link>
        <Link className='dropdown-item border-radius-md' to={`/content`}>
            <FontAwesomeIcon icon='address-card' />
            &nbsp;&nbsp;Content
        </Link>
        <Link className='dropdown-item border-radius-md' to={`/requirement`}>
            <FontAwesomeIcon icon='book' />
            &nbsp;&nbsp;Requirement
        </Link>
        {/*
        <Link className='dropdown-item border-radius-md' to={`/comp-pipe`}>
            <FontAwesomeIcon icon='user-plus' />
            &nbsp;&nbsp;Comp Pipe
        </Link>
        <Link className='dropdown-item border-radius-md' to={`/comp-pump`}>
            <FontAwesomeIcon icon='user-plus' />
            &nbsp;&nbsp;Comp Pump
        </Link>
        */}
    </>
);

const codeMenu = (
    <>
        <Link className='dropdown-item border-radius-md' to={`/department-code`}>
            <FontAwesomeIcon icon='newspaper' />
            &nbsp;&nbsp;Department Code
        </Link>
        <Link className='dropdown-item border-radius-md' to={`/equipment-code`}>
            <FontAwesomeIcon icon='warehouse' />
            &nbsp;&nbsp;Equipment Code
        </Link>
        <Link className='dropdown-item border-radius-md' to={`/fbs-code`}>
            <FontAwesomeIcon icon='laptop-code' />
            &nbsp;&nbsp;FBS Code
        </Link>
    </>
);

const accountMenu = (
    <>
        <Link className='dropdown-item border-radius-md' to={`/account/settings`}>
            <FontAwesomeIcon icon='wrench' />
            &nbsp;&nbsp;Settings
        </Link>
        <Link className='dropdown-item border-radius-md' to={`/account/password`}>
            <FontAwesomeIcon icon='lock' />
            &nbsp;&nbsp;Password
        </Link>
    </>
);

const adminMenu = (
    <>
        <Link className='dropdown-item border-radius-md' to={`/admin/user-management`}>
            <FontAwesomeIcon icon='user' />
            &nbsp;&nbsp;User
        </Link>
    </>
);

export const MenuService = props => {
    let menu = props.name;

    if (menu === 'serviceMenu') {
        menu = serviceMenu;
    } else if (menu === 'codeMenu') {
        menu = codeMenu;
    }else if (menu === 'accountMenu') {
        menu = accountMenu;
    } else if (menu === 'adminMenu') {
        menu = adminMenu;
    }

    return (
        <>
            <Link
                to='#'
                id='dropdownMenuBlocks'
                className='nav-link ps-2 d-flex justify-content-between cursor-pointer align-items-center'
                data-bs-toggle='dropdown'
                aria-expanded='false'
            >
                {' '}
                {props.title}
                <img src='../../../../content/img/down-arrow-dark.svg' alt='down-arrow' className='arrow ms-1' />
            </Link>
            <div
                className='dropdown-menu dropdown-menu-animation dropdown-md dropdown-lg-responsive p-3 border-radius-lg mt-0 mt-lg-3'
                aria-labelledby='dropdownMenuBlocks'
            >
                <div className='d-none d-lg-block'>{menu}</div>
                <div className='d-lg-none'>{menu}</div>
            </div>
        </>
    );
};
