import moment from 'moment';

import {
    APP_LOCAL_DATETIME_FORMAT,
    APP_LOCAL_DATETIME_FORMAT_Z,
    JKSEO_BIRTH_FORMAT,
    JKSEO_DATE_FORMAT
} from 'app/config/constants';

export const convertDateTimeFromServer = date => (date ? moment(date).format(APP_LOCAL_DATETIME_FORMAT) : null);

export const convertDateTimeToServer = date => (date ? moment(date, APP_LOCAL_DATETIME_FORMAT_Z).toDate() : null);

export const displayDefaultDateTime = () =>
    moment()
        .startOf('day')
        .format(APP_LOCAL_DATETIME_FORMAT);


export const convertTime = (date) => (date ? moment(Date.parse(date)).format(JKSEO_DATE_FORMAT) : null);
export const convertBirth = (date) => (date ? moment(Date.parse(date)).format(JKSEO_BIRTH_FORMAT) : null);

export const ageCal = (date) => {
    const birthDay = new Date(date)
    const today = new Date();
    let years = today.getFullYear() - birthDay.getFullYear();
    if (today < birthDay) years--;
    return years;
};
