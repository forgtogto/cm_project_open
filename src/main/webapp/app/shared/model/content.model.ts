import { IPlantSystem } from 'app/shared/model/plant-system.model';
import { IDepartmentCode } from 'app/shared/model/department-code.model';

export interface IContent {
    id?: number;
    version?: string;
    code?: string;
    name?: string;
    serialNo?: string;
    filePath?: string;
    fileName?: string;
    plantSystem?: IPlantSystem;
    departmentCode?: IDepartmentCode;
    createdBy?: string;
    createdDate?: Date;
    lastModifiedBy?: string;
    lastModifiedDate?: Date;
}

export const defaultValue: Readonly<IContent> = {};
