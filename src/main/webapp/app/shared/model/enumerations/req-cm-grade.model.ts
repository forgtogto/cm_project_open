export enum ReqCmGrade {
  ONE = '1등급',
  TWO = '2등급',
  THREE = '3등급',
  FOUR = '4등급'
}
