import { RequesterGroup } from '../../../../../../server/src/domain/enumeration/requester-group';

export interface IUser {
    id?: any;
    login?: string;
    name?: string;
    address?: string;
    birth?: Date;
    phone?: string;
    department: RequesterGroup;
    email?: string;
    activated?: boolean;
    langKey?: string;
    authorities?: any[];
    createdBy?: string;
    createdDate?: Date;
    lastModifiedBy?: string;
    lastModifiedDate?: Date;
    password?: string;
}

export const defaultValue: Readonly<IUser> = {
    id: '',
    login: '',
    name: '',
    address: '',
    birth: null,
    phone: '',
    department: null,
    email: '',
    activated: true,
    langKey: '',
    authorities: [],
    createdBy: '',
    createdDate: null,
    lastModifiedBy: '',
    lastModifiedDate: null,
    password: ''
};
