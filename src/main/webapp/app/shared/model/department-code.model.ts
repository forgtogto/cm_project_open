export interface IDepartmentCode {
  id?: number;
  code?: string;
  comment?: any;
}

export const defaultValue: Readonly<IDepartmentCode> = {};
