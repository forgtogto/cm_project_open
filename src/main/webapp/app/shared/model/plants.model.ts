export interface IPlants {
  id?: string;
  code?: string;
  name?: string;
  comment?: any;
  createdBy?: string;
  createdDate?: Date;
  lastModifiedBy?: string;
  lastModifiedDate?: Date;
}

export const defaultValue: Readonly<IPlants> = {};
