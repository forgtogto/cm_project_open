import { ReqStatus } from 'app/shared/model/enumerations/req-status.model';
import { ReqCmGrade } from 'app/shared/model/enumerations/req-cm-grade.model';
import { RequesterGroup } from 'app/shared/model/enumerations/requester-group.model';
import { IPlantSystem } from 'app/shared/model/plant-system.model';
import { IUser } from 'app/shared/model/user.model';

export interface IRequirement {
    id?: number;
    code?: string;
    name?: string;
    comment?: any;
    reqStatus?: ReqStatus;
    cmGrade?: ReqCmGrade;
    requesterGroup?: RequesterGroup;
    users?: IUser;
    plantSystem?: IPlantSystem;
    createdBy?: string;
    createdDate?: Date;
    lastModifiedBy?: string;
    lastModifiedDate?: Date;
}

export const defaultValue: Readonly<IRequirement> = {};
