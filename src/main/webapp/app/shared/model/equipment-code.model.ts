export interface IEquipmentCode {
  id?: number;
  code?: string;
  comment?: any;
}

export const defaultValue: Readonly<IEquipmentCode> = {};
