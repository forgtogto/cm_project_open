export interface IFbsCode {
  id?: number;
  code?: string;
  fbsType?: string;
  comment?: any;
}

export const defaultValue: Readonly<IFbsCode> = {};
