import { ComSubName } from 'app/shared/model/enumerations/com-sub-name';
import { EquipSubName } from 'app/shared/model/enumerations/equip-sub-name';
import { ReqCmGrade } from 'app/shared/model/enumerations/req-cm-grade.model';

export interface ICompPipe {
  id?: number;
  pipeClass?: string;
  schedule?: string;
  thickness?: string;
  plantSystemId?: number;
  equipmentCodeId?: number;
  createdBy?: string;
  createdDate?: Date;
  lastModifiedBy?: string;
  lastModifiedDate?: Date;
  version?: string;
  code?: string;
  name?: string;
  serialNo?: string;
  filePath?: string;
  fileName?: string;
  compSubName?: ComSubName;
  equipSubName?: EquipSubName;
  cmGrade?: ReqCmGrade;
  manufacturer?: string;
  size?: string;
  envZoneFlag?: string;
}

export const defaultValue: Readonly<ICompPipe> = {};
