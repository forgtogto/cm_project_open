import { IPlants } from 'app/shared/model/plants.model';

export interface IPlantSystem {
  id?: string;
  code?: string;
  name?: string;
  comment?: any;
  plants?: IPlants;
  createdBy?: string;
  createdDate?: Date;
  lastModifiedBy?: string;
  lastModifiedDate?: Date;
}

export const defaultValue: Readonly<IPlantSystem> = {};
