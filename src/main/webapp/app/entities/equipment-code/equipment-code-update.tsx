import React, {useEffect, useState} from 'react';
import {connect} from 'react-redux';
import {Link, RouteComponentProps} from 'react-router-dom';
import {Button, Col, Label, Row} from 'reactstrap';
import {AvField, AvForm, AvGroup, AvInput} from 'availity-reactstrap-validation';
import {setFileData} from 'react-jhipster';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {IRootState} from 'app/shared/reducers';

import {createEntity, getEntity, reset, setBlob, updateEntity} from './equipment-code.reducer';

export interface IEquipmentCodeUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {
}

export const EquipmentCodeUpdate = (props: IEquipmentCodeUpdateProps) => {
    const [isNew, setIsNew] = useState(!props.match.params || !props.match.params.id);

    const {equipmentCodeEntity, loading, updating} = props;

    const {comment} = equipmentCodeEntity;

    const handleClose = () => {
        props.history.push('/equipment-code' + props.location.search);
    };

    useEffect(() => {
        if (isNew) {
            props.reset();
        } else {
            props.getEntity(props.match.params.id);
        }
    }, []);

    const onBlobChange = (isAnImage, name) => event => {
        setFileData(event, (contentType, data) => props.setBlob(name, data, contentType), isAnImage);
    };

    const clearBlob = name => () => {
        props.setBlob(name, undefined, undefined);
    };

    useEffect(() => {
        if (props.updateSuccess) {
            handleClose();
        }
    }, [props.updateSuccess]);

    const saveEntity = (event, errors, values) => {
        if (errors.length === 0) {
            const entity = {
                ...equipmentCodeEntity,
                ...values
            };

            if (isNew) {
                props.createEntity(entity);
            } else {
                props.updateEntity(entity);
            }
        }
    };

    return (
        <section>
            <div className="container py-4 mt-6">
                <div className="row">
                    <div className="col-lg-12 mx-auto d-flex justify-content-center flex-column">
                        <div className="card d-flex justify-content-center p-4 shadow-lg">
                            <div className=" row">
                                <div className="col-lg-8 text-left">
                                    <h3 className="text-gradient text-primary">장비 코드</h3>
                                    <p className="mb-0 text-sm">이 페이지는 장비코드를 생성 및 수정 할 수 있습니다.</p>
                                    <br/>
                                </div>
                                <hr/>
                                <div className="card card-plain">
                                    {loading ? (
                                        <p>Loading...</p>
                                    ) : (
                                        <AvForm model={isNew ? {} : equipmentCodeEntity} onSubmit={saveEntity}>
                                            {/*
                          {!isNew ? (
                            <AvGroup>
                              <Label for="plants-id">ID</Label>
                              <AvInput id="plants-id" type="text" className="form-control" name="id" required readOnly />
                            </AvGroup>
                          ) : null}
                         */}
                                            <AvGroup>
                                                <Label id="codeLabel" for="equipment-code-code">
                                                    코드
                                                </Label>
                                                <AvField
                                                    id="equipment-code-code"
                                                    type="text"
                                                    name="code"
                                                    validate={{
                                                        required: {
                                                            value: true,
                                                            errorMessage: 'This field is required.'
                                                        }
                                                    }}
                                                />
                                            </AvGroup>
                                            <AvGroup>
                                                <Label id="commentLabel" for="equipment-code-comment">
                                                    코드 설명
                                                </Label>
                                                <AvInput id="equipment-code-comment" type="textarea" name="comment"
                                                         rows="5"/>
                                            </AvGroup>
                                            <Button className="btn bg-gradient-light btn-sm" tag={Link}
                                                    id="cancel-save" to="/equipment-code" replace>
                                                <FontAwesomeIcon icon="arrow-left"/>
                                                &nbsp;
                                                <span className="d-none d-md-inline">Back</span>
                                            </Button>
                                            &nbsp;
                                            <Button className="btn bg-gradient-dark btn-sm" id="save-entity"
                                                    type="submit" disabled={updating}>
                                                <FontAwesomeIcon icon="save"/>
                                                &nbsp; Save
                                            </Button>
                                        </AvForm>
                                    )}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

    );
};

const mapStateToProps = (storeState: IRootState) => ({
    equipmentCodeEntity: storeState.equipmentCode.entity,
    loading: storeState.equipmentCode.loading,
    updating: storeState.equipmentCode.updating,
    updateSuccess: storeState.equipmentCode.updateSuccess
});

const mapDispatchToProps = {
    getEntity,
    updateEntity,
    setBlob,
    createEntity,
    reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(EquipmentCodeUpdate);
