import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import EquipmentCode from './equipment-code';
import EquipmentCodeDetail from './equipment-code-detail';
import EquipmentCodeUpdate from './equipment-code-update';
import EquipmentCodeDeleteDialog from './equipment-code-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/:id/delete`} component={EquipmentCodeDeleteDialog} />
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={EquipmentCodeUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={EquipmentCodeUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={EquipmentCodeDetail} />
      <ErrorBoundaryRoute path={match.url} component={EquipmentCode} />
    </Switch>
  </>
);

export default Routes;
