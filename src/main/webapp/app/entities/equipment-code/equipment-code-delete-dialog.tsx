import React, {useEffect} from 'react';
import {connect} from 'react-redux';
import {RouteComponentProps} from 'react-router-dom';
import {Button, Modal, ModalBody, ModalFooter, ModalHeader} from 'reactstrap';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {IRootState} from 'app/shared/reducers';
import {deleteEntity, getEntity} from './equipment-code.reducer';

export interface IEquipmentCodeDeleteDialogProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const EquipmentCodeDeleteDialog = (props: IEquipmentCodeDeleteDialogProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  const handleClose = () => {
    props.history.push('/equipment-code' + props.location.search);
  };

  useEffect(() => {
    if (props.updateSuccess) {
      handleClose();
    }
  }, [props.updateSuccess]);

  const confirmDelete = () => {
    props.deleteEntity(props.equipmentCodeEntity.id);
  };

  const { equipmentCodeEntity } = props;
  return (
      <Modal isOpen toggle={handleClose}>
          <div className="modal-header">
              <h5 className="modal-title" id="exampleModalLabel">
                  정말로 삭제 하시겠습니까?
              </h5>
              <button onClick={handleClose} type="button" className="btn btn btn-link">
                  <FontAwesomeIcon icon="times-circle" />
              </button>
          </div>
          <ModalBody id="iplantNestApp.plants.delete.question">Are you sure you want to delete this Equipment Code?</ModalBody>
          <ModalFooter>
              <button type="button" className="btn bg-gradient-dark btn-sm" onClick={handleClose} data-bs-dismiss="modal">
                  <FontAwesomeIcon icon="ban" /> &nbsp; Cancel
              </button>
              <button type="button" className="btn bg-gradient-primary btn-sm" onClick={confirmDelete}>
                  <FontAwesomeIcon icon="trash" />
                  &nbsp; Delete
              </button>
          </ModalFooter>
      </Modal>


  );
};

const mapStateToProps = ({ equipmentCode }: IRootState) => ({
  equipmentCodeEntity: equipmentCode.entity,
  updateSuccess: equipmentCode.updateSuccess
});

const mapDispatchToProps = { getEntity, deleteEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(EquipmentCodeDeleteDialog);
