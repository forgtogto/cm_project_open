import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { IEquipmentCode, defaultValue } from 'app/shared/model/equipment-code.model';

export const ACTION_TYPES = {
  FETCH_EQUIPMENTCODE_LIST: 'equipmentCode/FETCH_EQUIPMENTCODE_LIST',
  FETCH_EQUIPMENTCODE: 'equipmentCode/FETCH_EQUIPMENTCODE',
  CREATE_EQUIPMENTCODE: 'equipmentCode/CREATE_EQUIPMENTCODE',
  UPDATE_EQUIPMENTCODE: 'equipmentCode/UPDATE_EQUIPMENTCODE',
  DELETE_EQUIPMENTCODE: 'equipmentCode/DELETE_EQUIPMENTCODE',
  SET_BLOB: 'equipmentCode/SET_BLOB',
  RESET: 'equipmentCode/RESET'
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IEquipmentCode>,
  entity: defaultValue,
  updating: false,
  totalItems: 0,
  updateSuccess: false
};

export type EquipmentCodeState = Readonly<typeof initialState>;

// Reducer

export default (state: EquipmentCodeState = initialState, action): EquipmentCodeState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_EQUIPMENTCODE_LIST):
    case REQUEST(ACTION_TYPES.FETCH_EQUIPMENTCODE):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true
      };
    case REQUEST(ACTION_TYPES.CREATE_EQUIPMENTCODE):
    case REQUEST(ACTION_TYPES.UPDATE_EQUIPMENTCODE):
    case REQUEST(ACTION_TYPES.DELETE_EQUIPMENTCODE):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true
      };
    case FAILURE(ACTION_TYPES.FETCH_EQUIPMENTCODE_LIST):
    case FAILURE(ACTION_TYPES.FETCH_EQUIPMENTCODE):
    case FAILURE(ACTION_TYPES.CREATE_EQUIPMENTCODE):
    case FAILURE(ACTION_TYPES.UPDATE_EQUIPMENTCODE):
    case FAILURE(ACTION_TYPES.DELETE_EQUIPMENTCODE):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload
      };
    case SUCCESS(ACTION_TYPES.FETCH_EQUIPMENTCODE_LIST):
      return {
        ...state,
        loading: false,
        entities: action.payload.data,
        totalItems: parseInt(action.payload.headers['x-total-count'], 10)
      };
    case SUCCESS(ACTION_TYPES.FETCH_EQUIPMENTCODE):
      return {
        ...state,
        loading: false,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.CREATE_EQUIPMENTCODE):
    case SUCCESS(ACTION_TYPES.UPDATE_EQUIPMENTCODE):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.DELETE_EQUIPMENTCODE):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {}
      };
    case ACTION_TYPES.SET_BLOB: {
      const { name, data, contentType } = action.payload;
      return {
        ...state,
        entity: {
          ...state.entity,
          [name]: data,
          [name + 'ContentType']: contentType
        }
      };
    }
    case ACTION_TYPES.RESET:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

const apiUrl = 'api/equipment-codes';

// Actions

export const getEntities: ICrudGetAllAction<IEquipmentCode> = (page, size, sort) => {
  const requestUrl = `${apiUrl}${sort ? `?page=${page}&size=${size}&sort=${sort}` : ''}`;
  return {
    type: ACTION_TYPES.FETCH_EQUIPMENTCODE_LIST,
    payload: axios.get<IEquipmentCode>(`${requestUrl}${sort ? '&' : '?'}cacheBuster=${new Date().getTime()}`)
  };
};

export const getEntity: ICrudGetAction<IEquipmentCode> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_EQUIPMENTCODE,
    payload: axios.get<IEquipmentCode>(requestUrl)
  };
};

export const createEntity: ICrudPutAction<IEquipmentCode> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_EQUIPMENTCODE,
    payload: axios.post(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<IEquipmentCode> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_EQUIPMENTCODE,
    payload: axios.put(apiUrl, cleanEntity(entity))
  });
  return result;
};

export const deleteEntity: ICrudDeleteAction<IEquipmentCode> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_EQUIPMENTCODE,
    payload: axios.delete(requestUrl)
  });
  return result;
};

export const setBlob = (name, data, contentType?) => ({
  type: ACTION_TYPES.SET_BLOB,
  payload: {
    name,
    data,
    contentType
  }
});

export const reset = () => ({
  type: ACTION_TYPES.RESET
});
