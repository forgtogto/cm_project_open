import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { RouteComponentProps } from 'react-router-dom';
import { Modal, ModalHeader, ModalBody, ModalFooter, Button } from 'reactstrap';
import { ICrudGetAction, ICrudDeleteAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRequirement } from 'app/shared/model/requirement.model';
import { IRootState } from 'app/shared/reducers';
import { getEntity, deleteEntity } from './requirement.reducer';

export interface IRequirementDeleteDialogProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const RequirementDeleteDialog = (props: IRequirementDeleteDialogProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  const handleClose = () => {
    props.history.push('/requirement' + props.location.search);
  };

  useEffect(() => {
    if (props.updateSuccess) {
      handleClose();
    }
  }, [props.updateSuccess]);

  const confirmDelete = () => {
    props.deleteEntity(props.requirementEntity.id);
  };

  const { requirementEntity } = props;
  return (
      <Modal isOpen toggle={handleClose}>
          <div className="modal-header">
              <h5 className="modal-title" id="exampleModalLabel">
                  Delete? Really?
              </h5>
              <button onClick={handleClose} type="button" className="btn btn btn-link">
                  <FontAwesomeIcon icon="times-circle" />
              </button>
          </div>
          <ModalBody id="iplantNestApp.plants.delete.question">Are you sure you want to delete this Requirement?</ModalBody>
          <ModalFooter>
              <button type="button" className="btn bg-gradient-dark btn-sm" onClick={handleClose} data-bs-dismiss="modal">
                  <FontAwesomeIcon icon="ban" /> &nbsp; Cancel
              </button>
              <button type="button" className="btn bg-gradient-primary btn-sm" onClick={confirmDelete}>
                  <FontAwesomeIcon icon="trash" />
                  &nbsp; Delete
              </button>
          </ModalFooter>
      </Modal>
  );
};

const mapStateToProps = ({ requirement }: IRootState) => ({
  requirementEntity: requirement.entity,
  updateSuccess: requirement.updateSuccess
});

const mapDispatchToProps = { getEntity, deleteEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(RequirementDeleteDialog);
