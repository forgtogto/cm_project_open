import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Label } from 'reactstrap';
import { AvField, AvForm, AvGroup, AvInput } from 'availity-reactstrap-validation';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';
import { getOnlyUsers } from 'app/modules/administration/user-management/user-management.reducer';
import { getEntities as getPlantSystems } from 'app/entities/plant-system/plant-system.reducer';
import { createEntity, getEntity, reset, setBlob, updateEntity } from './requirement.reducer';
import { RequesterGroup } from 'app/shared/model/enumerations/requester-group.model';
import { ReqStatus } from 'app/shared/model/enumerations/req-status.model';
import { ReqCmGrade } from 'app/shared/model/enumerations/req-cm-grade.model';

export interface IRequirementUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {

}

export const RequirementUpdate = (props: IRequirementUpdateProps) => {
    const [isNew, setIsNew] = useState(!props.match.params || !props.match.params.id);

    const { requirementEntity, users, plantSystems, loading, updating } = props;

    const handleClose = () => {
        props.history.push('/requirement' + props.location.search);
    };

    useEffect(() => {
        if (isNew) {
            props.reset();
        } else {
            props.getEntity(props.match.params.id);
        }

        props.getOnlyUsers();
        props.getPlantSystems();
    }, []);


    useEffect(() => {
        if (props.updateSuccess) {
            handleClose();
        }
    }, [props.updateSuccess]);

    const saveEntity = (event, errors, values) => {
        if (errors.length === 0) {
            const entity = {
                ...requirementEntity,
                ...values
            };
            entity.user = users[values.user];

            if (isNew) {
                props.createEntity(entity);
            } else {
                props.updateEntity(entity);
            }
        }
    };

    return (
        <section>
            <div className='container py-4 mt-6'>
                <div className='row'>
                    <div className='col-lg-12 mx-auto d-flex justify-content-center flex-column'>
                        <div className='card d-flex justify-content-center p-4 shadow-lg'>
                            <div className=' row'>
                                <div className='col-lg-8 text-left'>
                                    <h3 className='text-gradient text-primary'>Requirement</h3>
                                    <p className='mb-0 text-sm'>이 페이지는 Requirement 생성 및 수정 할 수 있습니다.</p>
                                    <br />
                                </div>
                                <hr />
                                <div className='card card-plain'>
                                    {loading ? (
                                        <p>Loading...</p>
                                    ) : (
                                        <AvForm model={isNew ? {} : requirementEntity} onSubmit={saveEntity}>
                                            <div className='row'>
                                                <div className='col-lg-6'>
                                                    <AvGroup>
                                                        <Label for='requirement-plantSystem'>Plant System</Label>
                                                        <AvInput id='requirement-plantSystem' type='select'
                                                                 className='form-select'
                                                                 name='plantSystem.id'>
                                                            <option value='' key='0' />
                                                            {plantSystems
                                                                ? plantSystems.map(otherEntity => (
                                                                    <option value={otherEntity.id} key={otherEntity.id}>
                                                                        {otherEntity.code}
                                                                    </option>
                                                                ))
                                                                : null}
                                                        </AvInput>
                                                    </AvGroup>
                                                </div>
                                                <div className='col-lg-6'>
                                                    <AvGroup>
                                                        <Label id='codeLabel' for='requirement-code'>
                                                            Code
                                                        </Label>
                                                        <AvField
                                                            id='requirement-code'
                                                            type='text'
                                                            name='code'
                                                            validate={{
                                                                required: {
                                                                    value: true,
                                                                    errorMessage: 'This field is required.'
                                                                }
                                                            }}
                                                        />
                                                    </AvGroup>
                                                </div>
                                            </div>
                                            <div className='row'>
                                                <div className='col-lg-6'>
                                                    <AvGroup>
                                                        <Label id='nameLabel' for='requirement-name'>
                                                            Name
                                                        </Label>
                                                        <AvField
                                                            id='requirement-name'
                                                            type='text'
                                                            name='name'
                                                            validate={{
                                                                required: {
                                                                    value: true,
                                                                    errorMessage: 'This field is required.'
                                                                }
                                                            }}
                                                        />
                                                    </AvGroup>
                                                </div>
                                                <div className='col-lg-6'>
                                                    <AvGroup>
                                                        <Label for='requirement-user'>User</Label>
                                                        <AvInput id='requirement-user' type='select'
                                                                 className='form-select'
                                                                 name='users.id'>
                                                            <option value='' key='0' />
                                                            {users
                                                                ? users.map(otherEntity => (
                                                                    <option value={otherEntity.id} key={otherEntity.id}>
                                                                        {otherEntity.name}
                                                                    </option>
                                                                ))
                                                                : null}
                                                        </AvInput>
                                                    </AvGroup>
                                                </div>
                                            </div>
                                            <AvGroup>
                                                <Label id='commentLabel' for='requirement-comment'>
                                                    Comment
                                                </Label>
                                                <AvInput id='requirement-comment' type='textarea' name='comment' />
                                            </AvGroup>
                                            <div className='row'>
                                                <div className='col-lg-4'>
                                                    <AvGroup>
                                                        <Label id='reqStatusLabel' for='requirement-reqStatus'>
                                                            Req Status
                                                        </Label>
                                                        <AvInput
                                                            id='requirement-reqStatus'
                                                            type='select'
                                                            className='form-select'
                                                            name='reqStatus'
                                                            value={(!isNew && requirementEntity.reqStatus) || '등록됨'}
                                                        >
                                                            {Object.entries(ReqStatus).map((key, index) =>
                                                                <option key={key[0].valueOf()} value={key[1].valueOf()}>
                                                                    {key[1].valueOf()}
                                                                </option>
                                                            )}
                                                        </AvInput>
                                                    </AvGroup>
                                                </div>
                                                <div className='col-lg-4'>
                                                    <AvGroup>
                                                        <Label id='cmGradeLabel' for='requirement-cmGrade'>
                                                            Cm Grade
                                                        </Label>
                                                        <AvInput
                                                            id='requirement-cmGrade'
                                                            type='select'
                                                            className='form-select'
                                                            name='cmGrade'
                                                            value={(!isNew && requirementEntity.cmGrade) || '1등급'}
                                                        >
                                                            {Object.entries(ReqCmGrade).map((key, index) =>
                                                                <option key={key[0].valueOf()} value={key[1].valueOf()}>
                                                                    {key[1].valueOf()}
                                                                </option>
                                                            )}
                                                        </AvInput>
                                                    </AvGroup>
                                                </div>
                                                <div className='col-lg-4'>
                                                    <AvGroup>
                                                        <Label id='requesterGroupLabel'
                                                               for='requirement-requesterGroup'>
                                                            Requester Group
                                                        </Label>
                                                        <AvInput
                                                            id='requirement-requesterGroup'
                                                            type='select'
                                                            className='form-select'
                                                            name='requesterGroup'
                                                            value={(!isNew && requirementEntity.requesterGroup) || '건축 부서'}
                                                        >
                                                            {Object.entries(RequesterGroup).map((key, index) =>
                                                                <option key={key[0].valueOf()} value={key[1].valueOf()}>
                                                                    {key[1].valueOf()}
                                                                </option>
                                                            )}
                                                        </AvInput>
                                                    </AvGroup>
                                                </div>
                                            </div>
                                            <Button className='btn bg-gradient-light btn-sm' tag={Link} id='cancel-save'
                                                    to='/requirement' replace>
                                                <FontAwesomeIcon icon='arrow-left' />
                                                &nbsp;
                                                <span className='d-none d-md-inline'>Back</span>
                                            </Button>
                                            &nbsp;
                                            <Button className='btn bg-gradient-dark btn-sm' id='save-entity'
                                                    type='submit' disabled={updating}>
                                                <FontAwesomeIcon icon='save' />
                                                &nbsp; Save
                                            </Button>
                                        </AvForm>
                                    )}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    );
};

const mapStateToProps = (storeState: IRootState) => ({
    users: storeState.userManagement.users,
    plantSystems: storeState.plantSystem.entities,
    requirementEntity: storeState.requirement.entity,
    loading: storeState.requirement.loading,
    updating: storeState.requirement.updating,
    updateSuccess: storeState.requirement.updateSuccess
});

const mapDispatchToProps = {
    getOnlyUsers,
    getPlantSystems,
    getEntity,
    updateEntity,
    setBlob,
    createEntity,
    reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(RequirementUpdate);
