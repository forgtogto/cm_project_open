import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity, getHistoryEntities } from './requirement.reducer';
import { convertTime } from 'app/shared/util/date-utils';

export interface IRequirementDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {
}

export const RequirementDetail = (props: IRequirementDetailProps) => {
    useEffect(() => {
        props.getEntity(props.match.params.id);
        props.getHistoryEntities(props.match.params.id);
    }, []);

    const { requirementEntity, requirementHistory } = props;
    return (
        <section>
            <div className='container py-4 mt-6'>
                <div className='row'>
                    <div className='col-lg-12 mx-auto d-flex justify-content-center flex-column'>
                        <div className='card d-flex justify-content-center p-4 shadow-lg'>
                            <div className=' row'>
                                <div className='col-lg-8 text-left'>
                                    <h3 className='text-gradient text-primary'>Plant System Details</h3>
                                    <p className='mb-0 text-sm'>이 페이지는 {requirementEntity.code} 코드의 세부정보를 확인 할 수
                                        있습니다.</p>
                                    <br />
                                </div>
                                <hr />
                                <nav>
                                    <div className='nav nav-tabs text-sm' id='nav-tab' role='tablist'>
                                        <button className='nav-link active text-primary font-weight-bold'
                                                id='nav-home-tab' data-bs-toggle='tab'
                                                data-bs-target='#nav-home' type='button' role='tab'
                                                aria-controls='nav-home' aria-selected='true'>Details
                                        </button>
                                        <button className='nav-link text-primary font-weight-bold'
                                                id='nav-profile-tab'
                                                data-bs-toggle='tab'
                                                data-bs-target='#nav-profile' type='button' role='tab'
                                                aria-controls='nav-profile' aria-selected='false'>History
                                        </button>
                                        <button className='nav-link text-primary font-weight-bold'
                                                id='nav-contact-tab'
                                                data-bs-toggle='tab'
                                                data-bs-target='#nav-contact' type='button' role='tab'
                                                aria-controls='nav-contact' aria-selected='false'>Etc.
                                        </button>
                                    </div>
                                </nav>
                                <div className='tab-content' id='nav-tabContent'>
                                    <div className='tab-pane fade show active' id='nav-home' role='tabpanel'
                                         aria-labelledby='nav-home-tab'>

                                        <div className='card card-plain pt-lg-3 text-sm'>
                                            <dl className='p-3'>
                                                <dt><span id='code'>요구사항 코드</span></dt>
                                                <dd>{requirementEntity.code}</dd>
                                                <dt><span id='name'>요구사항 명</span></dt>
                                                <dd>{requirementEntity.name}</dd>
                                                <dt><span id='comment'>요구사항 설명</span></dt>
                                                <dd>{requirementEntity.comment}</dd>
                                                <dt><span id='reqStatus'>상태</span></dt>
                                                <dd>{requirementEntity.reqStatus}</dd>
                                                <dt><span id='cmGrade'>CM 등급</span></dt>
                                                <dd>{requirementEntity.cmGrade}</dd>
                                                <dt><span id='requesterGroup'>담당 부서</span></dt>
                                                <dd>{requirementEntity.requesterGroup}</dd>
                                                <dt><span id='user.id'>사용자</span></dt>
                                                <dd>{requirementEntity.users ? requirementEntity.users.name : ''}</dd>
                                                <dt><span id='plantSystem.id'>Plant System</span></dt>
                                                <dd>{requirementEntity.plantSystem ? requirementEntity.plantSystem.code : ''}</dd>
                                            </dl>
                                        </div>

                                    </div>
                                    <div className='tab-pane fade' id='nav-profile' role='tabpanel'
                                         aria-labelledby='nav-profile-tab'>

                                        <div className='card card-plain pt-lg-3 p-3'>

                                            <table className='table table-striped  table-sm'>
                                                <thead>
                                                <tr>
                                                    <th className='text-center text-uppercase text-secondary
                                                text-sm font-weight-bolder opacity-7 col-1'> Code
                                                    </th>
                                                    <th className=' text-center text-uppercase text-secondary
                                                text-sm  font-weight-bolder opacity-7 col-2'> Name
                                                    </th>
                                                    <th className='text-center text-uppercase text-secondary
                                                text-sm font-weight-bolder opacity-7 col-3'> Comment
                                                    </th>
                                                    <th className='text-center text-uppercase text-secondary
                                                text-sm font-weight-bolder opacity-7 col-1 '> CreBy
                                                    </th>
                                                    <th className='text-center text-uppercase text-secondary
                                                text-sm font-weight-bolder opacity-7 col-2 '> CreDate
                                                    </th>
                                                    <th className='text-center text-uppercase text-secondary
                                                text-sm font-weight-bolder opacity-7 col-1 '> ModBy
                                                    </th>
                                                    <th className='text-center text-uppercase text-secondary
                                                text-sm font-weight-bolder opacity-7 col-2 '> ModDate
                                                    </th>
                                                    <th />
                                                </tr>
                                                </thead>
                                                <tbody>

                                                {requirementHistory.map((history, i) => (
                                                    <tr key={`entity-${i}`}>
                                                        <td className='align-middle text-center text-sm'>{history.code}</td>
                                                        <td className='align-middle text-center text-sm'>{history.name}</td>
                                                        <td className='align-middle text-center text-sm '
                                                            style={{ maxWidth: '200px' }}> {history.comment}</td>
                                                        <td className='align-middle text-center text-sm'>
                                                            {history.lastModifiedBy && history.lastModifiedBy.length > 0 ? ('') : (history.createdBy)}
                                                        </td>
                                                        <td className='align-middle text-center text-sm'>
                                                            {history.lastModifiedBy && history.lastModifiedBy.length > 0 ? ('') : (convertTime(history.createdDate))}
                                                        </td>
                                                        <td className='align-middle text-center text-sm'>{history.lastModifiedBy}
                                                        </td>
                                                        <td className='align-middle text-center text-sm'>{convertTime(history.lastModifiedDate)}</td>
                                                    </tr>
                                                ))}
                                                </tbody>
                                            </table>

                                        </div>
                                    </div>
                                    <div className='tab-pane fade' id='nav-contact' role='tabpanel'
                                         aria-labelledby='nav-contact-tab'>
                                        <div className='p-3 text-sm'>
                                            TDB
                                        </div>
                                    </div>
                                </div>
                                <div className='pt-lg-3'>
                                    <Link to='/requirement' type='button' className='btn bg-gradient-light btn-sm '>
                                        <FontAwesomeIcon icon='arrow-left' /> Back
                                    </Link>
                                    &nbsp;
                                    <Link to={`/requirement/${requirementEntity.id}/edit`} type='button'
                                          className='btn bg-gradient-dark btn-sm'>
                                        <FontAwesomeIcon icon='pencil-alt' /> Edit
                                    </Link>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    );
};

const mapStateToProps = ({ requirement }: IRootState) => ({
    requirementEntity: requirement.entity,
    requirementHistory: requirement.entities
});

const mapDispatchToProps = { getEntity, getHistoryEntities };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(RequirementDetail);
