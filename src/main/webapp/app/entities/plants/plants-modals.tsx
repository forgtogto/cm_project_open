import React, { useEffect } from 'react';
import { IRootState } from 'app/shared/reducers';
import { connect } from 'react-redux';
import { deleteEntity, getEntity } from 'app/entities/plants/plants.reducer';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

export const PlantsModals = props => {
  useEffect(() => {
    props.getEntity(props.targetId);
  }, []);

  const locationSearch = `?page=${props.page}&sort=${props.sort},${props.order}`;

  const handleClose = event => {
    event.stopPropagation();
    props.history.push('/plants' + locationSearch);
  };

  useEffect(() => {
    if (props.updateSuccess) {
      handleClose(event);
    }
  }, [props.updateSuccess]);

  const confirmDelete = () => {
    props.deleteEntity(props.plantsEntity.id);
  };

  const { plantsEntity } = props;
  return (
    <>
      <button type="button" className="btn bg-gradient-dark btn-sm mb-0" data-bs-toggle="modal" data-bs-target="#exampleModal">
        <FontAwesomeIcon icon="trash" />
        Delete
      </button>
      <div className="modal fade" id="exampleModal" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div className="modal-dialog">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title" id="exampleModalLabel">
                Confirm delete operation
              </h5>
              <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close" />
            </div>
            <div className="modal-body">Are you sure you want to delete this Plants?</div>
            <div className="modal-footer justify-content-between">
              <button type="button" className="btn bg-gradient-dark" onClick={handleClose} data-bs-dismiss="modal">
                Close
              </button>
              <button type="button" onClick={confirmDelete} className="btn bg-gradient-primary">
                Save changes
              </button>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};
const mapStateToProps = ({ plants }: IRootState) => ({
  plantsEntity: plants.entity,
  updateSuccess: plants.updateSuccess
});

const mapDispatchToProps = { getEntity, deleteEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(PlantsModals);
