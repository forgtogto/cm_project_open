import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Modal, ModalBody, ModalFooter, ModalHeader } from 'reactstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';
import { deleteEntity, getEntity } from './plants.reducer';
import store from 'app/config/store';

export interface IPlantsDeleteDialogProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const PlantsDeleteDialog = (props: IPlantsDeleteDialogProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  const handleClose = event => {
    event.stopPropagation();
    props.history.push('/plants' + props.location.search);
  };

  const confirmDelete = event => {
    props.deleteEntity(props.plantsEntity.id);
    handleClose(event);
  };

  const { plantsEntity } = props;
  return (
    <Modal isOpen toggle={handleClose}>
      <div className="modal-header">
        <h5 className="modal-title" id="exampleModalLabel">
          Delete? Really?
        </h5>
        <button onClick={handleClose} type="button" className="btn btn btn-link">
          <FontAwesomeIcon icon="times-circle" />
        </button>
      </div>
      <ModalBody id="iplantNestApp.plants.delete.question">Are you sure you want to delete this Plants?</ModalBody>
      <ModalFooter>
        <button type="button" className="btn bg-gradient-dark btn-sm" onClick={handleClose} data-bs-dismiss="modal">
          <FontAwesomeIcon icon="ban" /> &nbsp; Cancel
        </button>
        <button type="button" className="btn bg-gradient-primary btn-sm" onClick={confirmDelete}>
          <FontAwesomeIcon icon="trash" />
          &nbsp; Delete
        </button>
      </ModalFooter>
    </Modal>
  );
};

const mapStateToProps = (storeState: IRootState) => ({
  plantsEntity: storeState.plants.entity
});

const mapDispatchToProps = { getEntity, deleteEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(PlantsDeleteDialog);
