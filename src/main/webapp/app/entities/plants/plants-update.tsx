import React, {useEffect, useState} from 'react';
import {connect} from 'react-redux';
import {Link, RouteComponentProps} from 'react-router-dom';
import {Button, Label} from 'reactstrap';
import {AvField, AvForm, AvGroup, AvInput} from 'availity-reactstrap-validation';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {IRootState} from 'app/shared/reducers';

import {createEntity, getEntity, reset, setBlob, updateEntity} from './plants.reducer';

export interface IPlantsUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const PlantsUpdate = (props: IPlantsUpdateProps) => {
  const [isNew, setIsNew] = useState(!props.match.params || !props.match.params.id);

  const { plantsEntity, loading, updating } = props;

  const { comment } = plantsEntity;

  const handleClose = () => {
    props.history.push('/plants' + props.location.search);
  };

  useEffect(() => {
    if (isNew) {
      props.reset();
    } else {
      props.getEntity(props.match.params.id);
    }
  }, []);

  useEffect(() => {
    if (props.updateSuccess) {
      handleClose();
    }
  }, [props.updateSuccess]);

  const saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const entity = {
        ...plantsEntity,
        ...values
      };

      if (isNew) {
        props.createEntity(entity);
      } else {
        props.updateEntity(entity);
      }
    }
  };

  return (
    <section>
      <div className="container py-4 mt-6">
        <div className="row">
          <div className="col-lg-12 mx-auto d-flex justify-content-center flex-column">
            <div className="card d-flex justify-content-center p-4 shadow-lg">
              <div className=" row">
                <div className="col-lg-8 text-left">
                  <h3 className="text-gradient text-primary">Plants</h3>
                  <p className="mb-0 text-sm">이 페이지는 Plants를 생성 및 수정 할 수 있습니다.</p>
                  <br />
                </div>
                <hr />
                <div className="card card-plain">
                  {loading ? (
                    <p>Loading...</p>
                  ) : (
                    <AvForm model={isNew ? {} : plantsEntity} onSubmit={saveEntity}>
                      {/*
                          {!isNew ? (
                            <AvGroup>
                              <Label for="plants-id">ID</Label>
                              <AvInput id="plants-id" type="text" className="form-control" name="id" required readOnly />
                            </AvGroup>
                          ) : null}
                         */}
                      <AvGroup>
                        <Label id="codeLabel" for="plants-code">
                          플랜트 코드
                        </Label>
                        <AvField
                          id="plants-code"
                          type="text"
                          name="code"
                          validate={{
                            required: { value: true, errorMessage: 'This field is required.' }
                          }}
                        />
                      </AvGroup>
                      <AvGroup>
                        <Label id="nameLabel" for="plants-name">
                          플랜트 명
                        </Label>
                        <AvField
                          id="plants-name"
                          type="text"
                          name="name"
                          validate={{
                            required: { value: true, errorMessage: 'This field is required.' }
                          }}
                        />
                      </AvGroup>
                      <AvGroup>
                        <Label id="commentLabel" for="plants-comment">
                          플랜트 설명
                        </Label>
                        <AvInput id="plants-comment" type="textarea" name="comment" rows="5" />
                      </AvGroup>
                      <Button className="btn bg-gradient-light btn-sm" tag={Link} id="cancel-save" to="/plants" replace>
                        <FontAwesomeIcon icon="arrow-left" />
                        &nbsp;
                        <span className="d-none d-md-inline">Back</span>
                      </Button>
                      &nbsp;
                      <Button className="btn bg-gradient-dark btn-sm" id="save-entity" type="submit" disabled={updating}>
                        <FontAwesomeIcon icon="save" />
                        &nbsp; Save
                      </Button>
                    </AvForm>
                  )}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

const mapStateToProps = (storeState: IRootState) => ({
  plantsEntity: storeState.plants.entity,
  loading: storeState.plants.loading,
  updating: storeState.plants.updating,
  updateSuccess: storeState.plants.updateSuccess
});

const mapDispatchToProps = {
  getEntity,
  updateEntity,
  setBlob,
  createEntity,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(PlantsUpdate);
