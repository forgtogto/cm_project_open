import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import Plants from './plants';
import PlantsDetail from './plants-detail';
import PlantsUpdate from './plants-update';
import PlantsDeleteDialog from './plants-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={PlantsUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={PlantsUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={PlantsDetail} />
      <ErrorBoundaryRoute path={match.url} component={Plants} />
    </Switch>
    <ErrorBoundaryRoute exact path={`${match.url}/:id/delete`} component={PlantsDeleteDialog} />
  </>
);

export default Routes;
