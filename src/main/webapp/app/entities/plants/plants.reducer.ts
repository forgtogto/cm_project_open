import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { IPlants, defaultValue } from 'app/shared/model/plants.model';
import { messages } from 'app/config/constants';

export const ACTION_TYPES = {
    FETCH_PLANTS_LIST: 'plants/FETCH_PLANTS_LIST',
    FETCH_PLANTS_HISTORY_LIST: 'plants/FETCH_PLANTS_HISTORY_LIST',
    FETCH_PLANTS: 'plants/FETCH_PLANTS',
    CREATE_PLANTS: 'plants/CREATE_PLANTS',
    UPDATE_PLANTS: 'plants/UPDATE_PLANTS',
    DELETE_PLANTS: 'plants/DELETE_PLANTS',
    SET_BLOB: 'plants/SET_BLOB',
    RESET: 'plants/RESET'
};

const initialState = {
    loading: false,
    errorMessage: null,
    entities: [] as ReadonlyArray<IPlants>,
    entity: defaultValue,
    updating: false,
    totalItems: 0,
    updateSuccess: false
};

export type PlantsState = Readonly<typeof initialState>;

// Reducer

export default (state: PlantsState = initialState, action): PlantsState => {
    switch (action.type) {
        case REQUEST(ACTION_TYPES.FETCH_PLANTS_LIST):
        case REQUEST(ACTION_TYPES.FETCH_PLANTS_HISTORY_LIST):
        case REQUEST(ACTION_TYPES.FETCH_PLANTS):
            return {
                ...state,
                errorMessage: null,
                updateSuccess: false,
                loading: true
            };
        case REQUEST(ACTION_TYPES.CREATE_PLANTS):
        case REQUEST(ACTION_TYPES.UPDATE_PLANTS):
        case REQUEST(ACTION_TYPES.DELETE_PLANTS):
            return {
                ...state,
                errorMessage: null,
                updateSuccess: false,
                updating: true
            };
        case FAILURE(ACTION_TYPES.FETCH_PLANTS_LIST):
        case FAILURE(ACTION_TYPES.FETCH_PLANTS_HISTORY_LIST):
        case FAILURE(ACTION_TYPES.FETCH_PLANTS):
        case FAILURE(ACTION_TYPES.CREATE_PLANTS):
        case FAILURE(ACTION_TYPES.UPDATE_PLANTS):
        case FAILURE(ACTION_TYPES.DELETE_PLANTS):
            return {
                ...state,
                loading: false,
                updating: false,
                updateSuccess: false,
                errorMessage: action.payload
            };
        case SUCCESS(ACTION_TYPES.FETCH_PLANTS_LIST):
            return {
                ...state,
                loading: false,
                entities: action.payload.data,
                totalItems: parseInt(action.payload.headers['x-total-count'], 10)
            };
        case SUCCESS(ACTION_TYPES.FETCH_PLANTS_HISTORY_LIST):
            return {
                ...state,
                loading: false,
                entities: action.payload.data
            };
        case SUCCESS(ACTION_TYPES.FETCH_PLANTS):
            return {
                ...state,
                loading: false,
                entity: action.payload.data
            };
        case SUCCESS(ACTION_TYPES.CREATE_PLANTS):
        case SUCCESS(ACTION_TYPES.UPDATE_PLANTS):
            return {
                ...state,
                updating: false,
                updateSuccess: true,
                entity: action.payload.data
            };
        case SUCCESS(ACTION_TYPES.DELETE_PLANTS):
            return {
                ...state,
                updating: false,
                updateSuccess: true,
                entity: {}
            };
        case ACTION_TYPES.SET_BLOB: {
            const { name, data, contentType } = action.payload;
            return {
                ...state,
                entity: {
                    ...state.entity,
                    [name]: data,
                    [name + 'ContentType']: contentType
                }
            };
        }
        case ACTION_TYPES.RESET:
            return {
                ...initialState
            };
        default:
            return state;
    }
};

const apiUrl = 'api/plants';

// Actions

export const getEntities: ICrudGetAllAction<IPlants> = (page, size, sort) => {
    const requestUrl = `${apiUrl}${sort ? `?page=${page}&size=${size}&sort=${sort}` : ''}`;
    return {
        type: ACTION_TYPES.FETCH_PLANTS_LIST,
        payload: axios.get<IPlants>(`${requestUrl}${sort ? '&' : '?'}cacheBuster=${new Date().getTime()}`)
    };
};

export const getHistoryEntities: ICrudGetAction<IPlants> = id => {
    const requestUrl = `${apiUrl}/${id}/history`;
    return {
        type: ACTION_TYPES.FETCH_PLANTS_HISTORY_LIST,
        payload: axios.get<IPlants>(requestUrl)
    };
};

export const getEntity: ICrudGetAction<IPlants> = id => {
    const requestUrl = `${apiUrl}/${id}`;
    return {
        type: ACTION_TYPES.FETCH_PLANTS,
        payload: axios.get<IPlants>(requestUrl)
    };
};

export const createEntity: ICrudPutAction<IPlants> = entity => async dispatch => {
    const result = await dispatch({
        type: ACTION_TYPES.CREATE_PLANTS,
        payload: axios.post(apiUrl, cleanEntity(entity)),
        meta: {
            successMessage: messages.CREATE
        }
    });
    dispatch(getEntities());
    return result;
};

export const updateEntity: ICrudPutAction<IPlants> = entity => async dispatch => {
    const result = await dispatch({
        type: ACTION_TYPES.UPDATE_PLANTS,
        payload: axios.put(apiUrl, cleanEntity(entity)),
        meta: {
            successMessage: messages.UPDATE
        }
    });
    return result;
};

export const deleteEntity: ICrudDeleteAction<IPlants> = id => async dispatch => {
    const requestUrl = `${apiUrl}/${id}`;
    const result = await dispatch({
        type: ACTION_TYPES.DELETE_PLANTS,
        payload: axios.delete(requestUrl),
        meta: {
            successMessage: messages.DELETE
        }
    });
    dispatch(getEntities());
    return result;
};

export const setBlob = (name, data, contentType?) => ({
    type: ACTION_TYPES.SET_BLOB,
    payload: {
        name,
        data,
        contentType
    }
});

export const reset = () => ({
    type: ACTION_TYPES.RESET
});
