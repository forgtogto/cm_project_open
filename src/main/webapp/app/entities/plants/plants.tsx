import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { getSortState, JhiItemCount } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { JhiPagination } from 'app/shared/layout/pagination/pagination';

import { IRootState } from 'app/shared/reducers';
import { getEntities } from './plants.reducer';
import { ITEMS_PER_PAGE } from 'app/shared/util/pagination.constants';

import Grid from '@toast-ui/react-grid';
import 'tui-grid/dist/tui-grid.css';

export interface IPlantsProps extends StateProps, DispatchProps, RouteComponentProps<{ url: string }> {}

export const Plants = (props: IPlantsProps) => {
  const [pagination, setPagination] = useState(getSortState(props.location, ITEMS_PER_PAGE));

  useEffect(() => {
    props.getEntities(pagination.activePage - 1, pagination.itemsPerPage, `${pagination.sort},${pagination.order}`);
    props.history.push(`${props.location.pathname}?page=${pagination.activePage}&sort=${pagination.sort},${pagination.order}`);
  }, [pagination]);

  const sort = p => () => {
    setPagination({
      ...pagination,
      order: pagination.order === 'asc' ? 'desc' : 'asc',
      sort: p
    });
  };

  const handlePagination = currentPage =>
    setPagination({
      ...pagination,
      activePage: currentPage
    });

  const columns = [
    { name: 'code', header: '코드' },
    { name: 'name', header: '이름' },
    { name: 'comment', header: '설명' },
    { name: 'func', header: '' }
  ];

  const { plantsList, match, loading, totalItems } = props;
  return (
    <section>
      <div className="container py-4 mt-6">
        <div className="row">
          <div className="col-lg-12 mx-auto d-flex justify-content-center flex-column">
            <div className="card d-flex justify-content-center p-4 shadow-lg">
              <div className=" row">
                <div className="col-lg-8 text-left">
                  <h3 className="text-gradient text-primary">Plants</h3>
                  <p className="mb-0 text-sm">이곳엔 Plants의 내용을 볼 수 있습니다.</p>
                  <br />
                </div>
                <div className="col-lg-4 pt-2 text-lg-right">
                  <Link to={`${match.url}/new`} className="btn btn-primary float-right jh-create-entity " id="jh-create-entity">
                    <FontAwesomeIcon icon="plus" />
                    &nbsp; Create new Plants
                  </Link>
                </div>
                <hr />
              </div>
              <div className="card card-plain">
                <form role="form" id="contact-form" method="post" autoComplete="off">
                  {/* <Grid
                    data={plantsList.map(plants => (
                      {
                        code: plants.code,
                        name: plants.name,
                        comment: plants.comment,
                        func: gray(plants.id)
                      }
                    ))}
                    columns={columns}
                    rowHeaders={['rowNum']}
                  />
                  */}
                  <div className="card-body pb-2 pt-lg-0">
                    <div className="table-responsive">
                      {plantsList && plantsList.length > 0 ? (
                        <table className="table table-striped  table-sm">
                          <thead>
                            <tr>
                              <th
                                className="text-center text-uppercase text-secondary  text-sm font-weight-bolder opacity-7 col-2"
                                onClick={sort('code')}
                              >
                                Code <FontAwesomeIcon icon="sort" />
                              </th>
                              <th
                                className=" text-center text-uppercase text-secondary text-sm  font-weight-bolder opacity-7 col-2"
                                onClick={sort('name')}
                              >
                                Name <FontAwesomeIcon icon="sort" />
                              </th>
                              <th
                                className="text-center text-uppercase text-secondary text-sm font-weight-bolder opacity-7 col-2 "
                                onClick={sort('comment')}
                              >
                                Comment <FontAwesomeIcon icon="sort" />
                              </th>
                              <th />
                            </tr>
                          </thead>
                          <tbody>
                            {plantsList.map((plants, i) => (
                              <tr key={`entity-${i}`}>
                                <td className="align-middle text-center text-sm">{plants.code}</td>
                                <td className="align-middle text-center text-sm">{plants.name}</td>
                                <td className="align-middle text-center text-sm text-truncate " style={{ maxWidth: '200px' }}>
                                  {plants.comment}
                                </td>
                                <td className="align-middle text-right">
                                  <div className="btn-group flex-btn-group-container w-auto me-2 ">
                                    <Link
                                      to={`${match.url}/${plants.id}`}
                                      type="button"
                                      className="btn bg-gradient-secondary btn-sm mb-0 text-xs"
                                    >
                                      <FontAwesomeIcon icon="eye" /> View
                                    </Link>
                                    <Link
                                      to={`${match.url}/${plants.id}/edit?page=${pagination.activePage}&sort=${pagination.sort},${pagination.order}`}
                                      type="button"
                                      className="btn bg-gradient-light btn-sm mb-0 text-xs"
                                    >
                                      <FontAwesomeIcon icon="pencil-alt" /> Edit
                                    </Link>
                                    <Link
                                      to={`${match.url}/${plants.id}/delete?page=${pagination.activePage}&sort=${pagination.sort},${pagination.order}`}
                                      type="button"
                                      className="btn bg-gradient-dark btn-sm mb-0 text-xs"
                                    >
                                      <FontAwesomeIcon icon="trash" /> Delete
                                    </Link>
                                  </div>
                                </td>
                              </tr>
                            ))}
                          </tbody>
                        </table>
                      ) : (
                        !loading && <div className="alert alert-warning">No Plants found</div>
                      )}
                    </div>
                    <div className="text-sm align-items-center">
                      <div className={plantsList && plantsList.length > 0 ? '' : 'd-none'}>
                        <JhiItemCount page={pagination.activePage} total={totalItems} itemsPerPage={pagination.itemsPerPage} i18nEnabled />
                        <JhiPagination
                          activePage={pagination.activePage}
                          onSelect={handlePagination}
                          maxButtons={5}
                          itemsPerPage={pagination.itemsPerPage}
                          totalItems={props.totalItems}
                        />
                      </div>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

const mapStateToProps = ({ plants }: IRootState) => ({
  plantsList: plants.entities,
  loading: plants.loading,
  totalItems: plants.totalItems
});

const mapDispatchToProps = {
  getEntities
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(Plants);
