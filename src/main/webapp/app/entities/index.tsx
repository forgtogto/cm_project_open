import React from 'react';
import { Switch } from 'react-router-dom';

// eslint-disable-next-line @typescript-eslint/no-unused-vars
import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import Content from './content';
import Plants from './plants';
import PlantSystem from './plant-system';
import Requirement from './requirement';
import DepartmentCode from './department-code';
import EquipmentCode from './equipment-code';
import FbsCode from './fbs-code';
import CompPipe from './comp-pipe';
import CompPump from './comp-pump';
/* jhipster-needle-add-route-import - JHipster will add routes here */

const Routes = ({ match }) => (
  <div>
    <Switch>
      {/* prettier-ignore */}
      <ErrorBoundaryRoute path={`${match.url}content`} component={Content} />
      <ErrorBoundaryRoute path={`${match.url}plants`} component={Plants} />
      <ErrorBoundaryRoute path={`${match.url}plant-system`} component={PlantSystem} />
      <ErrorBoundaryRoute path={`${match.url}requirement`} component={Requirement} />
      <ErrorBoundaryRoute path={`${match.url}department-code`} component={DepartmentCode} />
      <ErrorBoundaryRoute path={`${match.url}equipment-code`} component={EquipmentCode} />
      <ErrorBoundaryRoute path={`${match.url}fbs-code`} component={FbsCode} />
      <ErrorBoundaryRoute path={`${match.url}comp-pipe`} component={CompPipe} />
      <ErrorBoundaryRoute path={`${match.url}comp-pump`} component={CompPump} />
      {/* jhipster-needle-add-route-path - JHipster will add routes here */}
    </Switch>
  </div>
);

export default Routes;
