import React, {useEffect, useState} from 'react';
import {connect} from 'react-redux';
import {Link, RouteComponentProps} from 'react-router-dom';
import {getSortState, JhiItemCount, JhiPagination} from 'react-jhipster';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';

import {IRootState} from 'app/shared/reducers';
import {getEntities} from './fbs-code.reducer';
import {ITEMS_PER_PAGE} from 'app/shared/util/pagination.constants';

export interface IFbsCodeProps extends StateProps, DispatchProps, RouteComponentProps<{ url: string }> {
}

export const FbsCode = (props: IFbsCodeProps) => {
    const [paginationState, setPaginationState] = useState(getSortState(props.location, ITEMS_PER_PAGE));

    const getAllEntities = () => {
        props.getEntities(paginationState.activePage - 1, paginationState.itemsPerPage, `${paginationState.sort},${paginationState.order}`);
    };

    const sortEntities = () => {
        getAllEntities();
        props.history.push(
            `${props.location.pathname}?page=${paginationState.activePage}&sort=${paginationState.sort},${paginationState.order}`
        );
    };

    useEffect(() => {
        sortEntities();
    }, [paginationState.activePage, paginationState.order, paginationState.sort]);

    const sort = p => () => {
        setPaginationState({
            ...paginationState,
            order: paginationState.order === 'asc' ? 'desc' : 'asc',
            sort: p
        });
    };

    const handlePagination = currentPage =>
        setPaginationState({
            ...paginationState,
            activePage: currentPage
        });

    const {fbsCodeList, match, loading, totalItems} = props;
    return (
        <section>
            <div className='container py-4 mt-6'>
                <div className='row'>
                    <div className='col-lg-12 mx-auto d-flex justify-content-center flex-column'>
                        <div className='card d-flex justify-content-center p-4 shadow-lg'>
                            <div className=' row'>
                                <div className='col-lg-8 text-left'>
                                    <h3 className='text-gradient text-primary'>FBS 코드</h3>
                                    <p className='mb-0 text-sm'>이 페이지는 FBS 코드를 볼 수 있습니다.</p>
                                    <br/>
                                </div>
                                <div className='col-lg-4 pt-2 text-lg-right'>
                                    <Link to={`${match.url}/new`}
                                          className='btn btn-primary float-right jh-create-entity '
                                          id='jh-create-entity'>
                                        <FontAwesomeIcon icon='plus'/>
                                        &nbsp; FBS 코드 추가
                                    </Link>
                                </div>
                                <hr/>
                            </div>
                            <div className='card card-plain'>
                                <form role='form' id='contact-form' method='post' autoComplete='off'>
                                    <div className='card-body pb-2 pt-lg-0'>
                                        <div className='table-responsive'>
                                            {fbsCodeList && fbsCodeList.length > 0 ? (
                                                <table className='table table-striped  table-sm'>
                                                    <thead>
                                                    <tr>
                                                        <th className='text-center text-uppercase text-secondary text-sm font-weight-bolder opacity-7  '
                                                            onClick={sort('code')}>
                                                            Code <FontAwesomeIcon icon='sort'/>
                                                        </th>
                                                        <th className='text-center text-uppercase text-secondary text-sm font-weight-bolder opacity-7  '
                                                            onClick={sort('fbsType')}>
                                                            Fbs Type <FontAwesomeIcon icon='sort'/>
                                                        </th>
                                                        <th className='text-center text-uppercase text-secondary text-sm font-weight-bolder opacity-7  '
                                                            onClick={sort('comment')}>
                                                            Comment <FontAwesomeIcon icon='sort'/>
                                                        </th>
                                                        <th/>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    {fbsCodeList.map((fbsCode, i) => (
                                                        <tr key={`entity-${i}`}>
                                                            <td className='align-middle text-center text-sm'>{fbsCode.code}</td>
                                                            <td className='align-middle text-center text-sm'>{fbsCode.fbsType}</td>
                                                            <td className='align-middle text-center text-sm'>{fbsCode.comment}</td>
                                                            <td className='align-middle text-right'>
                                                                <div
                                                                    className='btn-group flex-btn-group-container w-auto me-2 '>
                                                                    <Link
                                                                        to={`${match.url}/${fbsCode.id}`}
                                                                        type='button'
                                                                        className='btn bg-gradient-secondary btn-sm mb-0 text-xs'
                                                                    >
                                                                        <FontAwesomeIcon icon='eye'/> View
                                                                    </Link>
                                                                    <Link
                                                                        to={`${match.url}/${fbsCode.id}/edit?page=${paginationState.activePage}&sort=${paginationState.sort},${paginationState.order}`}
                                                                        type='button'
                                                                        className='btn bg-gradient-light btn-sm mb-0 text-xs'
                                                                    >
                                                                        <FontAwesomeIcon icon='pencil-alt'/> Edit
                                                                    </Link>
                                                                    <Link
                                                                        to={`${match.url}/${fbsCode.id}/delete?page=${paginationState.activePage}&sort=${paginationState.sort},${paginationState.order}`}
                                                                        type='button'
                                                                        className='btn bg-gradient-dark btn-sm mb-0 text-xs'
                                                                    >
                                                                        <FontAwesomeIcon icon='trash'/> Delete
                                                                    </Link>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    ))}
                                                    </tbody>
                                                </table>
                                            ) : (
                                                !loading &&
                                                <div className='alert alert-warning'>No Equipment Code found</div>
                                            )}
                                        </div>
                                        <div className='text-sm align-items-center'>
                                            <div
                                                className={fbsCodeList && fbsCodeList.length > 0 ? '' : 'd-none'}>
                                                <JhiItemCount
                                                    page={paginationState.activePage}
                                                    total={totalItems}
                                                    itemsPerPage={paginationState.itemsPerPage}
                                                    i18nEnabled
                                                />
                                                <JhiPagination
                                                    activePage={paginationState.activePage}
                                                    onSelect={handlePagination}
                                                    maxButtons={5}
                                                    itemsPerPage={paginationState.itemsPerPage}
                                                    totalItems={props.totalItems}
                                                />
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    );
};

const mapStateToProps = ({fbsCode}: IRootState) => ({
    fbsCodeList: fbsCode.entities,
    loading: fbsCode.loading,
    totalItems: fbsCode.totalItems
});

const mapDispatchToProps = {
    getEntities
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(FbsCode);
