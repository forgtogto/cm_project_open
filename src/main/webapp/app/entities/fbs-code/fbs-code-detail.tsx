import React, {useEffect} from 'react';
import {connect} from 'react-redux';
import {Link, RouteComponentProps} from 'react-router-dom';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';

import {IRootState} from 'app/shared/reducers';
import {getEntity} from './fbs-code.reducer';

export interface IFbsCodeDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const FbsCodeDetail = (props: IFbsCodeDetailProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  const { fbsCodeEntity } = props;
  return (
      <section>
          <div className='container py-4 mt-6'>
              <div className='row'>
                  <div className='col-lg-12 mx-auto d-flex justify-content-center flex-column'>
                      <div className='card d-flex justify-content-center p-4 shadow-lg'>
                          <div className=' row'>
                              <div className='col-lg-8 text-left'>
                                  <h3 className='text-gradient text-primary'>FBS 코드 Details</h3>
                                  <p className='mb-0 text-sm'>이 페이지는 {fbsCodeEntity.code} 코드의 세부정보를 확인 할 수
                                      있습니다.</p>
                                  <br />

                                  <nav>
                                      <div className='nav nav-tabs text-sm' id='nav-tab' role='tablist'>
                                          <button className='nav-link active text-primary font-weight-bold'
                                                  id='nav-home-tab' data-bs-toggle='tab'
                                                  data-bs-target='#nav-home' type='button' role='tab'
                                                  aria-controls='nav-home' aria-selected='true'>Details
                                          </button>

                                      </div>
                                  </nav>
                                  <div className='tab-content' id='nav-tabContent'>
                                      <div className='tab-pane fade show active' id='nav-home' role='tabpanel'
                                           aria-labelledby='nav-home-tab'>

                                          <div className='card card-plain pt-lg-3 text-sm'>
                                              <dl className='p-3'>
                                                  <dt>
                                                      <span id='code'>FBS 코드</span>
                                                  </dt>
                                                  <dd>{fbsCodeEntity.code}</dd>
                                                  <dt>
                                                      <span id='code'>FBS 타입</span>
                                                  </dt>
                                                  <dd>{fbsCodeEntity.fbsType}</dd>

                                                  <dt>
                                                      <span id='comment'>FBS 설명</span>
                                                  </dt>
                                                  <dd>{fbsCodeEntity.comment}</dd>

                                              </dl>
                                          </div>
                                      </div>
                                  </div>

                                  <div className='pt-lg-3'>
                                      <Link to='/equipment-code' type='button'
                                            className='btn bg-gradient-light btn-sm '>
                                          <FontAwesomeIcon icon='arrow-left' /> Back
                                      </Link>
                                      &nbsp;
                                      <Link to={`/fbs-code/${fbsCodeEntity.id}/edit`} type='button'
                                            className='btn bg-gradient-dark btn-sm'>
                                          <FontAwesomeIcon icon='pencil-alt' /> Edit
                                      </Link>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </section>
  );
};

const mapStateToProps = ({ fbsCode }: IRootState) => ({
  fbsCodeEntity: fbsCode.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(FbsCodeDetail);
