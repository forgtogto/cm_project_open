import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import FbsCode from './fbs-code';
import FbsCodeDetail from './fbs-code-detail';
import FbsCodeUpdate from './fbs-code-update';
import FbsCodeDeleteDialog from './fbs-code-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/:id/delete`} component={FbsCodeDeleteDialog} />
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={FbsCodeUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={FbsCodeUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={FbsCodeDetail} />
      <ErrorBoundaryRoute path={match.url} component={FbsCode} />
    </Switch>
  </>
);

export default Routes;
