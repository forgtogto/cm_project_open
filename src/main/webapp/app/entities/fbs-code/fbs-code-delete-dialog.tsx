import React, {useEffect} from 'react';
import {connect} from 'react-redux';
import {RouteComponentProps} from 'react-router-dom';
import {Modal, ModalBody, ModalFooter} from 'reactstrap';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {IRootState} from 'app/shared/reducers';
import {deleteEntity, getEntity} from './fbs-code.reducer';

export interface IFbsCodeDeleteDialogProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const FbsCodeDeleteDialog = (props: IFbsCodeDeleteDialogProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  const handleClose = () => {
    props.history.push('/fbs-code' + props.location.search);
  };

  useEffect(() => {
    if (props.updateSuccess) {
      handleClose();
    }
  }, [props.updateSuccess]);

  const confirmDelete = () => {
    props.deleteEntity(props.fbsCodeEntity.id);
  };

  const { fbsCodeEntity } = props;
  return (
      <Modal isOpen toggle={handleClose}>
          <div className="modal-header">
              <h5 className="modal-title" id="exampleModalLabel">
                  정말로 삭제 하시겠습니까?
              </h5>
              <button onClick={handleClose} type="button" className="btn btn btn-link">
                  <FontAwesomeIcon icon="times-circle" />
              </button>
          </div>
          <ModalBody id="iplantNestApp.plants.delete.question">Are you sure you want to delete this FBS Code?</ModalBody>
          <ModalFooter>
              <button type="button" className="btn bg-gradient-dark btn-sm" onClick={handleClose} data-bs-dismiss="modal">
                  <FontAwesomeIcon icon="ban" /> &nbsp; Cancel
              </button>
              <button type="button" className="btn bg-gradient-primary btn-sm" onClick={confirmDelete}>
                  <FontAwesomeIcon icon="trash" />
                  &nbsp; Delete
              </button>
          </ModalFooter>
      </Modal>
  );
};

const mapStateToProps = ({ fbsCode }: IRootState) => ({
  fbsCodeEntity: fbsCode.entity,
  updateSuccess: fbsCode.updateSuccess
});

const mapDispatchToProps = { getEntity, deleteEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(FbsCodeDeleteDialog);
