import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { IFbsCode, defaultValue } from 'app/shared/model/fbs-code.model';

export const ACTION_TYPES = {
  FETCH_FBSCODE_LIST: 'fbsCode/FETCH_FBSCODE_LIST',
  FETCH_FBSCODE: 'fbsCode/FETCH_FBSCODE',
  CREATE_FBSCODE: 'fbsCode/CREATE_FBSCODE',
  UPDATE_FBSCODE: 'fbsCode/UPDATE_FBSCODE',
  DELETE_FBSCODE: 'fbsCode/DELETE_FBSCODE',
  SET_BLOB: 'fbsCode/SET_BLOB',
  RESET: 'fbsCode/RESET'
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IFbsCode>,
  entity: defaultValue,
  updating: false,
  totalItems: 0,
  updateSuccess: false
};

export type FbsCodeState = Readonly<typeof initialState>;

// Reducer

export default (state: FbsCodeState = initialState, action): FbsCodeState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_FBSCODE_LIST):
    case REQUEST(ACTION_TYPES.FETCH_FBSCODE):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true
      };
    case REQUEST(ACTION_TYPES.CREATE_FBSCODE):
    case REQUEST(ACTION_TYPES.UPDATE_FBSCODE):
    case REQUEST(ACTION_TYPES.DELETE_FBSCODE):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true
      };
    case FAILURE(ACTION_TYPES.FETCH_FBSCODE_LIST):
    case FAILURE(ACTION_TYPES.FETCH_FBSCODE):
    case FAILURE(ACTION_TYPES.CREATE_FBSCODE):
    case FAILURE(ACTION_TYPES.UPDATE_FBSCODE):
    case FAILURE(ACTION_TYPES.DELETE_FBSCODE):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload
      };
    case SUCCESS(ACTION_TYPES.FETCH_FBSCODE_LIST):
      return {
        ...state,
        loading: false,
        entities: action.payload.data,
        totalItems: parseInt(action.payload.headers['x-total-count'], 10)
      };
    case SUCCESS(ACTION_TYPES.FETCH_FBSCODE):
      return {
        ...state,
        loading: false,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.CREATE_FBSCODE):
    case SUCCESS(ACTION_TYPES.UPDATE_FBSCODE):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.DELETE_FBSCODE):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {}
      };
    case ACTION_TYPES.SET_BLOB: {
      const { name, data, contentType } = action.payload;
      return {
        ...state,
        entity: {
          ...state.entity,
          [name]: data,
          [name + 'ContentType']: contentType
        }
      };
    }
    case ACTION_TYPES.RESET:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

const apiUrl = 'api/fbs-codes';

// Actions

export const getEntities: ICrudGetAllAction<IFbsCode> = (page, size, sort) => {
  const requestUrl = `${apiUrl}${sort ? `?page=${page}&size=${size}&sort=${sort}` : ''}`;
  return {
    type: ACTION_TYPES.FETCH_FBSCODE_LIST,
    payload: axios.get<IFbsCode>(`${requestUrl}${sort ? '&' : '?'}cacheBuster=${new Date().getTime()}`)
  };
};

export const getEntity: ICrudGetAction<IFbsCode> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_FBSCODE,
    payload: axios.get<IFbsCode>(requestUrl)
  };
};

export const createEntity: ICrudPutAction<IFbsCode> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_FBSCODE,
    payload: axios.post(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<IFbsCode> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_FBSCODE,
    payload: axios.put(apiUrl, cleanEntity(entity))
  });
  return result;
};

export const deleteEntity: ICrudDeleteAction<IFbsCode> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_FBSCODE,
    payload: axios.delete(requestUrl)
  });
  return result;
};

export const setBlob = (name, data, contentType?) => ({
  type: ACTION_TYPES.SET_BLOB,
  payload: {
    name,
    data,
    contentType
  }
});

export const reset = () => ({
  type: ACTION_TYPES.RESET
});
