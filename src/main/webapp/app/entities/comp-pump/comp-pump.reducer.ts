import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { ICompPump, defaultValue } from 'app/shared/model/comp-pump.model';

export const ACTION_TYPES = {
  FETCH_COMPPUMP_LIST: 'compPump/FETCH_COMPPUMP_LIST',
  FETCH_COMPPUMP: 'compPump/FETCH_COMPPUMP',
  CREATE_COMPPUMP: 'compPump/CREATE_COMPPUMP',
  UPDATE_COMPPUMP: 'compPump/UPDATE_COMPPUMP',
  DELETE_COMPPUMP: 'compPump/DELETE_COMPPUMP',
  RESET: 'compPump/RESET'
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<ICompPump>,
  entity: defaultValue,
  updating: false,
  totalItems: 0,
  updateSuccess: false
};

export type CompPumpState = Readonly<typeof initialState>;

// Reducer

export default (state: CompPumpState = initialState, action): CompPumpState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_COMPPUMP_LIST):
    case REQUEST(ACTION_TYPES.FETCH_COMPPUMP):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true
      };
    case REQUEST(ACTION_TYPES.CREATE_COMPPUMP):
    case REQUEST(ACTION_TYPES.UPDATE_COMPPUMP):
    case REQUEST(ACTION_TYPES.DELETE_COMPPUMP):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true
      };
    case FAILURE(ACTION_TYPES.FETCH_COMPPUMP_LIST):
    case FAILURE(ACTION_TYPES.FETCH_COMPPUMP):
    case FAILURE(ACTION_TYPES.CREATE_COMPPUMP):
    case FAILURE(ACTION_TYPES.UPDATE_COMPPUMP):
    case FAILURE(ACTION_TYPES.DELETE_COMPPUMP):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload
      };
    case SUCCESS(ACTION_TYPES.FETCH_COMPPUMP_LIST):
      return {
        ...state,
        loading: false,
        entities: action.payload.data,
        totalItems: parseInt(action.payload.headers['x-total-count'], 10)
      };
    case SUCCESS(ACTION_TYPES.FETCH_COMPPUMP):
      return {
        ...state,
        loading: false,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.CREATE_COMPPUMP):
    case SUCCESS(ACTION_TYPES.UPDATE_COMPPUMP):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.DELETE_COMPPUMP):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {}
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

const apiUrl = 'api/comp-pumps';

// Actions

export const getEntities: ICrudGetAllAction<ICompPump> = (page, size, sort) => {
  const requestUrl = `${apiUrl}${sort ? `?page=${page}&size=${size}&sort=${sort}` : ''}`;
  return {
    type: ACTION_TYPES.FETCH_COMPPUMP_LIST,
    payload: axios.get<ICompPump>(`${requestUrl}${sort ? '&' : '?'}cacheBuster=${new Date().getTime()}`)
  };
};

export const getEntity: ICrudGetAction<ICompPump> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_COMPPUMP,
    payload: axios.get<ICompPump>(requestUrl)
  };
};

export const createEntity: ICrudPutAction<ICompPump> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_COMPPUMP,
    payload: axios.post(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<ICompPump> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_COMPPUMP,
    payload: axios.put(apiUrl, cleanEntity(entity))
  });
  return result;
};

export const deleteEntity: ICrudDeleteAction<ICompPump> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_COMPPUMP,
    payload: axios.delete(requestUrl)
  });
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET
});
