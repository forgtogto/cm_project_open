import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { ICrudGetAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './comp-pump.reducer';
import { ICompPump } from 'app/shared/model/comp-pump.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface ICompPumpDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const CompPumpDetail = (props: ICompPumpDetailProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  const { compPumpEntity } = props;
  return (
    <Row>
      <Col md="8">
        <h2>
          CompPump [<b>{compPumpEntity.id}</b>]
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="dePressure">De Pressure</span>
          </dt>
          <dd>{compPumpEntity.dePressure}</dd>
          <dt>
            <span id="opPressure">Op Pressure</span>
          </dt>
          <dd>{compPumpEntity.opPressure}</dd>
          <dt>
            <span id="quantity">Quantity</span>
          </dt>
          <dd>{compPumpEntity.quantity}</dd>
          <dt>Equipment Code</dt>
          <dd>{compPumpEntity.equipmentCodeId ? compPumpEntity.equipmentCodeId : ''}</dd>
          <dt>Plant System</dt>
          <dd>{compPumpEntity.plantSystemId ? compPumpEntity.plantSystemId : ''}</dd>
        </dl>
        <Button tag={Link} to="/comp-pump" replace color="info">
          <FontAwesomeIcon icon="arrow-left" /> <span className="d-none d-md-inline">Back</span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/comp-pump/${compPumpEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
        </Button>
      </Col>
    </Row>
  );
};

const mapStateToProps = ({ compPump }: IRootState) => ({
  compPumpEntity: compPump.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(CompPumpDetail);
