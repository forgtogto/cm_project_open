import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import CompPump from './comp-pump';
import CompPumpDetail from './comp-pump-detail';
import CompPumpUpdate from './comp-pump-update';
import CompPumpDeleteDialog from './comp-pump-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/:id/delete`} component={CompPumpDeleteDialog} />
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={CompPumpUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={CompPumpUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={CompPumpDetail} />
      <ErrorBoundaryRoute path={match.url} component={CompPump} />
    </Switch>
  </>
);

export default Routes;
