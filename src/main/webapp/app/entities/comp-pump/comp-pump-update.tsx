import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { IEquipmentCode } from 'app/shared/model/equipment-code.model';
import { getEntities as getEquipmentCodes } from 'app/entities/equipment-code/equipment-code.reducer';
import { IPlantSystem } from 'app/shared/model/plant-system.model';
import { getEntities as getPlantSystems } from 'app/entities/plant-system/plant-system.reducer';
import { getEntity, updateEntity, createEntity, reset } from './comp-pump.reducer';
import { ICompPump } from 'app/shared/model/comp-pump.model';
import { convertDateTimeFromServer, convertDateTimeToServer, displayDefaultDateTime } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface ICompPumpUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const CompPumpUpdate = (props: ICompPumpUpdateProps) => {
  const [equipmentCodeId, setEquipmentCodeId] = useState('0');
  const [plantSystemId, setPlantSystemId] = useState('0');
  const [isNew, setIsNew] = useState(!props.match.params || !props.match.params.id);

  const { compPumpEntity, equipmentCodes, plantSystems, loading, updating } = props;

  const handleClose = () => {
    props.history.push('/comp-pump' + props.location.search);
  };

  useEffect(() => {
    if (isNew) {
      props.reset();
    } else {
      props.getEntity(props.match.params.id);
    }

    props.getEquipmentCodes();
    props.getPlantSystems();
  }, []);

  useEffect(() => {
    if (props.updateSuccess) {
      handleClose();
    }
  }, [props.updateSuccess]);

  const saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const entity = {
        ...compPumpEntity,
        ...values
      };

      if (isNew) {
        props.createEntity(entity);
      } else {
        props.updateEntity(entity);
      }
    }
  };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="iplantNestApp.compPump.home.createOrEditLabel">Create or edit a CompPump</h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <AvForm model={isNew ? {} : compPumpEntity} onSubmit={saveEntity}>
              {!isNew ? (
                <AvGroup>
                  <Label for="comp-pump-id">ID</Label>
                  <AvInput id="comp-pump-id" type="text" className="form-control" name="id" required readOnly />
                </AvGroup>
              ) : null}
              <AvGroup>
                <Label id="dePressureLabel" for="comp-pump-dePressure">
                  De Pressure
                </Label>
                <AvField id="comp-pump-dePressure" type="text" name="dePressure" />
              </AvGroup>
              <AvGroup>
                <Label id="opPressureLabel" for="comp-pump-opPressure">
                  Op Pressure
                </Label>
                <AvField id="comp-pump-opPressure" type="text" name="opPressure" />
              </AvGroup>
              <AvGroup>
                <Label id="quantityLabel" for="comp-pump-quantity">
                  Quantity
                </Label>
                <AvField id="comp-pump-quantity" type="text" name="quantity" />
              </AvGroup>
              <AvGroup>
                <Label for="comp-pump-equipmentCode">Equipment Code</Label>
                <AvInput id="comp-pump-equipmentCode" type="select" className="form-control" name="equipmentCodeId">
                  <option value="" key="0" />
                  {equipmentCodes
                    ? equipmentCodes.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.id}
                        </option>
                      ))
                    : null}
                </AvInput>
              </AvGroup>
              <AvGroup>
                <Label for="comp-pump-plantSystem">Plant System</Label>
                <AvInput id="comp-pump-plantSystem" type="select" className="form-control" name="plantSystemId">
                  <option value="" key="0" />
                  {plantSystems
                    ? plantSystems.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.id}
                        </option>
                      ))
                    : null}
                </AvInput>
              </AvGroup>
              <Button tag={Link} id="cancel-save" to="/comp-pump" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">Back</span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp; Save
              </Button>
            </AvForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

const mapStateToProps = (storeState: IRootState) => ({
  equipmentCodes: storeState.equipmentCode.entities,
  plantSystems: storeState.plantSystem.entities,
  compPumpEntity: storeState.compPump.entity,
  loading: storeState.compPump.loading,
  updating: storeState.compPump.updating,
  updateSuccess: storeState.compPump.updateSuccess
});

const mapDispatchToProps = {
  getEquipmentCodes,
  getPlantSystems,
  getEntity,
  updateEntity,
  createEntity,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(CompPumpUpdate);
