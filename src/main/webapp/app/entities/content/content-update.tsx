import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Col, Label, Row } from 'reactstrap';
import { AvField, AvForm, AvGroup, AvInput } from 'availity-reactstrap-validation';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';
import { getEntities as getPlantSystems } from 'app/entities/plant-system/plant-system.reducer';
import { getEntities as getDepartmentCodes } from 'app/entities/department-code/department-code.reducer';
import { createEntity, getEntity, reset, updateEntity } from './content.reducer';
import { ReqStatus } from 'app/shared/model/enumerations/req-status.model';
import { ReqCmGrade } from 'app/shared/model/enumerations/req-cm-grade.model';
import { RequesterGroup } from 'app/shared/model/enumerations/requester-group.model';

export interface IContentUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {
}

export const ContentUpdate = (props: IContentUpdateProps) => {
    const [isNew, setIsNew] = useState(!props.match.params || !props.match.params.id);

    const { contentEntity, plantSystems, departmentCodes, loading, updating } = props;

    const handleClose = () => {
        props.history.push('/content' + props.location.search);
    };

    useEffect(() => {
        if (isNew) {
            props.reset();
        } else {
            props.getEntity(props.match.params.id);
        }

        props.getPlantSystems();
        props.getDepartmentCodes();
    }, []);

    useEffect(() => {
        if (props.updateSuccess) {
            handleClose();
        }
    }, [props.updateSuccess]);

    const saveEntity = (event, errors, values) => {
        if (errors.length === 0) {
            const entity = {
                ...contentEntity,
                ...values
            };

            if (isNew) {
                props.createEntity(entity);
            } else {
                props.updateEntity(entity);
            }
        }
    };

    return (
        <section>
            <div className='container py-4 mt-6'>
                <div className='row'>
                    <div className='col-lg-12 mx-auto d-flex justify-content-center flex-column'>
                        <div className='card d-flex justify-content-center p-4 shadow-lg'>
                            <div className=' row'>
                                <div className='col-lg-8 text-left'>
                                    <h3 className='text-gradient text-primary'>Contents</h3>
                                    <p className='mb-0 text-sm'>이 페이지는 Contents 생성 및 수정 할 수 있습니다.</p>
                                    <br />
                                </div>
                                <hr />
                                <div className='card card-plain'>
                                    {loading ? (
                                        <p>Loading...</p>
                                    ) : (
                                        <AvForm model={isNew ? {} : contentEntity} onSubmit={saveEntity}>

                                            <div className='row'>
                                                <div className='col-lg-6'>
                                                    <AvGroup>
                                                        <Label for='content-plantSystem'>Plant System</Label>
                                                        <AvInput id='content-plantSystem' type='select'
                                                                 className='form-control' name='plantSystem.id'>
                                                            <option value='' key='0' />
                                                            {plantSystems
                                                                ? plantSystems.map(otherEntity => (
                                                                    <option value={otherEntity.id} key={otherEntity.id}>
                                                                        {otherEntity.code}
                                                                    </option>
                                                                ))
                                                                : null}
                                                        </AvInput>
                                                    </AvGroup>
                                                </div>
                                                <div className='col-lg-6'>
                                                    <AvGroup>
                                                        <Label for='content-departmentCode'>Department Code</Label>
                                                        <AvInput id='content-departmentCode' type='select'
                                                                 className='form-control' name='departmentCode.id'>
                                                            <option value='' key='0' />
                                                            {departmentCodes
                                                                ? departmentCodes.map(otherEntity => (
                                                                    <option value={otherEntity.id} key={otherEntity.id}>
                                                                        {otherEntity.code}
                                                                    </option>
                                                                ))
                                                                : null}
                                                        </AvInput>
                                                    </AvGroup>
                                                </div>
                                            </div>

                                            <div className='row'>
                                                <div className='col-lg-6'>
                                                    <AvGroup>
                                                        <Label id='codeLabel' for='content-code'>
                                                            Code
                                                        </Label>
                                                        <AvField
                                                            id='content-code'
                                                            type='text'
                                                            name='code'
                                                            validate={{
                                                                required: {
                                                                    value: true,
                                                                    errorMessage: 'This field is required.'
                                                                }
                                                            }}
                                                        />
                                                    </AvGroup>
                                                </div>
                                                <div className='col-lg-6'>
                                                    <AvGroup>
                                                        <Label id='nameLabel' for='content-name'>
                                                            Name
                                                        </Label>
                                                        <AvField
                                                            id='content-name'
                                                            type='text'
                                                            name='name'
                                                            validate={{
                                                                required: {
                                                                    value: true,
                                                                    errorMessage: 'This field is required.'
                                                                }
                                                            }}
                                                        />
                                                    </AvGroup>
                                                </div>
                                            </div>
                                            <div className='row'>
                                                <div className='col-lg-6'>
                                                    <AvGroup>
                                                        <Label id='versionLabel' for='content-version'>
                                                            Version
                                                        </Label>
                                                        <AvField id='content-version' type='text' name='version' />
                                                    </AvGroup>
                                                </div>
                                                <div className='col-lg-6'>
                                                    <AvGroup>
                                                        <Label id='serialNoLabel' for='content-serialNo'>
                                                            Serial No
                                                        </Label>
                                                        <AvField id='content-serialNo' type='text' name='serialNo' />
                                                    </AvGroup>
                                                </div>
                                            </div>

                                            <AvGroup>
                                                <Label id='filePathLabel' for='content-filePath'>
                                                    File Path
                                                </Label>
                                                <AvField id='content-filePath' type='text' name='filePath' />
                                            </AvGroup>
                                            <AvGroup>
                                                <Label id='fileNameLabel' for='content-fileName'>
                                                    File Name
                                                </Label>
                                                <AvField id='content-fileName' type='text' name='fileName' />
                                            </AvGroup>

                                            <Button className='btn bg-gradient-light btn-sm' tag={Link}
                                                    id='cancel-save' to='/content' replace>
                                                <FontAwesomeIcon icon='arrow-left' />
                                                &nbsp;
                                                <span className='d-none d-md-inline'>Back</span>
                                            </Button>
                                            &nbsp;
                                            <Button className='btn bg-gradient-dark btn-sm' id='save-entity'
                                                    type='submit' disabled={updating}>
                                                <FontAwesomeIcon icon='save' />
                                                &nbsp; Save
                                            </Button>
                                        </AvForm>
                                    )}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

    );
};

const mapStateToProps = (storeState: IRootState) => ({
    plantSystems: storeState.plantSystem.entities,
    departmentCodes: storeState.departmentCode.entities,
    contentEntity: storeState.content.entity,
    loading: storeState.content.loading,
    updating: storeState.content.updating,
    updateSuccess: storeState.content.updateSuccess
});

const mapDispatchToProps = {
    getPlantSystems,
    getDepartmentCodes,
    getEntity,
    updateEntity,
    createEntity,
    reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(ContentUpdate);
