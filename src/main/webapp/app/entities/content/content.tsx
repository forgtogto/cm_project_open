import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { getSortState, JhiItemCount } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { JhiPagination } from 'app/shared/layout/pagination/pagination';
import { IRootState } from 'app/shared/reducers';
import { getEntities } from './content.reducer';
import { ITEMS_PER_PAGE } from 'app/shared/util/pagination.constants';

export interface IContentProps extends StateProps, DispatchProps, RouteComponentProps<{ url: string }> {
}

export const Content = (props: IContentProps) => {
    const [paginationState, setPaginationState] = useState(getSortState(props.location, ITEMS_PER_PAGE));

    const getAllEntities = () => {
        props.getEntities(paginationState.activePage - 1, paginationState.itemsPerPage, `${paginationState.sort},${paginationState.order}`);
    };

    const sortEntities = () => {
        getAllEntities();
        props.history.push(
            `${props.location.pathname}?page=${paginationState.activePage}&sort=${paginationState.sort},${paginationState.order}`
        );
    };

    useEffect(() => {
        sortEntities();
    }, [paginationState.activePage, paginationState.order, paginationState.sort]);

    const sort = p => () => {
        setPaginationState({
            ...paginationState,
            order: paginationState.order === 'asc' ? 'desc' : 'asc',
            sort: p
        });
    };

    const handlePagination = currentPage =>
        setPaginationState({
            ...paginationState,
            activePage: currentPage
        });


    const { contentList, match, loading, totalItems } = props;
    return (
        <section>
            <div className='container py-4 mt-6'>
                <div className='row'>
                    <div className='col-lg-12 mx-auto d-flex justify-content-center flex-column'>
                        <div className='card d-flex justify-content-center p-4 shadow-lg'>
                            <div className=' row'>
                                <div className='col-lg-8 text-left'>
                                    <h3 className='text-gradient text-primary'>Contents</h3>
                                    <p className='mb-0 text-sm'>이곳엔 Contents 의 내용을 볼 수 있습니다.</p>
                                    <br />
                                </div>
                                <div className='col-lg-4 pt-2 text-lg-right'>
                                    <Link to={`${match.url}/new`}
                                          className='btn btn-primary float-right jh-create-entity '
                                          id='jh-create-entity'>
                                        <FontAwesomeIcon icon='plus' />
                                        &nbsp; Create new Contents
                                    </Link>
                                </div>
                                <hr />
                            </div>
                            <div className='card card-plain'>
                                <form role='form' id='contact-form' method='post' autoComplete='off'>
                                    <div className='card-body pb-2 pt-lg-0'>
                                        <div className='table-responsive'>
                                            {contentList && contentList.length > 0 ? (
                                                <table className='table table-striped  table-sm'>
                                                    <thead>
                                                    <tr>
                                                        <th className='text-center text-uppercase text-secondary text-sm font-weight-bolder opacity-7'
                                                            onClick={sort('plantSystem')}>
                                                            Plant System <FontAwesomeIcon icon='sort' />
                                                        </th>
                                                        <th className='text-center text-uppercase text-secondary text-sm font-weight-bolder opacity-7'
                                                            onClick={sort('code')}>
                                                            Code <FontAwesomeIcon icon='sort' />
                                                        </th>
                                                        <th className='text-center text-uppercase text-secondary text-sm font-weight-bolder opacity-7'
                                                            onClick={sort('name')}>
                                                            Name <FontAwesomeIcon icon='sort' />
                                                        </th>
                                                        <th className='text-center text-uppercase text-secondary text-sm font-weight-bolder opacity-7'
                                                            onClick={sort('version')}>
                                                            Version <FontAwesomeIcon icon='sort' />
                                                        </th>
                                                        <th className='text-center text-uppercase text-secondary text-sm font-weight-bolder opacity-7'
                                                            onClick={sort('serialNo')}>
                                                            Serial No <FontAwesomeIcon icon='sort' />
                                                        </th>
                                                        <th className='text-center text-uppercase text-secondary text-sm font-weight-bolder opacity-7'
                                                            onClick={sort('departmentCode')}>
                                                            Department Code <FontAwesomeIcon icon='sort' />
                                                        </th>
                                                        <th className='text-right text-uppercase text-secondary text-sm font-weight-bolder opacity-7'>
                                                            세부정보 | 수정 | 삭제
                                                        </th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    {contentList.map((content, i) => (
                                                        <tr key={`entity-${i}`}>
                                                            <td className='align-middle text-center text-sm'>
                                                                {content.plantSystem ? (
                                                                        <Link
                                                                            to={`plant-system/${content.plantSystem.id}`}>{content.plantSystem.code}
                                                                        </Link>
                                                                    )
                                                                    : ''}
                                                            </td>
                                                            <td className='align-middle text-center text-sm'>{content.code}</td>
                                                            <td className='align-middle text-center text-sm'>{content.name}</td>
                                                            <td className='align-middle text-center text-sm'>{content.version}</td>
                                                            <td className='align-middle text-center text-sm'>{content.serialNo}</td>

                                                            <td className='align-middle text-center text-sm'>
                                                                {content.departmentCode ? (
                                                                    <Link
                                                                        to={`department-code/${content.departmentCode.id}`}>{content.departmentCode.code}</Link>
                                                                ) : (
                                                                    ''
                                                                )}
                                                            </td>
                                                            <td className='align-middle text-right'>
                                                                <div
                                                                    className='btn-group flex-btn-group-container w-auto '>
                                                                    <Link
                                                                        to={`${match.url}/${content.id}`}
                                                                        type='button'
                                                                        className='btn bg-gradient-secondary btn-sm mb-0 px-3 py-2'
                                                                    >
                                                                        <FontAwesomeIcon icon='eye' />
                                                                    </Link>
                                                                    <Link
                                                                        to={`${match.url}/${content.id}/edit?page=${paginationState.activePage}&sort=${paginationState.sort},${paginationState.order}`}
                                                                        type='button'
                                                                        className='btn bg-gradient-light btn-sm mb-0 px-3 py-2'
                                                                    >
                                                                        <FontAwesomeIcon icon='pencil-alt' />
                                                                    </Link>
                                                                    <Link
                                                                        to={`${match.url}/${content.id}/delete?page=${paginationState.activePage}&sort=${paginationState.sort},${paginationState.order}`}
                                                                        type='button'
                                                                        className='btn bg-gradient-dark btn-sm mb-0 px-3 py-2'
                                                                    >
                                                                        <FontAwesomeIcon icon='trash' />
                                                                    </Link>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    ))}
                                                    </tbody>
                                                </table>
                                            ) : (
                                                !loading &&
                                                <div className='alert alert-warning'>No Contents found</div>
                                            )}
                                        </div>
                                        <div className='text-sm align-items-center'>
                                            <div className={contentList && contentList.length > 0 ? '' : 'd-none'}>
                                                <JhiItemCount
                                                    page={paginationState.activePage}
                                                    total={totalItems}
                                                    itemsPerPage={paginationState.itemsPerPage}
                                                    i18nEnabled
                                                />
                                                <JhiPagination
                                                    activePage={paginationState.activePage}
                                                    onSelect={handlePagination}
                                                    maxButtons={5}
                                                    itemsPerPage={paginationState.itemsPerPage}
                                                    totalItems={props.totalItems}
                                                />
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    );
};

const mapStateToProps = ({ content }: IRootState) => ({
    contentList: content.entities,
    loading: content.loading,
    totalItems: content.totalItems
});

const mapDispatchToProps = {
    getEntities
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(Content);
