import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { RouteComponentProps } from 'react-router-dom';
import { Modal, ModalBody, ModalFooter } from 'reactstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';
import { deleteEntity, getEntity } from './content.reducer';

export interface IContentDeleteDialogProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const ContentDeleteDialog = (props: IContentDeleteDialogProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  const handleClose = () => {
    props.history.push('/content' + props.location.search);
  };

  useEffect(() => {
    if (props.updateSuccess) {
      handleClose();
    }
  }, [props.updateSuccess]);

  const confirmDelete = () => {
    props.deleteEntity(props.contentEntity.id);
  };

  const { contentEntity } = props;
  return (
    <Modal isOpen toggle={handleClose}>
        <div className="modal-header">
            <h5 className="modal-title" id="exampleModalLabel">
                Delete? Really?
            </h5>
            <button onClick={handleClose} type="button" className="btn btn btn-link">
                <FontAwesomeIcon icon="times-circle" />
            </button>
        </div>
        <ModalBody id="iplantNestApp.plants.delete.question">Are you sure you want to delete this Content?</ModalBody>
        <ModalFooter>
            <button type="button" className="btn bg-gradient-dark btn-sm" onClick={handleClose} data-bs-dismiss="modal">
                <FontAwesomeIcon icon="ban" /> &nbsp; Cancel
            </button>
            <button type="button" className="btn bg-gradient-primary btn-sm" onClick={confirmDelete}>
                <FontAwesomeIcon icon="trash" />
                &nbsp; Delete
            </button>
        </ModalFooter>
    </Modal>
  );
};

const mapStateToProps = ({ content }: IRootState) => ({
  contentEntity: content.entity,
  updateSuccess: content.updateSuccess
});

const mapDispatchToProps = { getEntity, deleteEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(ContentDeleteDialog);
