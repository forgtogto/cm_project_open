import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Col, Row } from 'reactstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity, getHistoryEntities } from './content.reducer';
import { convertTime } from 'app/shared/util/date-utils';

export interface IContentDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {
}

export const ContentDetail = (props: IContentDetailProps) => {
    useEffect(() => {
        props.getEntity(props.match.params.id);
        props.getHistoryEntities(props.match.params.id);
    }, []);

    const { contentEntity, contentHistory } = props;
    return (
        <section>
            <div className='container py-4 mt-6'>
                <div className='row'>
                    <div className='col-lg-12 mx-auto d-flex justify-content-center flex-column'>
                        <div className='card d-flex justify-content-center p-4 shadow-lg'>
                            <div className=' row'>
                                <div className='col-lg-8 text-left'>
                                    <h3 className='text-gradient text-primary'>Contents Details</h3>
                                    <p className='mb-0 text-sm'>이 페이지는 {contentEntity.code} 코드의 세부정보를 확인 할 수
                                        있습니다.</p>
                                    <br />
                                </div>
                                <hr />
                                <nav>
                                    <div className='nav nav-tabs text-sm' id='nav-tab' role='tablist'>
                                        <button className='nav-link active text-primary font-weight-bold'
                                                id='nav-home-tab' data-bs-toggle='tab'
                                                data-bs-target='#nav-home' type='button' role='tab'
                                                aria-controls='nav-home' aria-selected='true'>Details
                                        </button>
                                        <button className='nav-link text-primary font-weight-bold'
                                                id='nav-profile-tab'
                                                data-bs-toggle='tab'
                                                data-bs-target='#nav-profile' type='button' role='tab'
                                                aria-controls='nav-profile' aria-selected='false'>History
                                        </button>
                                        <button className='nav-link text-primary font-weight-bold'
                                                id='nav-contact-tab'
                                                data-bs-toggle='tab'
                                                data-bs-target='#nav-contact' type='button' role='tab'
                                                aria-controls='nav-contact' aria-selected='false'>Etc.
                                        </button>
                                    </div>
                                </nav>
                                <div className='tab-content' id='nav-tabContent'>
                                    <div className='tab-pane fade show active' id='nav-home' role='tabpanel'
                                         aria-labelledby='nav-home-tab'>

                                        <div className='card card-plain pt-lg-3 text-sm'>
                                            <dl className='p-3'>
                                                <dt><span id='code'>컨텐츠 코드</span></dt>
                                                <dd>{contentEntity.code}</dd>
                                                <dt><span id='name'>컨텐츠 명</span></dt>
                                                <dd>{contentEntity.name}</dd>
                                                <dt><span id='version'>버전</span></dt>
                                                <dd>{contentEntity.version}</dd>
                                                <dt><span id='serialNo'>시리얼넘버</span></dt>
                                                <dd>{contentEntity.serialNo}</dd>
                                                <dt><span id='plantSystem.id'>Plant System</span></dt>
                                                <dd>{contentEntity.plantSystem ? contentEntity.plantSystem.code : ''}</dd>
                                                <dt><span id='departmentCode.id'>사용자</span></dt>
                                                <dd>{contentEntity.departmentCode ? contentEntity.departmentCode.code : ''}</dd>
                                            </dl>
                                        </div>

                                    </div>
                                    <div className='tab-pane fade' id='nav-profile' role='tabpanel'
                                         aria-labelledby='nav-profile-tab'>

                                        <div className='card card-plain pt-lg-3 p-3'>

                                            <table className='table table-striped  table-sm'>
                                                <thead>
                                                <tr>
                                                    <th className='text-center text-uppercase text-secondary
                                                text-sm font-weight-bolder opacity-7 col-1'> Code
                                                    </th>
                                                    <th className=' text-center text-uppercase text-secondary
                                                text-sm  font-weight-bolder opacity-7 col-2'> Name
                                                    </th>
                                                    <th className='text-center text-uppercase text-secondary
                                                text-sm font-weight-bolder opacity-7 col-3'> Comment
                                                    </th>
                                                    <th className='text-center text-uppercase text-secondary
                                                text-sm font-weight-bolder opacity-7 col-1 '> CreBy
                                                    </th>
                                                    <th className='text-center text-uppercase text-secondary
                                                text-sm font-weight-bolder opacity-7 col-2 '> CreDate
                                                    </th>
                                                    <th className='text-center text-uppercase text-secondary
                                                text-sm font-weight-bolder opacity-7 col-1 '> ModBy
                                                    </th>
                                                    <th className='text-center text-uppercase text-secondary
                                                text-sm font-weight-bolder opacity-7 col-2 '> ModDate
                                                    </th>
                                                    <th />
                                                </tr>
                                                </thead>
                                                <tbody>

                                                    {contentHistory.map((history, i) => (
                                                        <tr key={`entity-${i}`}>
                                                            <td className='align-middle text-center text-sm'>{history.code}</td>
                                                            <td className='align-middle text-center text-sm'>{history.name}</td>
                                                            <td className='align-middle text-center text-sm '
                                                                style={{ maxWidth: '200px' }}> {history.version}</td>
                                                            <td className='align-middle text-center text-sm'>
                                                                {history.lastModifiedBy && history.lastModifiedBy.length > 0 ? ('') : (history.createdBy)}
                                                            </td>
                                                            <td className='align-middle text-center text-sm'>
                                                                {history.lastModifiedBy && history.lastModifiedBy.length > 0 ? ('') : (convertTime(history.createdDate))}
                                                            </td>
                                                            <td className='align-middle text-center text-sm'>{history.lastModifiedBy}
                                                            </td>
                                                            <td className='align-middle text-center text-sm'>{convertTime(history.lastModifiedDate)}</td>
                                                        </tr>
                                                    ))}

                                                </tbody>
                                            </table>

                                        </div>
                                    </div>
                                    <div className='tab-pane fade' id='nav-contact' role='tabpanel'
                                         aria-labelledby='nav-contact-tab'>
                                        <div className='p-3 text-sm'>
                                            TDB
                                        </div>
                                    </div>
                                </div>
                                <div className='pt-lg-3'>
                                    <Link to='/content' type='button' className='btn bg-gradient-light btn-sm '>
                                        <FontAwesomeIcon icon='arrow-left' /> Back
                                    </Link>
                                    &nbsp;
                                    <Link to={`/content/${contentEntity.id}/edit`} type='button'
                                          className='btn bg-gradient-dark btn-sm'>
                                        <FontAwesomeIcon icon='pencil-alt' /> Edit
                                    </Link>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

    );
};

const mapStateToProps = ({ content }: IRootState) => ({
    contentEntity: content.entity,
    contentHistory: content.entities
});

const mapDispatchToProps = { getEntity, getHistoryEntities };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(ContentDetail);
