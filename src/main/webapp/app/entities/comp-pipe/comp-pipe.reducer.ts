import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { ICompPipe, defaultValue } from 'app/shared/model/comp-pipe.model';

export const ACTION_TYPES = {
  FETCH_COMPPIPE_LIST: 'compPipe/FETCH_COMPPIPE_LIST',
  FETCH_COMPPIPE: 'compPipe/FETCH_COMPPIPE',
  CREATE_COMPPIPE: 'compPipe/CREATE_COMPPIPE',
  UPDATE_COMPPIPE: 'compPipe/UPDATE_COMPPIPE',
  DELETE_COMPPIPE: 'compPipe/DELETE_COMPPIPE',
  RESET: 'compPipe/RESET'
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<ICompPipe>,
  entity: defaultValue,
  updating: false,
  totalItems: 0,
  updateSuccess: false
};

export type CompPipeState = Readonly<typeof initialState>;

// Reducer

export default (state: CompPipeState = initialState, action): CompPipeState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_COMPPIPE_LIST):
    case REQUEST(ACTION_TYPES.FETCH_COMPPIPE):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true
      };
    case REQUEST(ACTION_TYPES.CREATE_COMPPIPE):
    case REQUEST(ACTION_TYPES.UPDATE_COMPPIPE):
    case REQUEST(ACTION_TYPES.DELETE_COMPPIPE):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true
      };
    case FAILURE(ACTION_TYPES.FETCH_COMPPIPE_LIST):
    case FAILURE(ACTION_TYPES.FETCH_COMPPIPE):
    case FAILURE(ACTION_TYPES.CREATE_COMPPIPE):
    case FAILURE(ACTION_TYPES.UPDATE_COMPPIPE):
    case FAILURE(ACTION_TYPES.DELETE_COMPPIPE):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload
      };
    case SUCCESS(ACTION_TYPES.FETCH_COMPPIPE_LIST):
      return {
        ...state,
        loading: false,
        entities: action.payload.data,
        totalItems: parseInt(action.payload.headers['x-total-count'], 10)
      };
    case SUCCESS(ACTION_TYPES.FETCH_COMPPIPE):
      return {
        ...state,
        loading: false,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.CREATE_COMPPIPE):
    case SUCCESS(ACTION_TYPES.UPDATE_COMPPIPE):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.DELETE_COMPPIPE):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {}
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

const apiUrl = 'api/comp-pipes';

// Actions

export const getEntities: ICrudGetAllAction<ICompPipe> = (page, size, sort) => {
  const requestUrl = `${apiUrl}${sort ? `?page=${page}&size=${size}&sort=${sort}` : ''}`;
  return {
    type: ACTION_TYPES.FETCH_COMPPIPE_LIST,
    payload: axios.get<ICompPipe>(`${requestUrl}${sort ? '&' : '?'}cacheBuster=${new Date().getTime()}`)
  };
};

export const getEntity: ICrudGetAction<ICompPipe> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_COMPPIPE,
    payload: axios.get<ICompPipe>(requestUrl)
  };
};

export const createEntity: ICrudPutAction<ICompPipe> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_COMPPIPE,
    payload: axios.post(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<ICompPipe> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_COMPPIPE,
    payload: axios.put(apiUrl, cleanEntity(entity))
  });
  return result;
};

export const deleteEntity: ICrudDeleteAction<ICompPipe> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_COMPPIPE,
    payload: axios.delete(requestUrl)
  });
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET
});
