import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { ICrudGetAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './comp-pipe.reducer';
import { ICompPipe } from 'app/shared/model/comp-pipe.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface ICompPipeDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const CompPipeDetail = (props: ICompPipeDetailProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  const { compPipeEntity } = props;
  return (
    <Row>
      <Col md="8">
        <h2>
          CompPipe [<b>{compPipeEntity.id}</b>]
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="pipeClass">Pipe Class</span>
          </dt>
          <dd>{compPipeEntity.pipeClass}</dd>
          <dt>
            <span id="schedule">Schedule</span>
          </dt>
          <dd>{compPipeEntity.schedule}</dd>
          <dt>
            <span id="thickness">Thickness</span>
          </dt>
          <dd>{compPipeEntity.thickness}</dd>
          <dt>Plant System</dt>
          <dd>{compPipeEntity.plantSystemId ? compPipeEntity.plantSystemId : ''}</dd>
          <dt>Equipment Code</dt>
          <dd>{compPipeEntity.equipmentCodeId ? compPipeEntity.equipmentCodeId : ''}</dd>
        </dl>
        <Button tag={Link} to="/comp-pipe" replace color="info">
          <FontAwesomeIcon icon="arrow-left" /> <span className="d-none d-md-inline">Back</span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/comp-pipe/${compPipeEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
        </Button>
      </Col>
    </Row>
  );
};

const mapStateToProps = ({ compPipe }: IRootState) => ({
  compPipeEntity: compPipe.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(CompPipeDetail);
