import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { IPlantSystem } from 'app/shared/model/plant-system.model';
import { getEntities as getPlantSystems } from 'app/entities/plant-system/plant-system.reducer';
import { IEquipmentCode } from 'app/shared/model/equipment-code.model';
import { getEntities as getEquipmentCodes } from 'app/entities/equipment-code/equipment-code.reducer';
import { getEntity, updateEntity, createEntity, reset } from './comp-pipe.reducer';
import { ICompPipe } from 'app/shared/model/comp-pipe.model';
import { convertDateTimeFromServer, convertDateTimeToServer, displayDefaultDateTime } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface ICompPipeUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const CompPipeUpdate = (props: ICompPipeUpdateProps) => {
  const [plantSystemId, setPlantSystemId] = useState('0');
  const [equipmentCodeId, setEquipmentCodeId] = useState('0');
  const [isNew, setIsNew] = useState(!props.match.params || !props.match.params.id);

  const { compPipeEntity, plantSystems, equipmentCodes, loading, updating } = props;

  const handleClose = () => {
    props.history.push('/comp-pipe' + props.location.search);
  };

  useEffect(() => {
    if (isNew) {
      props.reset();
    } else {
      props.getEntity(props.match.params.id);
    }

    props.getPlantSystems();
    props.getEquipmentCodes();
  }, []);

  useEffect(() => {
    if (props.updateSuccess) {
      handleClose();
    }
  }, [props.updateSuccess]);

  const saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const entity = {
        ...compPipeEntity,
        ...values
      };

      if (isNew) {
        props.createEntity(entity);
      } else {
        props.updateEntity(entity);
      }
    }
  };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="iplantNestApp.compPipe.home.createOrEditLabel">Create or edit a CompPipe</h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <AvForm model={isNew ? {} : compPipeEntity} onSubmit={saveEntity}>
              {!isNew ? (
                <AvGroup>
                  <Label for="comp-pipe-id">ID</Label>
                  <AvInput id="comp-pipe-id" type="text" className="form-control" name="id" required readOnly />
                </AvGroup>
              ) : null}
              <AvGroup>
                <Label id="pipeClassLabel" for="comp-pipe-pipeClass">
                  Pipe Class
                </Label>
                <AvField id="comp-pipe-pipeClass" type="text" name="pipeClass" />
              </AvGroup>
              <AvGroup>
                <Label id="scheduleLabel" for="comp-pipe-schedule">
                  Schedule
                </Label>
                <AvField id="comp-pipe-schedule" type="text" name="schedule" />
              </AvGroup>
              <AvGroup>
                <Label id="thicknessLabel" for="comp-pipe-thickness">
                  Thickness
                </Label>
                <AvField id="comp-pipe-thickness" type="text" name="thickness" />
              </AvGroup>
              <AvGroup>
                <Label for="comp-pipe-plantSystem">Plant System</Label>
                <AvInput id="comp-pipe-plantSystem" type="select" className="form-control" name="plantSystemId">
                  <option value="" key="0" />
                  {plantSystems
                    ? plantSystems.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.id}
                        </option>
                      ))
                    : null}
                </AvInput>
              </AvGroup>
              <AvGroup>
                <Label for="comp-pipe-equipmentCode">Equipment Code</Label>
                <AvInput id="comp-pipe-equipmentCode" type="select" className="form-control" name="equipmentCodeId">
                  <option value="" key="0" />
                  {equipmentCodes
                    ? equipmentCodes.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.id}
                        </option>
                      ))
                    : null}
                </AvInput>
              </AvGroup>
              <Button tag={Link} id="cancel-save" to="/comp-pipe" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">Back</span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp; Save
              </Button>
            </AvForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

const mapStateToProps = (storeState: IRootState) => ({
  plantSystems: storeState.plantSystem.entities,
  equipmentCodes: storeState.equipmentCode.entities,
  compPipeEntity: storeState.compPipe.entity,
  loading: storeState.compPipe.loading,
  updating: storeState.compPipe.updating,
  updateSuccess: storeState.compPipe.updateSuccess
});

const mapDispatchToProps = {
  getPlantSystems,
  getEquipmentCodes,
  getEntity,
  updateEntity,
  createEntity,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(CompPipeUpdate);
