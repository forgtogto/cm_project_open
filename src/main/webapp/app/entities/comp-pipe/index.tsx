import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import CompPipe from './comp-pipe';
import CompPipeDetail from './comp-pipe-detail';
import CompPipeUpdate from './comp-pipe-update';
import CompPipeDeleteDialog from './comp-pipe-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/:id/delete`} component={CompPipeDeleteDialog} />
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={CompPipeUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={CompPipeUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={CompPipeDetail} />
      <ErrorBoundaryRoute path={match.url} component={CompPipe} />
    </Switch>
  </>
);

export default Routes;
