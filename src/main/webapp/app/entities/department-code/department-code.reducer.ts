import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { IDepartmentCode, defaultValue } from 'app/shared/model/department-code.model';

export const ACTION_TYPES = {
    FETCH_DEPARTMENTCODE_LIST: 'departmentCode/FETCH_DEPARTMENTCODE_LIST',
    FETCH_DEPARTMENTCODE: 'departmentCode/FETCH_DEPARTMENTCODE',
    CREATE_DEPARTMENTCODE: 'departmentCode/CREATE_DEPARTMENTCODE',
    UPDATE_DEPARTMENTCODE: 'departmentCode/UPDATE_DEPARTMENTCODE',
    DELETE_DEPARTMENTCODE: 'departmentCode/DELETE_DEPARTMENTCODE',
    SET_BLOB: 'departmentCode/SET_BLOB',
    RESET: 'departmentCode/RESET'
};

const initialState = {
    loading: false,
    errorMessage: null,
    entities: [] as ReadonlyArray<IDepartmentCode>,
    entity: defaultValue,
    updating: false,
    totalItems: 0,
    updateSuccess: false
};

export type DepartmentCodeState = Readonly<typeof initialState>;

// Reducer

export default (state: DepartmentCodeState = initialState, action): DepartmentCodeState => {
    switch (action.type) {
        case REQUEST(ACTION_TYPES.FETCH_DEPARTMENTCODE_LIST):
        case REQUEST(ACTION_TYPES.FETCH_DEPARTMENTCODE):
            return {
                ...state,
                errorMessage: null,
                updateSuccess: false,
                loading: true
            };
        case REQUEST(ACTION_TYPES.CREATE_DEPARTMENTCODE):
        case REQUEST(ACTION_TYPES.UPDATE_DEPARTMENTCODE):
        case REQUEST(ACTION_TYPES.DELETE_DEPARTMENTCODE):
            return {
                ...state,
                errorMessage: null,
                updateSuccess: false,
                updating: true
            };
        case FAILURE(ACTION_TYPES.FETCH_DEPARTMENTCODE_LIST):
        case FAILURE(ACTION_TYPES.FETCH_DEPARTMENTCODE):
        case FAILURE(ACTION_TYPES.CREATE_DEPARTMENTCODE):
        case FAILURE(ACTION_TYPES.UPDATE_DEPARTMENTCODE):
        case FAILURE(ACTION_TYPES.DELETE_DEPARTMENTCODE):
            return {
                ...state,
                loading: false,
                updating: false,
                updateSuccess: false,
                errorMessage: action.payload
            };
        case SUCCESS(ACTION_TYPES.FETCH_DEPARTMENTCODE_LIST):
            return {
                ...state,
                loading: false,
                entities: action.payload.data,
                totalItems: parseInt(action.payload.headers['x-total-count'], 10)
            };
        case SUCCESS(ACTION_TYPES.FETCH_DEPARTMENTCODE):
            return {
                ...state,
                loading: false,
                entity: action.payload.data
            };
        case SUCCESS(ACTION_TYPES.CREATE_DEPARTMENTCODE):
        case SUCCESS(ACTION_TYPES.UPDATE_DEPARTMENTCODE):
            return {
                ...state,
                updating: false,
                updateSuccess: true,
                entity: action.payload.data
            };
        case SUCCESS(ACTION_TYPES.DELETE_DEPARTMENTCODE):
            return {
                ...state,
                updating: false,
                updateSuccess: true,
                entity: {}
            };
        case ACTION_TYPES.SET_BLOB: {
            const { name, data, contentType } = action.payload;
            return {
                ...state,
                entity: {
                    ...state.entity,
                    [name]: data,
                    [name + 'ContentType']: contentType
                }
            };
        }
        case ACTION_TYPES.RESET:
            return {
                ...initialState
            };
        default:
            return state;
    }
};

const apiUrl = 'api/department-codes';

// Actions

export const getEntities: ICrudGetAllAction<IDepartmentCode> = (page, size, sort) => {
    const requestUrl = `${apiUrl}${sort ? `?page=${page}&size=${size}&sort=${sort}` : ''}`;
    return {
        type: ACTION_TYPES.FETCH_DEPARTMENTCODE_LIST,
        payload: axios.get<IDepartmentCode>(`${requestUrl}${sort ? '&' : '?'}cacheBuster=${new Date().getTime()}`)
    };
};

export const getEntity: ICrudGetAction<IDepartmentCode> = id => {
    const requestUrl = `${apiUrl}/${id}`;
    return {
        type: ACTION_TYPES.FETCH_DEPARTMENTCODE,
        payload: axios.get<IDepartmentCode>(requestUrl)
    };
};

export const createEntity: ICrudPutAction<IDepartmentCode> = entity => async dispatch => {
    const result = await dispatch({
        type: ACTION_TYPES.CREATE_DEPARTMENTCODE,
        payload: axios.post(apiUrl, cleanEntity(entity))
    });
    dispatch(getEntities());
    return result;
};

export const updateEntity: ICrudPutAction<IDepartmentCode> = entity => async dispatch => {
    const result = await dispatch({
        type: ACTION_TYPES.UPDATE_DEPARTMENTCODE,
        payload: axios.put(apiUrl, cleanEntity(entity))
    });
    return result;
};

export const deleteEntity: ICrudDeleteAction<IDepartmentCode> = id => async dispatch => {
    const requestUrl = `${apiUrl}/${id}`;
    const result = await dispatch({
        type: ACTION_TYPES.DELETE_DEPARTMENTCODE,
        payload: axios.delete(requestUrl)
    });
    dispatch(getEntities());
    return result;
};

export const setBlob = (name, data, contentType?) => ({
    type: ACTION_TYPES.SET_BLOB,
    payload: {
        name,
        data,
        contentType
    }
});

export const reset = () => ({
    type: ACTION_TYPES.RESET
});
