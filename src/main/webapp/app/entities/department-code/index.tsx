import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import DepartmentCode from './department-code';
import DepartmentCodeDetail from './department-code-detail';
import DepartmentCodeUpdate from './department-code-update';
import DepartmentCodeDeleteDialog from './department-code-delete-dialog';

const Routes = ({ match }) => (
    <>
        <Switch>
            <ErrorBoundaryRoute exact path={`${match.url}/new`} component={DepartmentCodeUpdate} />
            <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={DepartmentCodeUpdate} />
            <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={DepartmentCodeDetail} />
            <ErrorBoundaryRoute path={match.url} component={DepartmentCode} />
        </Switch>
        <ErrorBoundaryRoute exact path={`${match.url}/:id/delete`} component={DepartmentCodeDeleteDialog} />
    </>
);

export default Routes;
