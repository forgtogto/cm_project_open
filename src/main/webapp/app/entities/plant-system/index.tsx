import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import PlantSystem from './plant-system';
import PlantSystemDetail from './plant-system-detail';
import PlantSystemUpdate from './plant-system-update';
import PlantSystemDeleteDialog from './plant-system-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={PlantSystemUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={PlantSystemUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={PlantSystemDetail} />
      <ErrorBoundaryRoute path={match.url} component={PlantSystem} />
    </Switch>
    <ErrorBoundaryRoute exact path={`${match.url}/:id/delete`} component={PlantSystemDeleteDialog} />
  </>
);

export default Routes;
