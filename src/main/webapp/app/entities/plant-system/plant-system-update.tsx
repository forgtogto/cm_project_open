import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Label } from 'reactstrap';
import { AvField, AvForm, AvGroup, AvInput } from 'availity-reactstrap-validation';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';
import { getEntities as getPlants } from 'app/entities/plants/plants.reducer';
import { createEntity, getEntity, reset, setBlob, updateEntity } from './plant-system.reducer';

export interface IPlantSystemUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const PlantSystemUpdate = (props: IPlantSystemUpdateProps) => {
  const [isNew] = useState(!props.match.params || !props.match.params.id);

  const { plantSystemEntity, plants, loading, updating } = props;

  const handleClose = () => {
    props.history.push('/plant-system' + props.location.search);
  };

  useEffect(() => {
    if (isNew) {
      props.reset();
    } else {
      props.getEntity(props.match.params.id);
    }

    props.getPlants();
  }, []);

  useEffect(() => {
    if (props.updateSuccess) {
      handleClose();
    }
  }, [props.updateSuccess]);

  const saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const entity = {
        ...plantSystemEntity,
        ...values
      };

      if (isNew) {
        props.createEntity(entity);
      } else {
        props.updateEntity(entity);
      }
    }
  };

  return (
    <section>
      <div className="container py-4 mt-6">
        <div className="row">
          <div className="col-lg-12 mx-auto d-flex justify-content-center flex-column">
            <div className="card d-flex justify-content-center p-4 shadow-lg">
              <div className=" row">
                <div className="col-lg-8 text-left">
                  <h3 className="text-gradient text-primary">Plant System</h3>
                  <p className="mb-0 text-sm">이 페이지는 Plant System 생성 및 수정 할 수 있습니다.</p>
                  <br />
                </div>
                <hr />
                <div className="card card-plain">
                  {loading ? (
                    <p>Loading...</p>
                  ) : (
                    <AvForm model={isNew ? {} : plantSystemEntity} onSubmit={saveEntity}>
                      <AvGroup>
                        <Label id="codeLabel" for="plant-system-code">
                          Code
                        </Label>
                        <AvField
                          id="plant-system-code"
                          type="text"
                          name="code"
                          validate={{
                            required: {
                              value: true,
                              errorMessage: 'This field is required.'
                            }
                          }}
                        />
                      </AvGroup>
                      <AvGroup>
                        <Label id="nameLabel" for="plant-system-name">
                          Name
                        </Label>
                        <AvField
                          id="plant-system-name"
                          type="text"
                          name="name"
                          validate={{
                            required: {
                              value: true,
                              errorMessage: 'This field is required.'
                            }
                          }}
                        />
                      </AvGroup>
                      <AvGroup>
                        <Label id="commentLabel" for="plant-system-comment">
                          Comment
                        </Label>
                        <AvInput id="plant-system-comment" type="textarea" name="comment" />
                      </AvGroup>
                      <AvGroup>
                        <Label for="plant-system-plants">Plants</Label>
                        <AvInput id="plant-system-plants" type="select" className="form-select" name="plants.id">
                          <option value="" key="0" />
                          {plants
                            ? plants.map(otherEntity => (
                                <option value={otherEntity.id} key={otherEntity.id}>
                                  {otherEntity.code}
                                </option>
                              ))
                            : null}
                        </AvInput>
                      </AvGroup>
                      <Button className="btn bg-gradient-light btn-sm" tag={Link} id="cancel-save" to="/plant-system" replace>
                        <FontAwesomeIcon icon="arrow-left" />
                        &nbsp;
                        <span className="d-none d-md-inline">Back</span>
                      </Button>
                      &nbsp;
                      <Button className="btn bg-gradient-dark btn-sm" id="save-entity" type="submit" disabled={updating}>
                        <FontAwesomeIcon icon="save" />
                        &nbsp; Save
                      </Button>
                    </AvForm>
                  )}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

const mapStateToProps = (storeState: IRootState) => ({
  plants: storeState.plants.entities,
  plantSystemEntity: storeState.plantSystem.entity,
  loading: storeState.plantSystem.loading,
  updating: storeState.plantSystem.updating,
  updateSuccess: storeState.plantSystem.updateSuccess
});

const mapDispatchToProps = {
  getPlants,
  getEntity,
  updateEntity,
  setBlob,
  createEntity,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(PlantSystemUpdate);
