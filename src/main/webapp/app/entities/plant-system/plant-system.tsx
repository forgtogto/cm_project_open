import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { getSortState, JhiItemCount } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { JhiPagination } from 'app/shared/layout/pagination/pagination';

import { IRootState } from 'app/shared/reducers';
import { getEntities } from './plant-system.reducer';
import { ITEMS_PER_PAGE } from 'app/shared/util/pagination.constants';

export interface IPlantSystemProps extends StateProps, DispatchProps, RouteComponentProps<{ url: string }> {}

export const PlantSystem = (props: IPlantSystemProps) => {
  const [paginationState, setPaginationState] = useState(getSortState(props.location, ITEMS_PER_PAGE));

  const getAllEntities = () => {
    props.getEntities(paginationState.activePage - 1, paginationState.itemsPerPage, `${paginationState.sort},${paginationState.order}`);
  };

  const sortEntities = () => {
    getAllEntities();
    props.history.push(
      `${props.location.pathname}?page=${paginationState.activePage}&sort=${paginationState.sort},${paginationState.order}`
    );
  };

  useEffect(() => {
    sortEntities();
  }, [paginationState.activePage, paginationState.order, paginationState.sort]);

  const sort = p => () => {
    setPaginationState({
      ...paginationState,
      order: paginationState.order === 'asc' ? 'desc' : 'asc',
      sort: p
    });
  };

  const handlePagination = currentPage =>
    setPaginationState({
      ...paginationState,
      activePage: currentPage
    });

  const { plantSystemList, match, loading, totalItems } = props;
  return (
    <section>
      <div className="container py-4 mt-6">
        <div className="row">
          <div className="col-lg-12 mx-auto d-flex justify-content-center flex-column">
            <div className="card d-flex justify-content-center p-4 shadow-lg">
              <div className=" row">
                <div className="col-lg-8 text-left">
                  <h3 className="text-gradient text-primary">Plant System</h3>
                  <p className="mb-0 text-sm">이곳엔 Plant System의 내용을 볼 수 있습니다.</p>
                  <br />
                </div>
                <div className="col-lg-4 pt-2 text-lg-right">
                  <Link to={`${match.url}/new`} className="btn btn-primary float-right jh-create-entity " id="jh-create-entity">
                    <FontAwesomeIcon icon="plus" />
                    &nbsp; Create new Plant System
                  </Link>
                </div>
                <hr />
              </div>
              <div className="card card-plain">
                <form role="form" id="contact-form" method="post" autoComplete="off">
                  <div className="card-body pb-2 pt-lg-0">
                    <div className="table-responsive">
                      {plantSystemList && plantSystemList.length > 0 ? (
                        <table className="table table-striped  table-sm">
                          <thead>
                            <tr>
                              <th
                                className="text-center text-uppercase text-secondary text-sm font-weight-bolder opacity-7 col-1"
                                onClick={sort('plants')}
                              >
                                Plant <FontAwesomeIcon icon="sort" />
                              </th>
                              <th
                                className="text-center text-uppercase text-secondary  text-sm font-weight-bolder opacity-7 col-1"
                                onClick={sort('code')}
                              >
                                Code <FontAwesomeIcon icon="sort" />
                              </th>
                              <th
                                className=" text-center text-uppercase text-secondary text-sm  font-weight-bolder opacity-7 col-2"
                                onClick={sort('name')}
                              >
                                Name <FontAwesomeIcon icon="sort" />
                              </th>
                              <th
                                className="text-center text-uppercase text-secondary text-sm font-weight-bolder opacity-7 col-4"
                                onClick={sort('comment')}
                              >
                                Comment <FontAwesomeIcon icon="sort" />
                              </th>
                              <th />
                            </tr>
                          </thead>
                          <tbody>
                            {plantSystemList.map((plantSystem, i) => (
                              <tr key={`entity-${i}`}>
                                <td className="align-middle text-center text-sm">
                                  {plantSystem.plants ? (
                                    <Link className="text-primary" to={`plants/${plantSystem.plants.id}`}>
                                      {plantSystem.plants.code}
                                    </Link>
                                  ) : (
                                    ''
                                  )}
                                </td>
                                <td className="align-middle text-center text-sm">{plantSystem.code}</td>
                                <td className="align-middle text-center text-sm">{plantSystem.name}</td>
                                <td className="align-middle text-center text-sm">{plantSystem.comment}</td>
                                <td className="align-middle text-right">
                                  <div className="btn-group flex-btn-group-container w-auto me-2 ">
                                    <Link
                                      to={`${match.url}/${plantSystem.id}`}
                                      type="button"
                                      className="btn bg-gradient-secondary btn-sm mb-0 text-xs"
                                    >
                                      <FontAwesomeIcon icon="eye" /> View
                                    </Link>
                                    <Link
                                      to={`${match.url}/${plantSystem.id}/edit?page=${paginationState.activePage}&sort=${paginationState.sort},${paginationState.order}`}
                                      type="button"
                                      className="btn bg-gradient-light btn-sm mb-0 text-xs"
                                    >
                                      <FontAwesomeIcon icon="pencil-alt" /> Edit
                                    </Link>
                                    <Link
                                      to={`${match.url}/${plantSystem.id}/delete?page=${paginationState.activePage}&sort=${paginationState.sort},${paginationState.order}`}
                                      type="button"
                                      className="btn bg-gradient-dark btn-sm mb-0 text-xs"
                                    >
                                      <FontAwesomeIcon icon="trash" /> Delete
                                    </Link>
                                  </div>
                                </td>
                              </tr>
                            ))}
                          </tbody>
                        </table>
                      ) : (
                        !loading && <div className="alert alert-warning">No Plant System found</div>
                      )}
                    </div>
                    <div className="text-sm align-items-center">
                      <div className={plantSystemList && plantSystemList.length > 0 ? '' : 'd-none'}>
                        <JhiItemCount
                          page={paginationState.activePage}
                          total={totalItems}
                          itemsPerPage={paginationState.itemsPerPage}
                          i18nEnabled
                        />
                        <JhiPagination
                          activePage={paginationState.activePage}
                          onSelect={handlePagination}
                          maxButtons={5}
                          itemsPerPage={paginationState.itemsPerPage}
                          totalItems={props.totalItems}
                        />
                      </div>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

const mapStateToProps = ({ plantSystem }: IRootState) => ({
  plantSystemList: plantSystem.entities,
  loading: plantSystem.loading,
  totalItems: plantSystem.totalItems
});

const mapDispatchToProps = {
  getEntities
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(PlantSystem);
