import { EntityRepository, Repository } from 'typeorm';
import { EquipmentCode } from '../domain/equipment-code.entity';

@EntityRepository(EquipmentCode)
export class EquipmentCodeRepository extends Repository<EquipmentCode> {}
