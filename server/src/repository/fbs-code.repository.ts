import { EntityRepository, Repository } from 'typeorm';
import { FbsCode } from '../domain/fbs-code.entity';

@EntityRepository(FbsCode)
export class FbsCodeRepository extends Repository<FbsCode> {}
