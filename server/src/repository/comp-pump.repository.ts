import { EntityRepository, Repository } from 'typeorm';
import { CompPump } from '../domain/comp-pump.entity';

@EntityRepository(CompPump)
export class CompPumpRepository extends Repository<CompPump> {}
