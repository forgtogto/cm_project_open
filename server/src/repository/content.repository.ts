import { EntityRepository, Repository } from 'typeorm';
import { Content } from '../domain/content.entity';

@EntityRepository(Content)
export class ContentRepository extends Repository<Content> {}
