import { EntityRepository, Repository } from 'typeorm';
import { PlantSystem } from '../domain/plant-system.entity';

@EntityRepository(PlantSystem)
export class PlantSystemRepository extends Repository<PlantSystem> {}
