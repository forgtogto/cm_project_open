import { EntityRepository, Repository } from 'typeorm';
import { Plants } from '../domain/plants.entity';
import { PlantsHistoryEntity } from '../domain/history/plants.history.entity';

@EntityRepository(Plants)
export class PlantsRepository extends Repository<Plants> {}
