import { EntityRepository, Repository } from 'typeorm';
import { Requirement } from '../domain/requirement.entity';

@EntityRepository(Requirement)
export class RequirementRepository extends Repository<Requirement> {}
