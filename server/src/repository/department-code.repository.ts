import { EntityRepository, Repository } from 'typeorm';
import { DepartmentCode } from '../domain/department-code.entity';

@EntityRepository(DepartmentCode)
export class DepartmentCodeRepository extends Repository<DepartmentCode> {}
