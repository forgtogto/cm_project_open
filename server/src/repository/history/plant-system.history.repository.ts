import { EntityRepository, Repository } from 'typeorm';
import { PlantSystemHistoryEntity } from '../../domain/history/plant-system.history.entity';

@EntityRepository(PlantSystemHistoryEntity)
export class PlantSystemHistoryRepository extends Repository<PlantSystemHistoryEntity> {}
