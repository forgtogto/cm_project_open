import { EntityRepository, Repository } from 'typeorm';
import { RequirementHistoryEntity } from '../../domain/history/requirement.history.entity';

@EntityRepository(RequirementHistoryEntity)
export class RequirementHistoryRepository extends Repository<RequirementHistoryEntity> {}
