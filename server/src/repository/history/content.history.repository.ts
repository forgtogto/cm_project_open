import { EntityRepository, Repository } from 'typeorm';
import { ContentHistoryEntity } from '../../domain/history/content.history.entity';

@EntityRepository(ContentHistoryEntity)
export class ContentHistoryRepository extends Repository<ContentHistoryEntity> {}
