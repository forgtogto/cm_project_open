import { EntityRepository, Repository } from 'typeorm';
import { PlantsHistoryEntity } from '../../domain/history/plants.history.entity';


@EntityRepository(PlantsHistoryEntity)
export class PlantsHistoryRepository extends Repository<PlantsHistoryEntity> {
}
