import { EntityRepository, Repository } from 'typeorm';
import { CompPipe } from '../domain/comp-pipe.entity';

@EntityRepository(CompPipe)
export class CompPipeRepository extends Repository<CompPipe> {}
