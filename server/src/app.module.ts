import { CacheModule, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthModule } from './module/auth.module';
import { ormconfig } from './orm.config';
import { config } from './config';
import { ServeStaticModule } from '@nestjs/serve-static';
import { ContentModule } from './module/content.module';
import { PlantsModule } from './module/plants.module';
import { PlantSystemModule } from './module/plant-system.module';
import { RequirementModule } from './module/requirement.module';
import { DepartmentCodeModule } from './module/department-code.module';
import { EquipmentCodeModule } from './module/equipment-code.module';
import { FbsCodeModule } from './module/fbs-code.module';
import { CompPipeModule } from './module/comp-pipe.module';
import { CompPumpModule } from './module/comp-pump.module';
import { TypeOrmHistoryModule } from '@kittgen/nestjs-typeorm-history';
import { FileModule } from './module/file.module';
import { MulterModule } from '@nestjs/platform-express';
// jhipster-needle-add-entity-module-to-main-import - JHipster will import entity modules here, do not remove
// jhipster-needle-add-controller-module-to-main-import - JHipster will import controller modules here, do not remove
// jhipster-needle-add-service-module-to-main-import - JHipster will import service modules here, do not remove

@Module({
    imports: [
        CacheModule.register(),
        TypeOrmModule.forRoot(ormconfig),
        TypeOrmHistoryModule.register(),
        MulterModule.register({
            dest: config.getClientPath()
        }),
        ServeStaticModule.forRoot({
            rootPath: config.getClientPath(),
        }),
        AuthModule,
        FileModule,
        ContentModule,
        PlantsModule,
        PlantSystemModule,
        RequirementModule,
        DepartmentCodeModule,
        EquipmentCodeModule,
        FbsCodeModule,
        CompPipeModule,
        CompPumpModule,
        // jhipster-needle-add-entity-module-to-main - JHipster will add entity modules here, do not remove
    ],
    controllers: [
        // jhipster-needle-add-controller-module-to-main - JHipster will add controller modules here, do not remove
    ],
    providers: [
        // jhipster-needle-add-service-module-to-main - JHipster will add service modules here, do not remove
    ],
})
export class AppModule {}
