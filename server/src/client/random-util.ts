const keyList = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

export class RandomUtil {
    static generateKey(): string {
        let text = '';
        for (let i = 0; i < 20; i++)
            text += keyList.charAt(Math.floor(Math.random() * keyList.length));

        return text;
    }

}
