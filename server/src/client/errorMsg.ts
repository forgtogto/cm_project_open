export class ErrorMsg {

    static forSql(errorCode): string {

        let errMsg = '';

        switch (errorCode) {
            case '23503':
                errMsg = '해당 데이터는 타 테이블에서 사용중이에요유';
                break;
            case '23505':
                errMsg = '코드 값이 중복 되요유';
                break;
            default:
                errMsg = '무신에러여???';
        }

        return errMsg;
    }
}
