import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ContentController } from '../web/rest/content.controller';
import { ContentRepository } from '../repository/content.repository';
import { ContentService } from '../service/content.service';
import { ContentHistoryRepository } from '../repository/history/content.history.repository';

@Module({
    imports: [TypeOrmModule.forFeature([ContentRepository, ContentHistoryRepository])],
    controllers: [ContentController],
    providers: [ContentService],
    exports: [ContentService],
})
export class ContentModule {}
