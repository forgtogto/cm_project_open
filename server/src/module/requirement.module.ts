import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { RequirementController } from '../web/rest/requirement.controller';
import { RequirementRepository } from '../repository/requirement.repository';
import { RequirementService } from '../service/requirement.service';
import { RequirementHistoryRepository } from '../repository/history/requirement.history.repository';

@Module({
    imports: [TypeOrmModule.forFeature([RequirementRepository, RequirementHistoryRepository])],
    controllers: [RequirementController],
    providers: [RequirementService],
    exports: [RequirementService],
})
export class RequirementModule {}
