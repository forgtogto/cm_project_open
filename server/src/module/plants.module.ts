import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PlantsController } from '../web/rest/plants.controller';
import { PlantsRepository } from '../repository/plants.repository';
import { PlantsService } from '../service/plants.service';
import { PlantsHistoryRepository } from '../repository/history/plants.history.repository';

@Module({
    imports: [
        TypeOrmModule.forFeature([PlantsRepository, PlantsHistoryRepository]),
    ],
    controllers: [PlantsController],
    providers: [PlantsService],
    exports: [PlantsService],
})
export class PlantsModule {}
