import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { FbsCodeController } from '../web/rest/fbs-code.controller';
import { FbsCodeRepository } from '../repository/fbs-code.repository';
import { FbsCodeService } from '../service/fbs-code.service';

@Module({
    imports: [TypeOrmModule.forFeature([FbsCodeRepository])],
    controllers: [FbsCodeController],
    providers: [FbsCodeService],
    exports: [FbsCodeService],
})
export class FbsCodeModule {}
