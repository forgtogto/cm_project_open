import * as dotenv from 'dotenv';
import { Module } from '@nestjs/common';
import { AuthService } from '../service/auth.service';
import { UserModule } from './user.module';
import { PassportModule } from '@nestjs/passport';
import { JwtModule } from '@nestjs/jwt';
import { JwtStrategy } from '../security/passport.jwt.strategy';
import { UserJWTController } from '../web/rest/user.jwt.controller';
import { config } from '../config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthorityRepository } from '../repository/authority.repository';

import { AuthController } from '../web/rest/auth.controller';
import { AccountController } from '../web/rest/account.controller';
import { MailerModule } from '@nestjs-modules/mailer';
import { HandlebarsAdapter } from '@nestjs-modules/mailer/dist/adapters/handlebars.adapter';
import { MailService } from '../service/mail.service';

dotenv.config();

@Module({
    imports: [
        TypeOrmModule.forFeature([AuthorityRepository]),
        UserModule,
        PassportModule,
        JwtModule.register({
            secret: config['jhipster.security.authentication.jwt.base64-secret'],
            signOptions: { expiresIn: '300s' },
        }),
        MailerModule.forRootAsync({
            useFactory: () => ({
                transport: process.env.MAIL_SMTP,
                defaults: { form: process.env.MAIL_FROM },
                template: {
                    dir: __dirname + process.env.MAIL_TEMPLATE,
                    adapter: new HandlebarsAdapter(),
                    options: { strict: true },
                },
            }),
        }),
    ],
    controllers: [UserJWTController, AuthController, AccountController],
    providers: [AuthService, JwtStrategy, MailService],
    exports: [AuthService],
})
export class AuthModule {}
