import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CompPumpController } from '../web/rest/comp-pump.controller';
import { CompPumpRepository } from '../repository/comp-pump.repository';
import { CompPumpService } from '../service/comp-pump.service';

@Module({
    imports: [TypeOrmModule.forFeature([CompPumpRepository])],
    controllers: [CompPumpController],
    providers: [CompPumpService],
    exports: [CompPumpService],
})
export class CompPumpModule {}
