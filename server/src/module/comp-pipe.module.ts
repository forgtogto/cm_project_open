import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CompPipeController } from '../web/rest/comp-pipe.controller';
import { CompPipeRepository } from '../repository/comp-pipe.repository';
import { CompPipeService } from '../service/comp-pipe.service';

@Module({
    imports: [TypeOrmModule.forFeature([CompPipeRepository])],
    controllers: [CompPipeController],
    providers: [CompPipeService],
    exports: [CompPipeService],
})
export class CompPipeModule {}
