import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { EquipmentCodeController } from '../web/rest/equipment-code.controller';
import { EquipmentCodeRepository } from '../repository/equipment-code.repository';
import { EquipmentCodeService } from '../service/equipment-code.service';

@Module({
    imports: [TypeOrmModule.forFeature([EquipmentCodeRepository])],
    controllers: [EquipmentCodeController],
    providers: [EquipmentCodeService],
    exports: [EquipmentCodeService],
})
export class EquipmentCodeModule {}
