import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DepartmentCodeController } from '../web/rest/department-code.controller';
import { DepartmentCodeRepository } from '../repository/department-code.repository';
import { DepartmentCodeService } from '../service/department-code.service';

@Module({
    imports: [TypeOrmModule.forFeature([DepartmentCodeRepository])],
    controllers: [DepartmentCodeController],
    providers: [DepartmentCodeService],
    exports: [DepartmentCodeService],
})
export class DepartmentCodeModule {}
