import { Module } from '@nestjs/common';
import { FileController } from '../web/rest/file.controller';
import { FileService } from '../service/file.service';


@Module({
    controllers: [FileController],
    providers: [FileService]
})
export class FileModule {}
