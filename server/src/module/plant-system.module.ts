import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PlantSystemController } from '../web/rest/plant-system.controller';
import { PlantSystemRepository } from '../repository/plant-system.repository';
import { PlantSystemService } from '../service/plant-system.service';
import { PlantSystemHistoryRepository } from '../repository/history/plant-system.history.repository';

@Module({
    imports: [
        TypeOrmModule.forFeature([PlantSystemRepository, PlantSystemHistoryRepository]),
    ],
    controllers: [PlantSystemController],
    providers: [PlantSystemService],
    exports: [PlantSystemService],
})
export class PlantSystemModule {}
