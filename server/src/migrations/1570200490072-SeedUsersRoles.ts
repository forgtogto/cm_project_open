import { getRepository, MigrationInterface, QueryRunner } from 'typeorm';
import { User } from '../domain/user.entity';
import { Authority } from '../domain/authority.entity';
import { RequesterGroup } from '../domain/enumeration/requester-group';

export class SeedUsersRoles1570200490072 implements MigrationInterface {
    role1: Authority = { name: 'ROLE_ADMIN' };

    role2: Authority = { name: 'ROLE_USER' };

    user1: User = {
        login: 'system',
        password: 'system',
        name: 'System',
        address: 'Systems address',
        birth: new Date(),
        phone: '123123123',
        department: RequesterGroup.GROUP1,
        email: 'system@localhost.it',
        imageUrl: '',
        activated: true,
        langKey: 'en',
        createdBy: 'system',
        lastModifiedBy: 'system',
    };

    user2: User = {
        login: 'anonymoususer',
        password: 'anonymoususer',
        name: 'Anonymous',
        address: 'Systems address',
        birth: new Date(),
        phone: '1111111111',
        department: RequesterGroup.GROUP2,
        email: 'anonymoususer@localhost.it',
        imageUrl: '',
        activated: true,
        langKey: 'en',
        createdBy: 'system',
        lastModifiedBy: 'system',
    };

    user3: User = {
        login: 'admin',
        password: 'admin',
        name: 'Administrator',
        address: 'Systems address',
        birth: new Date(),
        phone: '2222222222',
        department: RequesterGroup.GROUP3,
        email: 'admin@localhost.it',
        imageUrl: '',
        activated: true,
        langKey: 'en',
        createdBy: 'system',
        lastModifiedBy: 'system',
    };

    user4: User = {
        login: 'user',
        password: 'user',
        name: 'User',
        address: 'Systems address',
        birth: new Date(),
        phone: '3333333333',
        department: RequesterGroup.GROUP4,
        email: 'user@localhost.it',
        imageUrl: '',
        activated: true,
        langKey: 'en',
        createdBy: 'system',
        lastModifiedBy: 'system',
    };

    // eslint-disable-next-line
	public async up(queryRunner: QueryRunner): Promise<any> {
        const authorityRepository = getRepository('tn_authority');

        const adminRole = await authorityRepository.save(this.role1);
        const userRole = await authorityRepository.save(this.role2);

        const userRepository = getRepository('tn_user');

        this.user1.authorities = [adminRole, userRole];
        this.user3.authorities = [adminRole, userRole];
        this.user4.authorities = [userRole];

        await userRepository.save([this.user1, this.user2, this.user3, this.user4]);
    }

    // eslint-disable-next-line
	public async down(queryRunner: QueryRunner): Promise<any> {}
}
