import { TypeOrmModuleOptions } from '@nestjs/typeorm';

const commonConf = {
    SYNCRONIZE: false,
    ENTITIES: [__dirname + '/domain/*.entity{.ts,.js}', __dirname + '/domain/history/*.entity{.ts,.js}'],

    MIGRATIONS: [__dirname + '/migrations/**/*{.ts,.js}'],
    CLI: {
        migrationsDir: 'src/migrations',
    },
    MIGRATIONS_RUN: true,
};

let ormconfig: TypeOrmModuleOptions = {
    name: 'default',
    type: 'postgres',
    database: '{INPUT_YOUR_DB_MANE}',
    url: 'postgresql://postgres:{INPUT_YOUR_PASSWORD}@localhost:5432/{INPUT_YOUR_DB_MANE}',
    logging: true,
    synchronize: true,
    entities: commonConf.ENTITIES ,
    migrations: commonConf.MIGRATIONS,
    cli: commonConf.CLI,
    migrationsRun: commonConf.MIGRATIONS_RUN,
};

if (process.env.NODE_ENV === 'prod') {
    ormconfig = {
        name: 'default',
        type: 'postgres',
        database: '{INPUT_YOUR_DB_MANE}',
        url: 'postgresql://postgres:{INPUT_YOUR_PASSWORD}@localhost:5432/{INPUT_YOUR_DB_MANE}',
        logging: false,
        synchronize: commonConf.SYNCRONIZE,
        entities: commonConf.ENTITIES,
        migrations: commonConf.MIGRATIONS,
        cli: commonConf.CLI,
        migrationsRun: commonConf.MIGRATIONS_RUN,
    };
}

if (process.env.NODE_ENV === 'test') {
    ormconfig = {
        name: 'default',
        type: 'sqlite',
        database: ':memory:',
        keepConnectionAlive: true,
        logging: true,
        synchronize: true,
        entities: commonConf.ENTITIES,
        migrations: commonConf.MIGRATIONS,
        cli: commonConf.CLI,
        migrationsRun: commonConf.MIGRATIONS_RUN,
    };
}

export { ormconfig };
