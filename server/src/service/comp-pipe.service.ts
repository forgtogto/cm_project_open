import { HttpException, HttpStatus, Injectable, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { FindManyOptions, FindOneOptions } from 'typeorm';
import { CompPipeDTO } from './dto/comp-pipe.dto';
import { CompPipeMapper } from './mapper/comp-pipe.mapper';
import { CompPipeRepository } from '../repository/comp-pipe.repository';

const relationshipNames = [];
relationshipNames.push('plantSystem');
relationshipNames.push('equipmentCode');

@Injectable()
export class CompPipeService {
    logger = new Logger('CompPipeService');

    constructor(
        @InjectRepository(CompPipeRepository) private compPipeRepository: CompPipeRepository,
    ) {}

    async findById(id: string): Promise<CompPipeDTO | undefined> {
        const options = { relations: relationshipNames };
        const result = await this.compPipeRepository.findOne(id, options);
        return CompPipeMapper.fromEntityToDTO(result);
    }

    async findByFields(options: FindOneOptions<CompPipeDTO>): Promise<CompPipeDTO | undefined> {
        const result = await this.compPipeRepository.findOne(options);
        return CompPipeMapper.fromEntityToDTO(result);
    }

    async findAndCount(options: FindManyOptions<CompPipeDTO>): Promise<[CompPipeDTO[], number]> {
        options.relations = relationshipNames;
        const resultList = await this.compPipeRepository.findAndCount(options);
        const compPipeDTO: CompPipeDTO[] = [];
        if (resultList && resultList[0]) {
            resultList[0].forEach(compPipe =>
                compPipeDTO.push(CompPipeMapper.fromEntityToDTO(compPipe)),
            );
            resultList[0] = compPipeDTO;
        }
        return resultList;
    }

    async save(compPipeDTO: CompPipeDTO): Promise<CompPipeDTO | undefined> {
        const entity = CompPipeMapper.fromDTOtoEntity(compPipeDTO);
        const result = await this.compPipeRepository.save(entity);
        return CompPipeMapper.fromEntityToDTO(result);
    }

    async update(compPipeDTO: CompPipeDTO): Promise<CompPipeDTO | undefined> {
        const entity = CompPipeMapper.fromDTOtoEntity(compPipeDTO);
        const result = await this.compPipeRepository.save(entity);
        return CompPipeMapper.fromEntityToDTO(result);
    }

    async deleteById(id: string): Promise<void | undefined> {
        await this.compPipeRepository.delete(id);
        const entityFind = await this.findById(id);
        if (entityFind) {
            throw new HttpException('Error, entity not deleted!', HttpStatus.NOT_FOUND);
        }
        return;
    }
}
