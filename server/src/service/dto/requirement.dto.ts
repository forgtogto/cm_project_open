/* eslint-disable @typescript-eslint/no-unused-vars */
import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, MinLength, MaxLength, Length, Min, Max, Matches } from 'class-validator';
import { BaseDTO } from './base.dto';

import { PlantSystemDTO } from './plant-system.dto';
import { ReqStatus } from '../../domain/enumeration/req-status';
import { ReqCmGrade } from '../../domain/enumeration/req-cm-grade';
import { RequesterGroup } from '../../domain/enumeration/requester-group';

import { UserDTO } from './user.dto';

/**
 * A Requirement DTO object.
 */
export class RequirementDTO extends BaseDTO {
    @IsNotEmpty()
    @ApiModelProperty({ description: 'code field' })
    code: string;

    @IsNotEmpty()
    @ApiModelProperty({ description: 'name field' })
    name: string;

    @ApiModelProperty({ description: 'comment field', required: false })
    comment: any;

    @ApiModelProperty({ enum: ReqStatus, description: 'reqStatus enum field', required: false })
    reqStatus: ReqStatus;

    @ApiModelProperty({ enum: ReqCmGrade, description: 'cmGrade enum field', required: false })
    cmGrade: ReqCmGrade;

    @ApiModelProperty({
        enum: RequesterGroup,
        description: 'requesterGroup enum field',
        required: false
    })
    requesterGroup: RequesterGroup;

    @ApiModelProperty({ type: UserDTO, description: 'user relationship' })
    users: UserDTO;

    @ApiModelProperty({ type: PlantSystemDTO, description: 'plantSystem relationship' })
    plantSystem: PlantSystemDTO;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
}
