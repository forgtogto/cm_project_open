import { Column } from 'typeorm';
import { ComSubName } from '../../domain/enumeration/com-sub-name';
import { EquipSubName } from '../../domain/enumeration/equip-sub-name';
import { ReqCmGrade } from '../../domain/enumeration/req-cm-grade';

/**
 * A DTO base objct.
 */
export class ComponentsDTO {
    id?: string;

    createdBy?: string;

    createdDate?: Date;

    lastModifiedBy?: string;

    lastModifiedDate?: Date;

    version?: string;

    code?: string;

    name?: string;

    serialNo?: string;

    filePath?: string;

    fileName?: string;

    compSubName: ComSubName;

    equipSubName: EquipSubName;

    cmGrade: ReqCmGrade;

    manufacturer?: string;

    size?: string;

    envZoneFlag?: string;
}
