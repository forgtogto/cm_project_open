import { ApiModelProperty } from '@nestjs/swagger';


export class FileDTO {

    @ApiModelProperty({ example: 'FileName', description: 'File Name', required: false })
    fileName?: string;

    @ApiModelProperty({ example: 'FileName', description: 'File Name', required: false })
    filePath?: string;

}
