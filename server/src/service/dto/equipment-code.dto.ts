/* eslint-disable @typescript-eslint/no-unused-vars */
import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, MinLength, MaxLength, Length, Min, Max, Matches } from 'class-validator';
import { BaseDTO } from './base.dto';

/**
 * A EquipmentCode DTO object.
 */
export class EquipmentCodeDTO extends BaseDTO {
    @IsNotEmpty()
    @ApiModelProperty({ description: 'code field' })
    code: string;

    @ApiModelProperty({ description: 'comment field', required: false })
    comment: any;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
}
