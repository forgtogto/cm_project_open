/* eslint-disable @typescript-eslint/no-unused-vars */
import { ApiModelProperty } from '@nestjs/swagger';
import { BaseDTO } from './base.dto';

import { PlantSystemDTO } from './plant-system.dto';
import { EquipmentCodeDTO } from './equipment-code.dto';
import { ComponentsDTO } from './components.dto';

/**
 * A CompPipe DTO object.
 */
export class CompPipeDTO extends ComponentsDTO {
    @ApiModelProperty({ description: 'pipeClass field', required: false })
    pipeClass: string;

    @ApiModelProperty({ description: 'schedule field', required: false })
    schedule: string;

    @ApiModelProperty({ description: 'thickness field', required: false })
    thickness: string;

    @ApiModelProperty({ type: PlantSystemDTO, description: 'plantSystem relationship' })
    plantSystem: PlantSystemDTO;

    @ApiModelProperty({ type: EquipmentCodeDTO, description: 'equipmentCode relationship' })
    equipmentCode: EquipmentCodeDTO;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
}
