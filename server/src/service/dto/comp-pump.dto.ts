/* eslint-disable @typescript-eslint/no-unused-vars */
import { ApiModelProperty } from '@nestjs/swagger';
import { BaseDTO } from './base.dto';

import { EquipmentCodeDTO } from './equipment-code.dto';
import { PlantSystemDTO } from './plant-system.dto';
import { ComponentsDTO } from './components.dto';

/**
 * A CompPump DTO object.
 */
export class CompPumpDTO extends ComponentsDTO {
    @ApiModelProperty({ description: 'dePressure field', required: false })
    dePressure: string;

    @ApiModelProperty({ description: 'opPressure field', required: false })
    opPressure: string;

    @ApiModelProperty({ description: 'quantity field', required: false })
    quantity: string;

    @ApiModelProperty({ type: EquipmentCodeDTO, description: 'equipmentCode relationship' })
    equipmentCode: EquipmentCodeDTO;

    @ApiModelProperty({ type: PlantSystemDTO, description: 'plantSystem relationship' })
    plantSystem: PlantSystemDTO;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
}
