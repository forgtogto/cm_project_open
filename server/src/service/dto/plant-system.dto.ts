/* eslint-disable @typescript-eslint/no-unused-vars */
import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, MinLength, MaxLength, Length, Min, Max, Matches } from 'class-validator';
import { BaseDTO } from './base.dto';

import { PlantsDTO } from './plants.dto';

/**
 * A PlantSystem DTO object.
 */
export class PlantSystemDTO extends BaseDTO {
    @IsNotEmpty()
    @ApiModelProperty({ description: 'code field' })
    code: string;

    @IsNotEmpty()
    @ApiModelProperty({ description: 'name field' })
    name: string;

    @ApiModelProperty({ description: 'comment field', required: false })
    comment: any;

    @ApiModelProperty({ type: PlantsDTO, description: 'plants relationship' })
    plants: PlantsDTO;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
}
