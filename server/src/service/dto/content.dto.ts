/* eslint-disable @typescript-eslint/no-unused-vars */
import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, MinLength, MaxLength, Length, Min, Max, Matches } from 'class-validator';
import { BaseDTO } from './base.dto';

import { PlantSystemDTO } from './plant-system.dto';
import { DepartmentCodeDTO } from './department-code.dto';

/**
 * A Content DTO object.
 */
export class ContentDTO extends BaseDTO {
    @ApiModelProperty({ description: 'version field', required: false })
    version: string;

    @IsNotEmpty()
    @ApiModelProperty({ description: 'code field' })
    code: string;

    @IsNotEmpty()
    @ApiModelProperty({ description: 'name field' })
    name: string;

    @ApiModelProperty({ description: 'serialNo field', required: false })
    serialNo: string;

    @ApiModelProperty({ description: 'filePath field', required: false })
    filePath: string;

    @ApiModelProperty({ description: 'fileName field', required: false })
    fileName: string;

    @ApiModelProperty({ type: PlantSystemDTO, description: 'plantSystem relationship' })
    plantSystem: PlantSystemDTO;

    @ApiModelProperty({ type: DepartmentCodeDTO, description: 'departmentCode relationship' })
    departmentCode: DepartmentCodeDTO;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
}
