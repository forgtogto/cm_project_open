import { Injectable, HttpException, HttpStatus, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { FindManyOptions, FindOneOptions } from 'typeorm';
import { EquipmentCodeDTO } from './dto/equipment-code.dto';
import { EquipmentCodeMapper } from './mapper/equipment-code.mapper';
import { EquipmentCodeRepository } from '../repository/equipment-code.repository';

const relationshipNames = [];

@Injectable()
export class EquipmentCodeService {
    logger = new Logger('EquipmentCodeService');

    constructor(
        @InjectRepository(EquipmentCodeRepository)
        private equipmentCodeRepository: EquipmentCodeRepository,
    ) {}

    async findById(id: string): Promise<EquipmentCodeDTO | undefined> {
        const options = { relations: relationshipNames };
        const result = await this.equipmentCodeRepository.findOne(id, options);
        return EquipmentCodeMapper.fromEntityToDTO(result);
    }

    async findByFields(
        options: FindOneOptions<EquipmentCodeDTO>,
    ): Promise<EquipmentCodeDTO | undefined> {
        const result = await this.equipmentCodeRepository.findOne(options);
        return EquipmentCodeMapper.fromEntityToDTO(result);
    }

    async findAndCount(
        options: FindManyOptions<EquipmentCodeDTO>,
    ): Promise<[EquipmentCodeDTO[], number]> {
        options.relations = relationshipNames;
        const resultList = await this.equipmentCodeRepository.findAndCount(options);
        const equipmentCodeDTO: EquipmentCodeDTO[] = [];
        if (resultList && resultList[0]) {
            resultList[0].forEach(equipmentCode =>
                equipmentCodeDTO.push(EquipmentCodeMapper.fromEntityToDTO(equipmentCode)),
            );
            resultList[0] = equipmentCodeDTO;
        }
        return resultList;
    }

    async save(equipmentCodeDTO: EquipmentCodeDTO): Promise<EquipmentCodeDTO | undefined> {
        const entity = EquipmentCodeMapper.fromDTOtoEntity(equipmentCodeDTO);
        const result = await this.equipmentCodeRepository.save(entity);
        return EquipmentCodeMapper.fromEntityToDTO(result);
    }

    async update(equipmentCodeDTO: EquipmentCodeDTO): Promise<EquipmentCodeDTO | undefined> {
        const entity = EquipmentCodeMapper.fromDTOtoEntity(equipmentCodeDTO);
        const result = await this.equipmentCodeRepository.save(entity);
        return EquipmentCodeMapper.fromEntityToDTO(result);
    }

    async deleteById(id: string): Promise<void | undefined> {
        await this.equipmentCodeRepository.delete(id);
        const entityFind = await this.findById(id);
        if (entityFind) {
            throw new HttpException('Error, entity not deleted!', HttpStatus.NOT_FOUND);
        }
        return;
    }
}
