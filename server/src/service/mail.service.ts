import * as dotenv from 'dotenv';
import { Injectable } from '@nestjs/common';
import { MailerService } from '@nestjs-modules/mailer';
import { UserDTO } from './dto/user.dto';

dotenv.config();

@Injectable()
export class MailService {
    constructor(private readonly mailerService: MailerService) {
    }

    async sendPasswordResetMail(user: UserDTO) {
        //(user, 'passwordResetEmail', 'email.reset.title');

        await this.mailerService
            .sendMail({
                to: user.email,
                from: process.env.MAIL_FROM,
                subject: '패스워드 리셋 이메일',
                template: 'passwordResetEmail',
                context: {
                    username: user.name,
                    url: 'http://localhost:9000/account/reset/finish?key=' + user.resetKey
                }
            })
            .then((success) => {
                console.log(success);
            })
            .catch((err) => {
                console.log(err);
            });
    }

    async sendActivationEmail(user: UserDTO) {
        await this.mailerService
            .sendMail({
                to: user.email,
                from: process.env.MAIL_FROM,
                subject: '이메일을 인증하세요.',
                template: 'activationEmail',
                context: {
                    username: user.name,
                    url: 'http://localhost:9000/account/activate?key=' + user.activationKey
                }
            })
            .then((success) => {
                console.log(success);
            })
            .catch((err) => {
                console.log(err);
            });
    }
}
