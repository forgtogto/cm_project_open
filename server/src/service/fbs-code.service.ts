import { Injectable, HttpException, HttpStatus, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { FindManyOptions, FindOneOptions } from 'typeorm';
import { FbsCodeDTO } from '../service/dto/fbs-code.dto';
import { FbsCodeMapper } from '../service/mapper/fbs-code.mapper';
import { FbsCodeRepository } from '../repository/fbs-code.repository';

const relationshipNames = [];

@Injectable()
export class FbsCodeService {
    logger = new Logger('FbsCodeService');

    constructor(
        @InjectRepository(FbsCodeRepository) private fbsCodeRepository: FbsCodeRepository,
    ) {}

    async findById(id: string): Promise<FbsCodeDTO | undefined> {
        const options = { relations: relationshipNames };
        const result = await this.fbsCodeRepository.findOne(id, options);
        return FbsCodeMapper.fromEntityToDTO(result);
    }

    async findByFields(options: FindOneOptions<FbsCodeDTO>): Promise<FbsCodeDTO | undefined> {
        const result = await this.fbsCodeRepository.findOne(options);
        return FbsCodeMapper.fromEntityToDTO(result);
    }

    async findAndCount(options: FindManyOptions<FbsCodeDTO>): Promise<[FbsCodeDTO[], number]> {
        options.relations = relationshipNames;
        const resultList = await this.fbsCodeRepository.findAndCount(options);
        const fbsCodeDTO: FbsCodeDTO[] = [];
        if (resultList && resultList[0]) {
            resultList[0].forEach(fbsCode =>
                fbsCodeDTO.push(FbsCodeMapper.fromEntityToDTO(fbsCode)),
            );
            resultList[0] = fbsCodeDTO;
        }
        return resultList;
    }

    async save(fbsCodeDTO: FbsCodeDTO): Promise<FbsCodeDTO | undefined> {
        const entity = FbsCodeMapper.fromDTOtoEntity(fbsCodeDTO);
        const result = await this.fbsCodeRepository.save(entity);
        return FbsCodeMapper.fromEntityToDTO(result);
    }

    async update(fbsCodeDTO: FbsCodeDTO): Promise<FbsCodeDTO | undefined> {
        const entity = FbsCodeMapper.fromDTOtoEntity(fbsCodeDTO);
        const result = await this.fbsCodeRepository.save(entity);
        return FbsCodeMapper.fromEntityToDTO(result);
    }

    async deleteById(id: string): Promise<void | undefined> {
        await this.fbsCodeRepository.delete(id);
        const entityFind = await this.findById(id);
        if (entityFind) {
            throw new HttpException('Error, entity not deleted!', HttpStatus.NOT_FOUND);
        }
        return;
    }
}
