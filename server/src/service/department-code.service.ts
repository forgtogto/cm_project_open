import { Injectable, HttpException, HttpStatus, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { FindManyOptions, FindOneOptions } from 'typeorm';
import { DepartmentCodeDTO } from './dto/department-code.dto';
import { DepartmentCodeMapper } from './mapper/department-code.mapper';
import { DepartmentCodeRepository } from '../repository/department-code.repository';
import { ErrorMsg } from '../client/errorMsg';

const relationshipNames = [];

@Injectable()
export class DepartmentCodeService {
    logger = new Logger('DepartmentCodeService');

    constructor(
        @InjectRepository(DepartmentCodeRepository)
        private departmentCodeRepository: DepartmentCodeRepository
    ) {
    }

    async findById(id: string): Promise<DepartmentCodeDTO | undefined> {
        const options = { relations: relationshipNames };
        const result = await this.departmentCodeRepository.findOne(id, options);
        return DepartmentCodeMapper.fromEntityToDTO(result);
    }

    async findByFields(
        options: FindOneOptions<DepartmentCodeDTO>
    ): Promise<DepartmentCodeDTO | undefined> {
        const result = await this.departmentCodeRepository.findOne(options);
        return DepartmentCodeMapper.fromEntityToDTO(result);
    }

    async findAndCount(
        options: FindManyOptions<DepartmentCodeDTO>
    ): Promise<[DepartmentCodeDTO[], number]> {
        options.relations = relationshipNames;
        const resultList = await this.departmentCodeRepository.findAndCount(options);
        const departmentCodeDTO: DepartmentCodeDTO[] = [];
        if (resultList && resultList[0]) {
            resultList[0].forEach(departmentCode =>
                departmentCodeDTO.push(DepartmentCodeMapper.fromEntityToDTO(departmentCode))
            );
            resultList[0] = departmentCodeDTO;
        }
        return resultList;
    }

    async save(departmentCodeDTO: DepartmentCodeDTO): Promise<DepartmentCodeDTO | undefined> {
        const entity = DepartmentCodeMapper.fromDTOtoEntity(departmentCodeDTO);
        const result = await this.departmentCodeRepository.save(entity).catch(err => {
            throw new HttpException(ErrorMsg.forSql(err.code), HttpStatus.BAD_REQUEST);
        });
        return DepartmentCodeMapper.fromEntityToDTO(result);
    }

    async update(departmentCodeDTO: DepartmentCodeDTO): Promise<DepartmentCodeDTO | undefined> {
        const entity = DepartmentCodeMapper.fromDTOtoEntity(departmentCodeDTO);
        const result = await this.departmentCodeRepository.save(entity).catch(err => {
            throw new HttpException(ErrorMsg.forSql(err.code), HttpStatus.BAD_REQUEST);
        });
        return DepartmentCodeMapper.fromEntityToDTO(result);
    }

    async deleteById(id: string): Promise<void | undefined> {
        await this.departmentCodeRepository.delete(id).catch(err => {
            throw new HttpException(ErrorMsg.forSql(err.code), HttpStatus.BAD_REQUEST);
        });
        const entityFind = await this.findById(id);
        if (entityFind) {
            throw new HttpException('Error, entity not deleted!', HttpStatus.NOT_FOUND);
        }
        return;
    }
}
