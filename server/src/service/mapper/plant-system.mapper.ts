import { PlantSystem } from '../../domain/plant-system.entity';
import { PlantSystemDTO } from '../dto/plant-system.dto';

/**
 * A PlantSystem mapper object.
 */
export class PlantSystemMapper {
    static fromDTOtoEntity(entityDTO: PlantSystemDTO): PlantSystem {
        if (!entityDTO) {
            return;
        }
        const entity = new PlantSystem();
        const fields = Object.getOwnPropertyNames(entityDTO);
        fields.forEach(field => {
            entity[field] = entityDTO[field];
        });
        return entity;
    }

    static fromEntityToDTO(entity: PlantSystem): PlantSystemDTO {
        if (!entity) {
            return;
        }
        const entityDTO = new PlantSystemDTO();

        const fields = Object.getOwnPropertyNames(entity);

        fields.forEach(field => {
            entityDTO[field] = entity[field];
        });

        return entityDTO;
    }
}
