import { Content } from '../../domain/content.entity';
import { ContentDTO } from '../dto/content.dto';

/**
 * A Content mapper object.
 */
export class ContentMapper {
    static fromDTOtoEntity(entityDTO: ContentDTO): Content {
        if (!entityDTO) {
            return;
        }
        const entity = new Content();
        const fields = Object.getOwnPropertyNames(entityDTO);
        fields.forEach(field => {
            entity[field] = entityDTO[field];
        });
        return entity;
    }

    static fromEntityToDTO(entity: Content): ContentDTO {
        if (!entity) {
            return;
        }
        const entityDTO = new ContentDTO();

        const fields = Object.getOwnPropertyNames(entity);

        fields.forEach(field => {
            entityDTO[field] = entity[field];
        });

        return entityDTO;
    }
}
