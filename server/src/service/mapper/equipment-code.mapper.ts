import { EquipmentCode } from '../../domain/equipment-code.entity';
import { EquipmentCodeDTO } from '../dto/equipment-code.dto';

/**
 * A EquipmentCode mapper object.
 */
export class EquipmentCodeMapper {
    static fromDTOtoEntity(entityDTO: EquipmentCodeDTO): EquipmentCode {
        if (!entityDTO) {
            return;
        }
        const entity = new EquipmentCode();
        const fields = Object.getOwnPropertyNames(entityDTO);
        fields.forEach(field => {
            entity[field] = entityDTO[field];
        });
        return entity;
    }

    static fromEntityToDTO(entity: EquipmentCode): EquipmentCodeDTO {
        if (!entity) {
            return;
        }
        const entityDTO = new EquipmentCodeDTO();

        const fields = Object.getOwnPropertyNames(entity);

        fields.forEach(field => {
            entityDTO[field] = entity[field];
        });

        return entityDTO;
    }
}
