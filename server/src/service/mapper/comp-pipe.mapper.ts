import { CompPipe } from '../../domain/comp-pipe.entity';
import { CompPipeDTO } from '../dto/comp-pipe.dto';

/**
 * A CompPipe mapper object.
 */
export class CompPipeMapper {
    static fromDTOtoEntity(entityDTO: CompPipeDTO): CompPipe {
        if (!entityDTO) {
            return;
        }
        const entity = new CompPipe();
        const fields = Object.getOwnPropertyNames(entityDTO);
        fields.forEach(field => {
            entity[field] = entityDTO[field];
        });
        return entity;
    }

    static fromEntityToDTO(entity: CompPipe): CompPipeDTO {
        if (!entity) {
            return;
        }
        const entityDTO = new CompPipeDTO();

        const fields = Object.getOwnPropertyNames(entity);

        fields.forEach(field => {
            entityDTO[field] = entity[field];
        });

        return entityDTO;
    }
}
