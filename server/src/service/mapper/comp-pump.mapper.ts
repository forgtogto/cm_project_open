import { CompPump } from '../../domain/comp-pump.entity';
import { CompPumpDTO } from '../dto/comp-pump.dto';

/**
 * A CompPump mapper object.
 */
export class CompPumpMapper {
    static fromDTOtoEntity(entityDTO: CompPumpDTO): CompPump {
        if (!entityDTO) {
            return;
        }
        const entity = new CompPump();
        const fields = Object.getOwnPropertyNames(entityDTO);
        fields.forEach(field => {
            entity[field] = entityDTO[field];
        });
        return entity;
    }

    static fromEntityToDTO(entity: CompPump): CompPumpDTO {
        if (!entity) {
            return;
        }
        const entityDTO = new CompPumpDTO();

        const fields = Object.getOwnPropertyNames(entity);

        fields.forEach(field => {
            entityDTO[field] = entity[field];
        });

        return entityDTO;
    }
}
