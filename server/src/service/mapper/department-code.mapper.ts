import { DepartmentCode } from '../../domain/department-code.entity';
import { DepartmentCodeDTO } from '../dto/department-code.dto';

/**
 * A DepartmentCode mapper object.
 */
export class DepartmentCodeMapper {
    static fromDTOtoEntity(entityDTO: DepartmentCodeDTO): DepartmentCode {
        if (!entityDTO) {
            return;
        }
        const entity = new DepartmentCode();
        const fields = Object.getOwnPropertyNames(entityDTO);
        fields.forEach(field => {
            entity[field] = entityDTO[field];
        });
        return entity;
    }

    static fromEntityToDTO(entity: DepartmentCode): DepartmentCodeDTO {
        if (!entity) {
            return;
        }
        const entityDTO = new DepartmentCodeDTO();

        const fields = Object.getOwnPropertyNames(entity);

        fields.forEach(field => {
            entityDTO[field] = entity[field];
        });

        return entityDTO;
    }
}
