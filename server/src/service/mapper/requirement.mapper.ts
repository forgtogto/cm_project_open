import { Requirement } from '../../domain/requirement.entity';
import { RequirementDTO } from '../dto/requirement.dto';

/**
 * A Requirement mapper object.
 */
export class RequirementMapper {
    static fromDTOtoEntity(entityDTO: RequirementDTO): Requirement {
        if (!entityDTO) {
            return;
        }
        const entity = new Requirement();
        const fields = Object.getOwnPropertyNames(entityDTO);
        fields.forEach(field => {
            entity[field] = entityDTO[field];
        });

        return entity;
    }

    static fromEntityToDTO(entity: Requirement): RequirementDTO {
        if (!entity) {
            return;
        }
        const entityDTO = new RequirementDTO();

        const fields = Object.getOwnPropertyNames(entity);

        fields.forEach(field => {
            entityDTO[field] = entity[field];
        });

        return entityDTO;
    }
}
