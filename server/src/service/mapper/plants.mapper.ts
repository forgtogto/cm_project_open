import { Plants } from '../../domain/plants.entity';
import { PlantsDTO } from '../dto/plants.dto';

/**
 * A Plants mapper object.
 */
export class PlantsMapper {
    static fromDTOtoEntity(entityDTO: PlantsDTO): Plants {
        if (!entityDTO) {
            return;
        }
        const entity = new Plants();
        const fields = Object.getOwnPropertyNames(entityDTO);
        fields.forEach(field => {
            entity[field] = entityDTO[field];
        });
        return entity;
    }

    static fromEntityToDTO(entity: Plants): PlantsDTO {
        if (!entity) {
            return;
        }
        const entityDTO = new PlantsDTO();

        const fields = Object.getOwnPropertyNames(entity);

        fields.forEach(field => {
            entityDTO[field] = entity[field];
        });

        return entityDTO;
    }
}
