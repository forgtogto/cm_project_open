import { FbsCode } from '../../domain/fbs-code.entity';
import { FbsCodeDTO } from '../dto/fbs-code.dto';

/**
 * A FbsCode mapper object.
 */
export class FbsCodeMapper {
    static fromDTOtoEntity(entityDTO: FbsCodeDTO): FbsCode {
        if (!entityDTO) {
            return;
        }
        const entity = new FbsCode();
        const fields = Object.getOwnPropertyNames(entityDTO);
        fields.forEach(field => {
            entity[field] = entityDTO[field];
        });
        return entity;
    }

    static fromEntityToDTO(entity: FbsCode): FbsCodeDTO {
        if (!entity) {
            return;
        }
        const entityDTO = new FbsCodeDTO();

        const fields = Object.getOwnPropertyNames(entity);

        fields.forEach(field => {
            entityDTO[field] = entity[field];
        });

        return entityDTO;
    }
}
