import { Injectable, HttpException, HttpStatus, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { FindManyOptions, FindOneOptions } from 'typeorm';
import { ContentDTO } from './dto/content.dto';
import { ContentMapper } from './mapper/content.mapper';
import { ContentRepository } from '../repository/content.repository';
import { ContentHistoryRepository } from '../repository/history/content.history.repository';
import { RequirementDTO } from './dto/requirement.dto';
import { RequirementMapper } from './mapper/requirement.mapper';

const relationshipNames = [];
relationshipNames.push('plantSystem');
relationshipNames.push('departmentCode');

@Injectable()
export class ContentService {
    logger = new Logger('ContentService');

    constructor(
        @InjectRepository(ContentRepository)
        private contentRepository: ContentRepository,
        @InjectRepository(ContentHistoryRepository)
        private contentHistoryRepository: ContentHistoryRepository
    ) {}

    async findById(id: string): Promise<ContentDTO | undefined> {
        const options = { relations: relationshipNames };
        const result = await this.contentRepository.findOne(id, options);
        return ContentMapper.fromEntityToDTO(result);
    }

    async findByFields(options: FindOneOptions<ContentDTO>): Promise<ContentDTO | undefined> {
        const result = await this.contentRepository.findOne(options);
        return ContentMapper.fromEntityToDTO(result);
    }

    async findByHistory(id: string): Promise<ContentDTO[]> {
        const resultList = await this.contentHistoryRepository.find({
            where: { historyId: id }, order: { id: 'DESC' }
        });
        const contentDTO: ContentDTO[] = [];
        resultList.forEach(content => {
                // if (requirement.id % 2 == 0) {
            contentDTO.push(ContentMapper.fromEntityToDTO(content.payload));
                // }
            }
        );
        return contentDTO;
    }


    async findAndCount(options: FindManyOptions<ContentDTO>): Promise<[ContentDTO[], number]> {
        options.relations = relationshipNames;
        const resultList = await this.contentRepository.findAndCount(options);
        const contentDTO: ContentDTO[] = [];
        if (resultList && resultList[0]) {
            resultList[0].forEach(content =>
                contentDTO.push(ContentMapper.fromEntityToDTO(content)),
            );
            resultList[0] = contentDTO;
        }
        return resultList;
    }

    async save(contentDTO: ContentDTO, userLogin: string): Promise<ContentDTO | undefined> {
        const entity = ContentMapper.fromDTOtoEntity(contentDTO);
        entity.createdDate = new Date();
        entity.createdBy = userLogin;

        const result = await this.contentRepository.save(entity);
        return ContentMapper.fromEntityToDTO(result);
    }

    async update(contentDTO: ContentDTO, userLogin: string): Promise<ContentDTO | undefined> {
        const entity = ContentMapper.fromDTOtoEntity(contentDTO);
        entity.lastModifiedDate = new Date();
        entity.lastModifiedBy = userLogin;
        const result = await this.contentRepository.save(entity);
        return ContentMapper.fromEntityToDTO(result);
    }

    async deleteById(id: string): Promise<void | undefined> {
        await this.contentRepository.delete(id);
        const entityFind = await this.findById(id);
        if (entityFind) {
            throw new HttpException('Error, entity not deleted!', HttpStatus.NOT_FOUND);
        }
        return;
    }
}
