import { HttpException, HttpStatus, Injectable, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { FindManyOptions, FindOneOptions } from 'typeorm';
import { RequirementDTO } from './dto/requirement.dto';
import { RequirementMapper } from './mapper/requirement.mapper';
import { RequirementRepository } from '../repository/requirement.repository';
import { ErrorMsg } from '../client/errorMsg';
import { RequirementHistoryRepository } from '../repository/history/requirement.history.repository';

const relationshipNames = [];
relationshipNames.push('users');
relationshipNames.push('plantSystem');

@Injectable()
export class RequirementService {
    logger = new Logger('RequirementService');

    constructor(
        @InjectRepository(RequirementRepository)
        private requirementRepository: RequirementRepository,
        @InjectRepository(RequirementHistoryRepository)
        private requirementHistoryRepository: RequirementHistoryRepository
    ) {
    }

    async findById(id: string): Promise<RequirementDTO | undefined> {
        const options = { relations: relationshipNames };
        const result = await this.requirementRepository.findOne(id, options);
        return RequirementMapper.fromEntityToDTO(result);
    }

    async findByHistory(id: string): Promise<RequirementDTO[]> {
        const resultList = await this.requirementHistoryRepository.find({
            where: { historyId: id }, order: { id: 'DESC' }
        });
        const requirementDTO: RequirementDTO[] = [];
        resultList.forEach(requirement => {
                // if (requirement.id % 2 == 0) {
                requirementDTO.push(RequirementMapper.fromEntityToDTO(requirement.payload));
                // }
            }
        );
        return requirementDTO;
    }


    async findByFields(
        options: FindOneOptions<RequirementDTO>
    ): Promise<RequirementDTO | undefined> {
        const result = await this.requirementRepository.findOne(options);
        return RequirementMapper.fromEntityToDTO(result);
    }

    async findAndCount(
        options: FindManyOptions<RequirementDTO>
    ): Promise<[RequirementDTO[], number]> {
        options.relations = relationshipNames;
        const resultList = await this.requirementRepository.findAndCount(options);
        const requirementDTO: RequirementDTO[] = [];
        if (resultList && resultList[0]) {
            resultList[0].forEach(requirement =>
                requirementDTO.push(RequirementMapper.fromEntityToDTO(requirement))
            );
            resultList[0] = requirementDTO;
        }
        return resultList;
    }

    async save(requirementDTO: RequirementDTO, userLogin: string): Promise<RequirementDTO | undefined> {
        const entity = RequirementMapper.fromDTOtoEntity(requirementDTO);
        entity.createdDate = new Date();
        entity.createdBy = userLogin;

        const result = await this.requirementRepository.save(entity).catch(err => {
            throw new HttpException(ErrorMsg.forSql(err.code), HttpStatus.BAD_REQUEST);
        });
        return RequirementMapper.fromEntityToDTO(result);
    }

    async update(requirementDTO: RequirementDTO, userLogin: string): Promise<RequirementDTO | undefined> {
        const entity = RequirementMapper.fromDTOtoEntity(requirementDTO);
        entity.lastModifiedDate = new Date();
        entity.lastModifiedBy = userLogin;

        const result = await this.requirementRepository.save(entity).catch(err => {
            throw new HttpException(ErrorMsg.forSql(err.code), HttpStatus.BAD_REQUEST);
        });
        return RequirementMapper.fromEntityToDTO(result);
    }

    async deleteById(id: string): Promise<void | undefined> {
        await this.requirementRepository.delete(id).catch(err => {
            throw new HttpException(ErrorMsg.forSql(err.code), HttpStatus.BAD_REQUEST);
        });
        const entityFind = await this.findById(id);
        if (entityFind) {
            throw new HttpException('Error, entity not deleted!', HttpStatus.NOT_FOUND);
        }
        return;
    }
}
