import { Injectable, HttpException, HttpStatus, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { FindManyOptions, FindOneOptions } from 'typeorm';
import { CompPumpDTO } from './dto/comp-pump.dto';
import { CompPumpMapper } from './mapper/comp-pump.mapper';
import { CompPumpRepository } from '../repository/comp-pump.repository';

const relationshipNames = [];
relationshipNames.push('equipmentCode');
relationshipNames.push('plantSystem');

@Injectable()
export class CompPumpService {
    logger = new Logger('CompPumpService');

    constructor(
        @InjectRepository(CompPumpRepository) private compPumpRepository: CompPumpRepository,
    ) {}

    async findById(id: string): Promise<CompPumpDTO | undefined> {
        const options = { relations: relationshipNames };
        const result = await this.compPumpRepository.findOne(id, options);
        return CompPumpMapper.fromEntityToDTO(result);
    }

    async findByFields(options: FindOneOptions<CompPumpDTO>): Promise<CompPumpDTO | undefined> {
        const result = await this.compPumpRepository.findOne(options);
        return CompPumpMapper.fromEntityToDTO(result);
    }

    async findAndCount(options: FindManyOptions<CompPumpDTO>): Promise<[CompPumpDTO[], number]> {
        options.relations = relationshipNames;
        const resultList = await this.compPumpRepository.findAndCount(options);
        const compPumpDTO: CompPumpDTO[] = [];
        if (resultList && resultList[0]) {
            resultList[0].forEach(compPump =>
                compPumpDTO.push(CompPumpMapper.fromEntityToDTO(compPump)),
            );
            resultList[0] = compPumpDTO;
        }
        return resultList;
    }

    async save(compPumpDTO: CompPumpDTO): Promise<CompPumpDTO | undefined> {
        const entity = CompPumpMapper.fromDTOtoEntity(compPumpDTO);
        const result = await this.compPumpRepository.save(entity);
        return CompPumpMapper.fromEntityToDTO(result);
    }

    async update(compPumpDTO: CompPumpDTO): Promise<CompPumpDTO | undefined> {
        const entity = CompPumpMapper.fromDTOtoEntity(compPumpDTO);
        const result = await this.compPumpRepository.save(entity);
        return CompPumpMapper.fromEntityToDTO(result);
    }

    async deleteById(id: string): Promise<void | undefined> {
        await this.compPumpRepository.delete(id);
        const entityFind = await this.findById(id);
        if (entityFind) {
            throw new HttpException('Error, entity not deleted!', HttpStatus.NOT_FOUND);
        }
        return;
    }
}
