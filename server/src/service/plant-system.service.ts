import { HttpException, HttpStatus, Injectable, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { FindManyOptions, FindOneOptions } from 'typeorm';
import { PlantSystemDTO } from './dto/plant-system.dto';
import { PlantSystemMapper } from './mapper/plant-system.mapper';
import { PlantSystemRepository } from '../repository/plant-system.repository';
import { ErrorMsg } from '../client/errorMsg';
import { PlantSystemHistoryRepository } from '../repository/history/plant-system.history.repository';

const relationshipNames = [];
relationshipNames.push('plants');

@Injectable()
export class PlantSystemService {
    logger = new Logger('PlantSystemService');

    constructor(
        @InjectRepository(PlantSystemRepository)
        private plantSystemRepository: PlantSystemRepository,
        @InjectRepository(PlantSystemHistoryRepository) private plantSystemHisRepository: PlantSystemHistoryRepository
    ) {
    }

    async findById(id: string): Promise<PlantSystemDTO | undefined> {
        const options = { relations: relationshipNames };
        const result = await this.plantSystemRepository.findOne(id, options);
        return PlantSystemMapper.fromEntityToDTO(result);
    }


    async findByHistory(id: string): Promise<PlantSystemDTO[]> {
        const resultList = await this.plantSystemHisRepository.find({
            where: { historyId: id } , order: {id:  "DESC" }
        });
        const plantSystemDTO: PlantSystemDTO[] = [];
        resultList.forEach(plantSystem => {
               // if (plantSystem.id % 2 == 0) {
                    plantSystemDTO.push(PlantSystemMapper.fromEntityToDTO(plantSystem.payload));
               // }
            }
        );
        return plantSystemDTO;
    }

    async findByFields(
        options: FindOneOptions<PlantSystemDTO>
    ): Promise<PlantSystemDTO | undefined> {
        const result = await this.plantSystemRepository.findOne(options);
        return PlantSystemMapper.fromEntityToDTO(result);
    }

    async findAndCount(
        options: FindManyOptions<PlantSystemDTO>
    ): Promise<[PlantSystemDTO[], number]> {
        options.relations = relationshipNames;
        const resultList = await this.plantSystemRepository.findAndCount(options);
        const plantSystemDTO: PlantSystemDTO[] = [];
        if (resultList && resultList[0]) {
            resultList[0].forEach(plantSystem =>
                plantSystemDTO.push(PlantSystemMapper.fromEntityToDTO(plantSystem))
            );
            resultList[0] = plantSystemDTO;
        }
        return resultList;
    }

    async save(plantSystemDTO: PlantSystemDTO, user: string): Promise<PlantSystemDTO | undefined> {


        const entity = PlantSystemMapper.fromDTOtoEntity(plantSystemDTO);
        entity.createdDate = new Date();
        entity.createdBy = user;
        const result = await this.plantSystemRepository.save(entity).catch(err => {
            throw new HttpException(ErrorMsg.forSql(err.code), HttpStatus.BAD_REQUEST);
        });
        return PlantSystemMapper.fromEntityToDTO(result);
    }

    async update(plantSystemDTO: PlantSystemDTO, user: string): Promise<PlantSystemDTO | undefined> {
        const entity = PlantSystemMapper.fromDTOtoEntity(plantSystemDTO);
        entity.lastModifiedDate = new Date();
        entity.lastModifiedBy = user;
        const result = await this.plantSystemRepository.save(entity).catch(err => {
            throw new HttpException(ErrorMsg.forSql(err.code), HttpStatus.BAD_REQUEST);
        });
        return PlantSystemMapper.fromEntityToDTO(result);
    }

    async deleteById(id: string): Promise<void | undefined> {
        await this.plantSystemRepository.delete(id).catch(err => {
            throw new HttpException(ErrorMsg.forSql(err.code), HttpStatus.BAD_REQUEST);
        });
        const entityFind = await this.findById(id);
        if (entityFind) {
            throw new HttpException('Error, entity not deleted!', HttpStatus.NOT_FOUND);
        }
        return;
    }
}
