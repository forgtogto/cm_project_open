import { HttpException, HttpStatus, Injectable, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { JwtService } from '@nestjs/jwt';
import { UserLoginDTO } from './dto/user-login.dto';
import { Payload } from '../security/payload.interface';
import { AuthorityRepository } from '../repository/authority.repository';
import { UserService } from './user.service';
import { UserDTO } from './dto/user.dto';
import { RandomUtil } from '../client/random-util';
import { MailService } from './mail.service';
import { MoreThanOrEqual } from 'typeorm';

@Injectable()
export class AuthService {
    logger = new Logger('AuthService');

    constructor(
        private readonly jwtService: JwtService,
        @InjectRepository(AuthorityRepository) private authorityRepository: AuthorityRepository,
        private userService: UserService,
        private mailService: MailService
    ) {
    }

    async login(userLogin: UserLoginDTO): Promise<any> {
        const loginUserName = userLogin.username;
        const loginPassword = userLogin.password;

        const userFind = await this.userService.findByFields({
            where: { login: loginUserName, password: loginPassword }
        });
        if (!userFind) {
            throw new HttpException('로그인 정보가 유효하지 않습니다!', HttpStatus.BAD_REQUEST);
        }

        if (userFind && !userFind.activated) {
            throw new HttpException('계정정보가 활성화되지 않았습니다. 이메일을 확인하세요!', HttpStatus.BAD_REQUEST);
        }

        const user = await this.findUserWithAuthById(userFind.id);

        const payload: Payload = {
            id: user.id,
            username: user.login,
            authorities: user.authorities
        };

        /* eslint-disable */
        return {
            id_token: this.jwtService.sign(payload)
        };
    }

    /* eslint-enable */
    async validateUser(payload: Payload): Promise<UserDTO | undefined> {
        return await this.findUserWithAuthById(payload.id);
    }

    async findUserWithAuthById(userId: string): Promise<UserDTO | undefined> {
        return await this.userService.findByFields({ where: { id: userId } });
    }

    async getAccount(userId: string): Promise<UserDTO | undefined> {
        const userDTO: UserDTO = await this.findUserWithAuthById(userId);
        if (!userDTO) {
            return;
        }
        return userDTO;
    }

    async changePassword(
        userLogin: string,
        currentClearTextPassword: string,
        newPassword: string
    ): Promise<void> {
        const userFind: UserDTO = await this.userService.findByFields({
            where: { login: userLogin }
        });
        if (!userFind) {
            throw new HttpException('유효하지 않은 아이디 입니다!', HttpStatus.BAD_REQUEST);
        }
        if (userFind.password !== currentClearTextPassword) {
            throw new HttpException('유효하지 않은 패스워드 입니다!', HttpStatus.BAD_REQUEST);
        }
        userFind.password = newPassword;
        await this.userService.save(userFind);
        return;
    }

    async registerNewUser(newUser: UserDTO): Promise<UserDTO> {
        let userFind: UserDTO = await this.userService.findByFields({
            where: { login: newUser.login }
        });
        if (userFind) {
            throw new HttpException('유효하지 않은 아이디 입니다!', HttpStatus.BAD_REQUEST);
        }
        userFind = await this.userService.findByFields({ where: { email: newUser.email } });
        if (userFind) {
            throw new HttpException('이메일이 이미 사용 중입니다!', HttpStatus.BAD_REQUEST);
        }
        newUser.activationKey = RandomUtil.generateKey();
        newUser.authorities = ['ROLE_USER'];
        newUser.createdDate = new Date();

        await this.mailService.sendActivationEmail(newUser);

        return await this.userService.save(newUser);
    }

    async updateUserSettings(userLogin: string, newUserInfo: UserDTO): Promise<UserDTO> {
        const userFind: UserDTO = await this.userService.findByFields({
            where: { login: userLogin }
        });
        if (!userFind) {
            throw new HttpException('유효하지 않은 아이디 입니다!', HttpStatus.BAD_REQUEST);
        }
        if (userFind.email && userFind.email !== newUserInfo.email) {
            throw new HttpException('이메일이 이미 사용중입니다!', HttpStatus.BAD_REQUEST);
        }

        userFind.name = newUserInfo.name;
        userFind.email = newUserInfo.email;
        userFind.phone = newUserInfo.phone;
        userFind.address = newUserInfo.address;
        userFind.birth = newUserInfo.birth;
        userFind.department = newUserInfo.department
        await this.userService.save(userFind);
        return;
    }

    async activateRegistration(key: string) {
        const userFind = await this.userService.findByFields({
            where: { activationKey: key }
        });
        if (!userFind) {
            throw new HttpException(
                '활성화 키가 유효하지 않거나 존재하지 않습니다!',
                HttpStatus.BAD_REQUEST
            );
        }
        userFind.activated = true;
        userFind.activationKey = null;
        await this.userService.save(userFind);
        return;
    }

    async requestPasswordReset(email: string) {
        const userFind: UserDTO = await this.userService.findByFields({
            where: { email: email }
        });
        if (!userFind) {
            throw new HttpException('존재하지 않는 이메일 정보 입니다!', HttpStatus.BAD_REQUEST);
        }

        userFind.resetKey = RandomUtil.generateKey();
        userFind.resetDate = new Date();
        userFind.lastModifiedBy = 'system';
        userFind.lastModifiedDate = new Date();
        await this.userService.save(userFind);
        await this.mailService.sendPasswordResetMail(userFind);
        return;
    }

    async completePasswordReset(key: string, newPassword: string) {
        const date = new Date();
        const authDate = new Date(date.setDate(date.getDate() - 1));
        const userFind: UserDTO = await this.userService.findByFields({
            where: { resetKey: key, resetDate: MoreThanOrEqual(authDate) }
        });
        if (!userFind) {
            throw new HttpException('존재하지 않는 사용자 정보 입니다!', HttpStatus.BAD_REQUEST);
        }
        userFind.password = newPassword;
        userFind.resetKey = null;
        userFind.resetDate = null;
        userFind.lastModifiedDate = new Date();
        userFind.lastModifiedBy = userFind.login;
        await this.userService.save(userFind);
        return;
    }

}
