import { HttpException, HttpStatus, Injectable, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { FindManyOptions, FindOneOptions } from 'typeorm';
import { PlantsDTO } from './dto/plants.dto';
import { PlantsMapper } from './mapper/plants.mapper';
import { PlantsRepository } from '../repository/plants.repository';
import { ErrorMsg } from '../client/errorMsg';
import { PlantsHistoryRepository } from '../repository/history/plants.history.repository';

const relationshipNames = [];

@Injectable()
export class PlantsService {
    logger = new Logger('PlantsService');

    constructor(@InjectRepository(PlantsRepository) private plantsRepository: PlantsRepository,
                @InjectRepository(PlantsHistoryRepository) private plantsHisRepository: PlantsHistoryRepository) {
    }

    async findById(id: string): Promise<PlantsDTO | undefined> {
        const options = { relations: relationshipNames };
        const result = await this.plantsRepository.findOne(id, options);
        return PlantsMapper.fromEntityToDTO(result);
    }

    async findByHistory(id: string): Promise<PlantsDTO[]> {
        const resultList = await this.plantsHisRepository.find({
            where: { historyId: id } , order: {id:  "DESC" }
        });
        const plantDTO: PlantsDTO[] = [];
        resultList.forEach(plants => {
                if (plants.id % 2 == 0) {
                    plantDTO.push(PlantsMapper.fromEntityToDTO(plants.payload));
                }
            }
        );
        return plantDTO;
    }

    async findByFields(options: FindOneOptions<PlantsDTO>): Promise<PlantsDTO | undefined> {
        const result = await this.plantsRepository.findOne(options);
        return PlantsMapper.fromEntityToDTO(result);
    }

    async findAndCount(options: FindManyOptions<PlantsDTO>): Promise<[PlantsDTO[], number]> {
        options.relations = relationshipNames;
        const resultList = await this.plantsRepository.findAndCount(options);
        const plantsDTO: PlantsDTO[] = [];
        if (resultList && resultList[0]) {
            resultList[0].forEach(plants => plantsDTO.push(PlantsMapper.fromEntityToDTO(plants)));
            resultList[0] = plantsDTO;
        }
        return resultList;
    }

    async save(plantsDTO: PlantsDTO, userName: string): Promise<PlantsDTO | undefined> {
        const entity = PlantsMapper.fromDTOtoEntity(plantsDTO);
        entity.createdDate = new Date();
        entity.createdBy = userName;
        const result = await this.plantsRepository.save(entity).catch(err => {
            throw new HttpException(ErrorMsg.forSql(err.code), HttpStatus.BAD_REQUEST);
        });
        return PlantsMapper.fromEntityToDTO(result);
    }

    async update(plantsDTO: PlantsDTO, userName: string): Promise<PlantsDTO | undefined> {
        const entity = PlantsMapper.fromDTOtoEntity(plantsDTO);
        entity.lastModifiedDate = new Date();
        entity.lastModifiedBy = userName;
        const result = await this.plantsRepository.save(entity).catch(err => {
            throw new HttpException(ErrorMsg.forSql(err.code), HttpStatus.BAD_REQUEST);
        });
        return PlantsMapper.fromEntityToDTO(result);
    }

    async deleteById(id: string): Promise<void | undefined> {
        await this.plantsRepository.delete(id).catch(err => {
            throw new HttpException(ErrorMsg.forSql(err.code), HttpStatus.BAD_REQUEST);
        });
        const entityFind = await this.findById(id);
        if (entityFind) {
            throw new HttpException('Error, entity not deleted!', HttpStatus.NOT_FOUND);
        }
        return;
    }

}
