import { Body, Controller, Get, Logger, Post, UploadedFile, UseInterceptors } from '@nestjs/common';
import { FileService } from '../../service/file.service';
import { FileInterceptor } from '@nestjs/platform-express';
import { FileDTO } from '../../service/dto/file.dto';
import { Express } from 'express';
import { LoggingInterceptor } from '../../client/interceptors/logging.interceptor';


@Controller('api')
@UseInterceptors(LoggingInterceptor)
export class FileController {
    logger = new Logger('FileController');

    constructor(private readonly fileService: FileService) {
    }

    @Get('/hello')
    sayHello() {
        return this.fileService.getHello();
    }


    @UseInterceptors(FileInterceptor('file'))
    @Post('/file')
    uploadFile(
        @Body() fileDTO: FileDTO,
        @UploadedFile() file: Express.Multer.File
    ) {
        return {
            fileDTO,
            file: file.buffer.toString()
        };
    }


}
