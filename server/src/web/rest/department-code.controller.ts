import {
    Body,
    Controller,
    Delete,
    Get,
    Logger,
    Param,
    Post as PostMethod,
    Put,
    UseGuards,
    Req,
    UseInterceptors,
} from '@nestjs/common';
import { ApiBearerAuth, ApiUseTags, ApiResponse, ApiOperation } from '@nestjs/swagger';
import { Request } from 'express';
import { DepartmentCodeDTO } from '../../service/dto/department-code.dto';
import { DepartmentCodeService } from '../../service/department-code.service';
import { PageRequest, Page } from '../../domain/base/pagination.entity';
import { AuthGuard, Roles, RolesGuard, RoleType } from '../../security';
import { HeaderUtil } from '../../client/header-util';
import { LoggingInterceptor } from '../../client/interceptors/logging.interceptor';

@Controller('api/department-codes')
@UseGuards(AuthGuard, RolesGuard)
@UseInterceptors(LoggingInterceptor)
@ApiBearerAuth()
@ApiUseTags('department-codes')
export class DepartmentCodeController {
    logger = new Logger('DepartmentCodeController');

    constructor(private readonly departmentCodeService: DepartmentCodeService) {}

    @Get('/')
    @Roles(RoleType.USER)
    @ApiResponse({
        status: 200,
        description: 'List all records',
        type: DepartmentCodeDTO,
    })
    async getAll(@Req() req: Request): Promise<DepartmentCodeDTO[]> {
        const pageRequest: PageRequest = new PageRequest(
            req.query.page,
            req.query.size,
            req.query.sort,
        );
        const [results, count] = await this.departmentCodeService.findAndCount({
            skip: +pageRequest.page * pageRequest.size,
            take: +pageRequest.size,
            order: pageRequest.sort.asOrder(),
        });
        HeaderUtil.addPaginationHeaders(req.res, new Page(results, count, pageRequest));
        return results;
    }

    @Get('/:id')
    @Roles(RoleType.USER)
    @ApiResponse({
        status: 200,
        description: 'The found record',
        type: DepartmentCodeDTO,
    })
    async getOne(@Param('id') id: string): Promise<DepartmentCodeDTO> {
        return await this.departmentCodeService.findById(id);
    }

    @PostMethod('/')
    @Roles(RoleType.ADMIN)
    @ApiOperation({ title: 'Create departmentCode' })
    @ApiResponse({
        status: 201,
        description: 'The record has been successfully created.',
        type: DepartmentCodeDTO,
    })
    @ApiResponse({ status: 403, description: 'Forbidden.' })
    async post(
        @Req() req: Request,
            @Body() departmentCodeDTO: DepartmentCodeDTO,
    ): Promise<DepartmentCodeDTO> {
        const created = await this.departmentCodeService.save(departmentCodeDTO);
        HeaderUtil.addEntityCreatedHeaders(req.res, 'DepartmentCode', created.id);
        return created;
    }

    @Put('/')
    @Roles(RoleType.ADMIN)
    @ApiOperation({ title: 'Update departmentCode' })
    @ApiResponse({
        status: 200,
        description: 'The record has been successfully updated.',
        type: DepartmentCodeDTO,
    })
    async put(
        @Req() req: Request,
            @Body() departmentCodeDTO: DepartmentCodeDTO,
    ): Promise<DepartmentCodeDTO> {
        HeaderUtil.addEntityCreatedHeaders(req.res, 'DepartmentCode', departmentCodeDTO.id);
        return await this.departmentCodeService.update(departmentCodeDTO);
    }

    @Delete('/:id')
    @Roles(RoleType.ADMIN)
    @ApiOperation({ title: 'Delete departmentCode' })
    @ApiResponse({
        status: 204,
        description: 'The record has been successfully deleted.',
    })
    async deleteById(@Req() req: Request, @Param('id') id: string): Promise<void> {
        HeaderUtil.addEntityDeletedHeaders(req.res, 'DepartmentCode', id);
        return await this.departmentCodeService.deleteById(id);
    }
}
