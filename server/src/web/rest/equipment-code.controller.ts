import {
    Body,
    Controller,
    Delete,
    Get,
    Logger,
    Param,
    Post as PostMethod,
    Put,
    UseGuards,
    Req,
    UseInterceptors,
} from '@nestjs/common';
import { ApiBearerAuth, ApiUseTags, ApiResponse, ApiOperation } from '@nestjs/swagger';
import { Request } from 'express';
import { EquipmentCodeDTO } from '../../service/dto/equipment-code.dto';
import { EquipmentCodeService } from '../../service/equipment-code.service';
import { PageRequest, Page } from '../../domain/base/pagination.entity';
import { AuthGuard, Roles, RolesGuard, RoleType } from '../../security';
import { HeaderUtil } from '../../client/header-util';
import { LoggingInterceptor } from '../../client/interceptors/logging.interceptor';

@Controller('api/equipment-codes')
@UseGuards(AuthGuard, RolesGuard)
@UseInterceptors(LoggingInterceptor)
@ApiBearerAuth()
@ApiUseTags('equipment-codes')
export class EquipmentCodeController {
    logger = new Logger('EquipmentCodeController');

    constructor(private readonly equipmentCodeService: EquipmentCodeService) {}

    @Get('/')
    @Roles(RoleType.USER)
    @ApiResponse({
        status: 200,
        description: 'List all records',
        type: EquipmentCodeDTO,
    })
    async getAll(@Req() req: Request): Promise<EquipmentCodeDTO[]> {
        const pageRequest: PageRequest = new PageRequest(
            req.query.page,
            req.query.size,
            req.query.sort,
        );
        const [results, count] = await this.equipmentCodeService.findAndCount({
            skip: +pageRequest.page * pageRequest.size,
            take: +pageRequest.size,
            order: pageRequest.sort.asOrder(),
        });
        HeaderUtil.addPaginationHeaders(req.res, new Page(results, count, pageRequest));
        return results;
    }

    @Get('/:id')
    @Roles(RoleType.USER)
    @ApiResponse({
        status: 200,
        description: 'The found record',
        type: EquipmentCodeDTO,
    })
    async getOne(@Param('id') id: string): Promise<EquipmentCodeDTO> {
        return await this.equipmentCodeService.findById(id);
    }

    @PostMethod('/')
    @Roles(RoleType.ADMIN)
    @ApiOperation({ title: 'Create equipmentCode' })
    @ApiResponse({
        status: 201,
        description: 'The record has been successfully created.',
        type: EquipmentCodeDTO,
    })
    @ApiResponse({ status: 403, description: 'Forbidden.' })
    async post(
        @Req() req: Request,
            @Body() equipmentCodeDTO: EquipmentCodeDTO,
    ): Promise<EquipmentCodeDTO> {
        const created = await this.equipmentCodeService.save(equipmentCodeDTO);
        HeaderUtil.addEntityCreatedHeaders(req.res, 'EquipmentCode', created.id);
        return created;
    }

    @Put('/')
    @Roles(RoleType.ADMIN)
    @ApiOperation({ title: 'Update equipmentCode' })
    @ApiResponse({
        status: 200,
        description: 'The record has been successfully updated.',
        type: EquipmentCodeDTO,
    })
    async put(
        @Req() req: Request,
            @Body() equipmentCodeDTO: EquipmentCodeDTO,
    ): Promise<EquipmentCodeDTO> {
        HeaderUtil.addEntityCreatedHeaders(req.res, 'EquipmentCode', equipmentCodeDTO.id);
        return await this.equipmentCodeService.update(equipmentCodeDTO);
    }

    @Delete('/:id')
    @Roles(RoleType.ADMIN)
    @ApiOperation({ title: 'Delete equipmentCode' })
    @ApiResponse({
        status: 204,
        description: 'The record has been successfully deleted.',
    })
    async deleteById(@Req() req: Request, @Param('id') id: string): Promise<void> {
        HeaderUtil.addEntityDeletedHeaders(req.res, 'EquipmentCode', id);
        return await this.equipmentCodeService.deleteById(id);
    }
}
