import {
    Body,
    Controller,
    Delete,
    Get,
    Logger,
    Param,
    Post as PostMethod,
    Put,
    UseGuards,
    Req,
    UseInterceptors,
} from '@nestjs/common';
import { ApiBearerAuth, ApiUseTags, ApiResponse, ApiOperation } from '@nestjs/swagger';
import { Request } from 'express';
import { CompPipeDTO } from '../../service/dto/comp-pipe.dto';
import { CompPipeService } from '../../service/comp-pipe.service';
import { PageRequest, Page } from '../../domain/base/pagination.entity';
import { AuthGuard, Roles, RolesGuard, RoleType } from '../../security';
import { HeaderUtil } from '../../client/header-util';
import { LoggingInterceptor } from '../../client/interceptors/logging.interceptor';

@Controller('api/comp-pipes')
@UseGuards(AuthGuard, RolesGuard)
@UseInterceptors(LoggingInterceptor)
@ApiBearerAuth()
@ApiUseTags('comp-pipes')
export class CompPipeController {
    logger = new Logger('CompPipeController');

    constructor(private readonly compPipeService: CompPipeService) {}

    @Get('/')
    @Roles(RoleType.USER)
    @ApiResponse({
        status: 200,
        description: 'List all records',
        type: CompPipeDTO,
    })
    async getAll(@Req() req: Request): Promise<CompPipeDTO[]> {
        const pageRequest: PageRequest = new PageRequest(
            req.query.page,
            req.query.size,
            req.query.sort,
        );
        const [results, count] = await this.compPipeService.findAndCount({
            skip: +pageRequest.page * pageRequest.size,
            take: +pageRequest.size,
            order: pageRequest.sort.asOrder(),
        });
        HeaderUtil.addPaginationHeaders(req.res, new Page(results, count, pageRequest));
        return results;
    }

    @Get('/:id')
    @Roles(RoleType.USER)
    @ApiResponse({
        status: 200,
        description: 'The found record',
        type: CompPipeDTO,
    })
    async getOne(@Param('id') id: string): Promise<CompPipeDTO> {
        return await this.compPipeService.findById(id);
    }

    @PostMethod('/')
    @Roles(RoleType.ADMIN)
    @ApiOperation({ title: 'Create compPipe' })
    @ApiResponse({
        status: 201,
        description: 'The record has been successfully created.',
        type: CompPipeDTO,
    })
    @ApiResponse({ status: 403, description: 'Forbidden.' })
    async post(@Req() req: Request, @Body() compPipeDTO: CompPipeDTO): Promise<CompPipeDTO> {
        const created = await this.compPipeService.save(compPipeDTO);
        HeaderUtil.addEntityCreatedHeaders(req.res, 'CompPipe', created.id);
        return created;
    }

    @Put('/')
    @Roles(RoleType.ADMIN)
    @ApiOperation({ title: 'Update compPipe' })
    @ApiResponse({
        status: 200,
        description: 'The record has been successfully updated.',
        type: CompPipeDTO,
    })
    async put(@Req() req: Request, @Body() compPipeDTO: CompPipeDTO): Promise<CompPipeDTO> {
        HeaderUtil.addEntityCreatedHeaders(req.res, 'CompPipe', compPipeDTO.id);
        return await this.compPipeService.update(compPipeDTO);
    }

    @Delete('/:id')
    @Roles(RoleType.ADMIN)
    @ApiOperation({ title: 'Delete compPipe' })
    @ApiResponse({
        status: 204,
        description: 'The record has been successfully deleted.',
    })
    async deleteById(@Req() req: Request, @Param('id') id: string): Promise<void> {
        HeaderUtil.addEntityDeletedHeaders(req.res, 'CompPipe', id);
        return await this.compPipeService.deleteById(id);
    }
}
