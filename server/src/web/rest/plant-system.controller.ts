import {
    Body,
    Controller,
    Delete,
    Get,
    Logger,
    Param,
    Post as PostMethod,
    Put,
    Req,
    UseGuards,
    UseInterceptors
} from '@nestjs/common';
import { ApiBearerAuth, ApiOperation, ApiResponse, ApiUseTags } from '@nestjs/swagger';
import { Request } from 'express';
import { PlantSystemDTO } from '../../service/dto/plant-system.dto';
import { PlantSystemService } from '../../service/plant-system.service';
import { Page, PageRequest } from '../../domain/base/pagination.entity';
import { AuthGuard, Roles, RolesGuard, RoleType } from '../../security';
import { HeaderUtil } from '../../client/header-util';
import { LoggingInterceptor } from '../../client/interceptors/logging.interceptor';

@Controller('api/plant-systems')
@UseGuards(AuthGuard, RolesGuard)
@UseInterceptors(LoggingInterceptor)
@ApiBearerAuth()
@ApiUseTags('plant-systems')
export class PlantSystemController {
    logger = new Logger('PlantSystemController');

    constructor(private readonly plantSystemService: PlantSystemService) {}

    @Get('/')
    @Roles(RoleType.USER)
    @ApiResponse({
        status: 200,
        description: 'List all records',
        type: PlantSystemDTO,
    })
    async getAll(@Req() req: Request): Promise<PlantSystemDTO[]> {
        const pageRequest: PageRequest = new PageRequest(
            req.query.page,
            req.query.size,
            req.query.sort,
        );
        const [results, count] = await this.plantSystemService.findAndCount({
            skip: +pageRequest.page * pageRequest.size,
            take: +pageRequest.size,
            order: pageRequest.sort.asOrder(),
        });
        HeaderUtil.addPaginationHeaders(req.res, new Page(results, count, pageRequest));
        return results;
    }

    @Get('/:id')
    @Roles(RoleType.USER)
    @ApiResponse({
        status: 200,
        description: 'The found record',
        type: PlantSystemDTO,
    })
    async getOne(@Param('id') id: string): Promise<PlantSystemDTO> {
        return await this.plantSystemService.findById(id);
    }

    @Get('/:id/history')
    @Roles(RoleType.USER)
    @ApiResponse( {
        status: 200,
        description: 'Record\'s History'
    })
    async getOneHistory(@Req() req: Request, @Param('id') id: string): Promise<PlantSystemDTO[]> {
        return await this.plantSystemService.findByHistory(id);
    }


    @PostMethod('/')
    @Roles(RoleType.USER)
    @ApiOperation({ title: 'Create plantSystem' })
    @ApiResponse({
        status: 201,
        description: 'The record has been successfully created.',
        type: PlantSystemDTO,
    })
    @ApiResponse({ status: 403, description: 'Forbidden.' })
    async post(
        @Req() req: Request,
            @Body() plantSystemDTO: PlantSystemDTO,
    ): Promise<PlantSystemDTO> {
        const user: any = req.user;
        const created = await this.plantSystemService.save(plantSystemDTO, user.name);
        HeaderUtil.addEntityCreatedHeaders(req.res, 'PlantSystem', created.id);
        return created;
    }

    @Put('/')
    @Roles(RoleType.USER)
    @ApiOperation({ title: 'Update plantSystem' })
    @ApiResponse({
        status: 200,
        description: 'The record has been successfully updated.',
        type: PlantSystemDTO,
    })
    async put(
        @Req() req: Request,
            @Body() plantSystemDTO: PlantSystemDTO,
    ): Promise<PlantSystemDTO> {
        const user: any = req.user;
        HeaderUtil.addEntityCreatedHeaders(req.res, 'PlantSystem', plantSystemDTO.id);
        return await this.plantSystemService.update(plantSystemDTO, user.name);
    }

    @Delete('/:id')
    @Roles(RoleType.USER)
    @ApiOperation({ title: 'Delete plantSystem' })
    @ApiResponse({
        status: 204,
        description: 'The record has been successfully deleted.',
    })
    async deleteById(@Req() req: Request, @Param('id') id: string): Promise<void> {
        HeaderUtil.addEntityDeletedHeaders(req.res, 'PlantSystem', id);
        return await this.plantSystemService.deleteById(id);
    }
}
