import {
    Body,
    Controller,
    Delete,
    Get,
    Logger,
    Param,
    Post as PostMethod,
    Put,
    Req,
    UseGuards,
    UseInterceptors
} from '@nestjs/common';
import { ApiBearerAuth, ApiOperation, ApiResponse, ApiUseTags } from '@nestjs/swagger';
import { Request } from 'express';
import { RequirementDTO } from '../../service/dto/requirement.dto';
import { RequirementService } from '../../service/requirement.service';
import { Page, PageRequest } from '../../domain/base/pagination.entity';
import { AuthGuard, Roles, RolesGuard, RoleType } from '../../security';
import { HeaderUtil } from '../../client/header-util';
import { LoggingInterceptor } from '../../client/interceptors/logging.interceptor';

@Controller('api/requirements')
@UseGuards(AuthGuard, RolesGuard)
@UseInterceptors(LoggingInterceptor)
@ApiBearerAuth()
@ApiUseTags('requirements')
export class RequirementController {
    logger = new Logger('RequirementController');

    constructor(private readonly requirementService: RequirementService) {}

    @Get('/')
    @Roles(RoleType.USER)
    @ApiResponse({
        status: 200,
        description: 'List all records',
        type: RequirementDTO,
    })
    async getAll(@Req() req: Request): Promise<RequirementDTO[]> {
        const pageRequest: PageRequest = new PageRequest(
            req.query.page,
            req.query.size,
            req.query.sort,
        );
        const [results, count] = await this.requirementService.findAndCount({
            skip: +pageRequest.page * pageRequest.size,
            take: +pageRequest.size,
            order: pageRequest.sort.asOrder(),
        });
        HeaderUtil.addPaginationHeaders(req.res, new Page(results, count, pageRequest));
        return results;
    }

    @Get('/:id')
    @Roles(RoleType.USER)
    @ApiResponse({
        status: 200,
        description: 'The found record',
        type: RequirementDTO,
    })
    async getOne(@Param('id') id: string): Promise<RequirementDTO> {
        return await this.requirementService.findById(id);
    }

    @Get('/:id/history')
    @Roles(RoleType.USER)
    @ApiResponse( {
        status: 200,
        description: 'Record\'s History'
    })
    async getOneHistory(@Req() req: Request, @Param('id') id: string): Promise<RequirementDTO[]> {
        return await this.requirementService.findByHistory(id);
    }


    @PostMethod('/')
    @Roles(RoleType.USER)
    @ApiOperation({ title: 'Create requirement' })
    @ApiResponse({
        status: 201,
        description: 'The record has been successfully created.',
        type: RequirementDTO,
    })
    @ApiResponse({ status: 403, description: 'Forbidden.' })
    async post(
        @Req() req: Request,
            @Body() requirementDTO: RequirementDTO,
    ): Promise<RequirementDTO> {
        const user: any = req.user;
        const created = await this.requirementService.save(requirementDTO, user.name);
        HeaderUtil.addEntityCreatedHeaders(req.res, 'Requirement', created.id);
        return created;
    }

    @Put('/')
    @Roles(RoleType.USER)
    @ApiOperation({ title: 'Update requirement' })
    @ApiResponse({
        status: 200,
        description: 'The record has been successfully updated.',
        type: RequirementDTO,
    })
    async put(
        @Req() req: Request,
            @Body() requirementDTO: RequirementDTO,
    ): Promise<RequirementDTO> {
        const user: any = req.user;
        HeaderUtil.addEntityCreatedHeaders(req.res, 'Requirement', requirementDTO.id);
        return await this.requirementService.update(requirementDTO, user.name);
    }

    @Delete('/:id')
    @Roles(RoleType.USER)
    @ApiOperation({ title: 'Delete requirement' })
    @ApiResponse({
        status: 204,
        description: 'The record has been successfully deleted.',
    })
    async deleteById(@Req() req: Request, @Param('id') id: string): Promise<void> {
        HeaderUtil.addEntityDeletedHeaders(req.res, 'Requirement', id);
        return await this.requirementService.deleteById(id);
    }
}
