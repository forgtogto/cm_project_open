import {
    Body,
    Controller,
    Delete,
    Get,
    Logger,
    Param,
    Post as PostMethod,
    Put,
    UseGuards,
    Req,
    UseInterceptors
} from '@nestjs/common';
import { ApiBearerAuth, ApiUseTags, ApiResponse, ApiOperation } from '@nestjs/swagger';
import { Request } from 'express';
import { ContentDTO } from '../../service/dto/content.dto';
import { ContentService } from '../../service/content.service';
import { PageRequest, Page } from '../../domain/base/pagination.entity';
import { AuthGuard, Roles, RolesGuard, RoleType } from '../../security';
import { HeaderUtil } from '../../client/header-util';
import { LoggingInterceptor } from '../../client/interceptors/logging.interceptor';
import { RequirementDTO } from '../../service/dto/requirement.dto';

@Controller('api/contents')
@UseGuards(AuthGuard, RolesGuard)
@UseInterceptors(LoggingInterceptor)
@ApiBearerAuth()
@ApiUseTags('contents')
export class ContentController {
    logger = new Logger('ContentController');

    constructor(private readonly contentService: ContentService) {
    }

    @Get('/')
    @Roles(RoleType.USER)
    @ApiResponse({
        status: 200,
        description: 'List all records',
        type: ContentDTO
    })
    async getAll(@Req() req: Request): Promise<ContentDTO[]> {
        const pageRequest: PageRequest = new PageRequest(
            req.query.page,
            req.query.size,
            req.query.sort
        );
        const [results, count] = await this.contentService.findAndCount({
            skip: +pageRequest.page * pageRequest.size,
            take: +pageRequest.size,
            order: pageRequest.sort.asOrder()
        });
        HeaderUtil.addPaginationHeaders(req.res, new Page(results, count, pageRequest));
        return results;
    }

    @Get('/:id')
    @Roles(RoleType.USER)
    @ApiResponse({
        status: 200,
        description: 'The found record',
        type: ContentDTO
    })
    async getOne(@Param('id') id: string): Promise<ContentDTO> {
        return await this.contentService.findById(id);
    }

    @Get('/:id/history')
    @Roles(RoleType.USER)
    @ApiResponse({
        status: 200,
        description: 'Record\'s History'
    })
    async getOneHistory(@Req() req: Request, @Param('id') id: string): Promise<ContentDTO[]> {
        return await this.contentService.findByHistory(id);
    }


    @PostMethod('/')
    @Roles(RoleType.USER)
    @ApiOperation({ title: 'Create content' })
    @ApiResponse({
        status: 201,
        description: 'The record has been successfully created.',
        type: ContentDTO
    })
    @ApiResponse({ status: 403, description: 'Forbidden.' })
    async post(@Req() req: Request, @Body() contentDTO: ContentDTO): Promise<ContentDTO> {
        const user: any = req.user;
        const created = await this.contentService.save(contentDTO, user.name);
        HeaderUtil.addEntityCreatedHeaders(req.res, 'Content', created.id);
        return created;
    }

    @Put('/')
    @Roles(RoleType.USER)
    @ApiOperation({ title: 'Update content' })
    @ApiResponse({
        status: 200,
        description: 'The record has been successfully updated.',
        type: ContentDTO
    })
    async put(@Req() req: Request, @Body() contentDTO: ContentDTO): Promise<ContentDTO> {
        const user: any = req.user;
        HeaderUtil.addEntityCreatedHeaders(req.res, 'Content', contentDTO.id);
        return await this.contentService.update(contentDTO, user.name);
    }

    @Delete('/:id')
    @Roles(RoleType.USER)
    @ApiOperation({ title: 'Delete content' })
    @ApiResponse({
        status: 204,
        description: 'The record has been successfully deleted.'
    })
    async deleteById(@Req() req: Request, @Param('id') id: string): Promise<void> {
        HeaderUtil.addEntityDeletedHeaders(req.res, 'Content', id);
        return await this.contentService.deleteById(id);
    }
}
