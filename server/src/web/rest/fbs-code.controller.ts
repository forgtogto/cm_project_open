import {
    Body,
    Controller,
    Delete,
    Get,
    Logger,
    Param,
    Post as PostMethod,
    Put,
    UseGuards,
    Req,
    UseInterceptors,
} from '@nestjs/common';
import { ApiBearerAuth, ApiUseTags, ApiResponse, ApiOperation } from '@nestjs/swagger';
import { Request } from 'express';
import { FbsCodeDTO } from '../../service/dto/fbs-code.dto';
import { FbsCodeService } from '../../service/fbs-code.service';
import { PageRequest, Page } from '../../domain/base/pagination.entity';
import { AuthGuard, Roles, RolesGuard, RoleType } from '../../security';
import { HeaderUtil } from '../../client/header-util';
import { LoggingInterceptor } from '../../client/interceptors/logging.interceptor';

@Controller('api/fbs-codes')
@UseGuards(AuthGuard, RolesGuard)
@UseInterceptors(LoggingInterceptor)
@ApiBearerAuth()
@ApiUseTags('fbs-codes')
export class FbsCodeController {
    logger = new Logger('FbsCodeController');

    constructor(private readonly fbsCodeService: FbsCodeService) {}

    @Get('/')
    @Roles(RoleType.USER)
    @ApiResponse({
        status: 200,
        description: 'List all records',
        type: FbsCodeDTO,
    })
    async getAll(@Req() req: Request): Promise<FbsCodeDTO[]> {
        const pageRequest: PageRequest = new PageRequest(
            req.query.page,
            req.query.size,
            req.query.sort,
        );
        const [results, count] = await this.fbsCodeService.findAndCount({
            skip: +pageRequest.page * pageRequest.size,
            take: +pageRequest.size,
            order: pageRequest.sort.asOrder(),
        });
        HeaderUtil.addPaginationHeaders(req.res, new Page(results, count, pageRequest));
        return results;
    }

    @Get('/:id')
    @Roles(RoleType.USER)
    @ApiResponse({
        status: 200,
        description: 'The found record',
        type: FbsCodeDTO,
    })
    async getOne(@Param('id') id: string): Promise<FbsCodeDTO> {
        return await this.fbsCodeService.findById(id);
    }

    @PostMethod('/')
    @Roles(RoleType.ADMIN)
    @ApiOperation({ title: 'Create fbsCode' })
    @ApiResponse({
        status: 201,
        description: 'The record has been successfully created.',
        type: FbsCodeDTO,
    })
    @ApiResponse({ status: 403, description: 'Forbidden.' })
    async post(@Req() req: Request, @Body() fbsCodeDTO: FbsCodeDTO): Promise<FbsCodeDTO> {
        const created = await this.fbsCodeService.save(fbsCodeDTO);
        HeaderUtil.addEntityCreatedHeaders(req.res, 'FbsCode', created.id);
        return created;
    }

    @Put('/')
    @Roles(RoleType.ADMIN)
    @ApiOperation({ title: 'Update fbsCode' })
    @ApiResponse({
        status: 200,
        description: 'The record has been successfully updated.',
        type: FbsCodeDTO,
    })
    async put(@Req() req: Request, @Body() fbsCodeDTO: FbsCodeDTO): Promise<FbsCodeDTO> {
        HeaderUtil.addEntityCreatedHeaders(req.res, 'FbsCode', fbsCodeDTO.id);
        return await this.fbsCodeService.update(fbsCodeDTO);
    }

    @Delete('/:id')
    @Roles(RoleType.ADMIN)
    @ApiOperation({ title: 'Delete fbsCode' })
    @ApiResponse({
        status: 204,
        description: 'The record has been successfully deleted.',
    })
    async deleteById(@Req() req: Request, @Param('id') id: string): Promise<void> {
        HeaderUtil.addEntityDeletedHeaders(req.res, 'FbsCode', id);
        return await this.fbsCodeService.deleteById(id);
    }
}
