import {
    Body,
    Controller,
    Delete,
    Get,
    Logger,
    Param,
    Post,
    Put,
    Req,
    UseGuards,
    UseInterceptors
} from '@nestjs/common';
import { ApiBearerAuth, ApiOperation, ApiResponse, ApiUseTags } from '@nestjs/swagger';
import { Request } from 'express';
import { PlantsDTO } from '../../service/dto/plants.dto';
import { PlantsService } from '../../service/plants.service';
import { Page, PageRequest } from '../../domain/base/pagination.entity';
import { AuthGuard, Roles, RolesGuard, RoleType } from '../../security';
import { HeaderUtil } from '../../client/header-util';
import { LoggingInterceptor } from '../../client/interceptors/logging.interceptor';

@Controller('api/plants')
@UseGuards(AuthGuard, RolesGuard)
@UseInterceptors(LoggingInterceptor)
@ApiBearerAuth()
@ApiUseTags('plants')
@Roles(RoleType.USER)
export class PlantsController {
    logger = new Logger('PlantsController');

    constructor(private readonly plantsService: PlantsService) {}

    @Get('/')
    @ApiResponse({
        status: 200,
        description: 'List all records',
        type: PlantsDTO,
    })
    async getAll(@Req() req: Request): Promise<PlantsDTO[]> {
        const pageRequest: PageRequest = new PageRequest(
            req.query.page,
            req.query.size,
            req.query.sort,
        );
        const [results, count] = await this.plantsService.findAndCount({
            skip: +pageRequest.page * pageRequest.size,
            take: +pageRequest.size,
            order: pageRequest.sort.asOrder(),
        });
        HeaderUtil.addPaginationHeaders(req.res, new Page(results, count, pageRequest));
        return results;
    }

    @Get('/:id')
    @Roles(RoleType.USER)
    @ApiResponse({
        status: 200,
        description: 'The found record',
        type: PlantsDTO,
    })
    async getOne(@Param('id') id: string): Promise<PlantsDTO> {
        return await this.plantsService.findById(id);
    }

    @Get('/:id/history')
    @Roles(RoleType.USER)
    @ApiResponse( {
        status: 200,
        description: 'Record\'s History'
    })
    async getOneHistory(@Req() req: Request, @Param('id') id: string): Promise<PlantsDTO[]> {
        return await this.plantsService.findByHistory(id);
    }

    @Post('/')
    @Roles(RoleType.USER)
    @ApiOperation({ title: 'Create plants' })
    @ApiResponse({
        status: 201,
        description: 'The record has been successfully created.',
        type: PlantsDTO,
    })
    @ApiResponse({ status: 403, description: 'Forbidden.' })
    async post(@Req() req: Request, @Body() plantsDTO: PlantsDTO): Promise<PlantsDTO> {
        const user: any = req.user;
        const created = await this.plantsService.save(plantsDTO, user.name);
        HeaderUtil.addEntityCreatedHeaders(req.res, 'Plants', created.id);
        return created;
    }

    @Put('/')
    @Roles(RoleType.USER)
    @ApiOperation({ title: 'Update plants' })
    @ApiResponse({
        status: 200,
        description: 'The record has been successfully updated.',
        type: PlantsDTO,
    })
    async put(@Req() req: Request, @Body() plantsDTO: PlantsDTO): Promise<PlantsDTO> {
        const user: any = req.user;
        HeaderUtil.addEntityCreatedHeaders(req.res, 'Plants', plantsDTO.id);
        return await this.plantsService.update(plantsDTO, user.name);
    }

    @Delete('/:id')
    @Roles(RoleType.USER)
    @ApiOperation({ title: 'Delete plants' })
    @ApiResponse({
        status: 204,
        description: 'The record has been successfully deleted.',
    })
    async deleteById(@Req() req: Request, @Param('id') id: string): Promise<void> {
        HeaderUtil.addEntityDeletedHeaders(req.res, 'Plants', id);
        return await this.plantsService.deleteById(id);
    }
}
