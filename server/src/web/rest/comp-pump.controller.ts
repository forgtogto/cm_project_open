import {
    Body,
    Controller,
    Delete,
    Get,
    Logger,
    Param,
    Post as PostMethod,
    Put,
    UseGuards,
    Req,
    UseInterceptors,
} from '@nestjs/common';
import { ApiBearerAuth, ApiUseTags, ApiResponse, ApiOperation } from '@nestjs/swagger';
import { Request } from 'express';
import { CompPumpDTO } from '../../service/dto/comp-pump.dto';
import { CompPumpService } from '../../service/comp-pump.service';
import { PageRequest, Page } from '../../domain/base/pagination.entity';
import { AuthGuard, Roles, RolesGuard, RoleType } from '../../security';
import { HeaderUtil } from '../../client/header-util';
import { LoggingInterceptor } from '../../client/interceptors/logging.interceptor';

@Controller('api/comp-pumps')
@UseGuards(AuthGuard, RolesGuard)
@UseInterceptors(LoggingInterceptor)
@ApiBearerAuth()
@ApiUseTags('comp-pumps')
export class CompPumpController {
    logger = new Logger('CompPumpController');

    constructor(private readonly compPumpService: CompPumpService) {}

    @Get('/')
    @Roles(RoleType.USER)
    @ApiResponse({
        status: 200,
        description: 'List all records',
        type: CompPumpDTO,
    })
    async getAll(@Req() req: Request): Promise<CompPumpDTO[]> {
        const pageRequest: PageRequest = new PageRequest(
            req.query.page,
            req.query.size,
            req.query.sort,
        );
        const [results, count] = await this.compPumpService.findAndCount({
            skip: +pageRequest.page * pageRequest.size,
            take: +pageRequest.size,
            order: pageRequest.sort.asOrder(),
        });
        HeaderUtil.addPaginationHeaders(req.res, new Page(results, count, pageRequest));
        return results;
    }

    @Get('/:id')
    @Roles(RoleType.USER)
    @ApiResponse({
        status: 200,
        description: 'The found record',
        type: CompPumpDTO,
    })
    async getOne(@Param('id') id: string): Promise<CompPumpDTO> {
        return await this.compPumpService.findById(id);
    }

    @PostMethod('/')
    @Roles(RoleType.ADMIN)
    @ApiOperation({ title: 'Create compPump' })
    @ApiResponse({
        status: 201,
        description: 'The record has been successfully created.',
        type: CompPumpDTO,
    })
    @ApiResponse({ status: 403, description: 'Forbidden.' })
    async post(@Req() req: Request, @Body() compPumpDTO: CompPumpDTO): Promise<CompPumpDTO> {
        const created = await this.compPumpService.save(compPumpDTO);
        HeaderUtil.addEntityCreatedHeaders(req.res, 'CompPump', created.id);
        return created;
    }

    @Put('/')
    @Roles(RoleType.ADMIN)
    @ApiOperation({ title: 'Update compPump' })
    @ApiResponse({
        status: 200,
        description: 'The record has been successfully updated.',
        type: CompPumpDTO,
    })
    async put(@Req() req: Request, @Body() compPumpDTO: CompPumpDTO): Promise<CompPumpDTO> {
        HeaderUtil.addEntityCreatedHeaders(req.res, 'CompPump', compPumpDTO.id);
        return await this.compPumpService.update(compPumpDTO);
    }

    @Delete('/:id')
    @Roles(RoleType.ADMIN)
    @ApiOperation({ title: 'Delete compPump' })
    @ApiResponse({
        status: 204,
        description: 'The record has been successfully deleted.',
    })
    async deleteById(@Req() req: Request, @Param('id') id: string): Promise<void> {
        HeaderUtil.addEntityDeletedHeaders(req.res, 'CompPump', id);
        return await this.compPumpService.deleteById(id);
    }
}
