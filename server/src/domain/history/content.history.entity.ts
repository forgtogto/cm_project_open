import {
    HistoryActionColumn,
    HistoryActionType,
    HistoryFor,
    MappedColumn,
    SnapshotColumn
} from '@kittgen/nestjs-typeorm-history';
import { Entity, PrimaryGeneratedColumn } from 'typeorm';
import { Content } from '../content.entity';

@Entity('th_content_history')
@HistoryFor(Content)
export class ContentHistoryEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @SnapshotColumn({ type: 'jsonb'  })
    payload: Content;

    @HistoryActionColumn()
    action: HistoryActionType;

    @MappedColumn<Content>((content: Content) => content.id, { name: 'content_id' })
    historyId: string;
}
