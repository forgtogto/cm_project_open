import {
    HistoryActionColumn,
    HistoryActionType,
    HistoryFor,
    MappedColumn,
    SnapshotColumn
} from '@kittgen/nestjs-typeorm-history';
import { Entity, PrimaryGeneratedColumn } from 'typeorm';
import { Requirement } from '../requirement.entity';

@Entity('th_requirement_history')
@HistoryFor(Requirement)
export class RequirementHistoryEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @SnapshotColumn({ type: 'jsonb'  })
    payload: Requirement;

    @HistoryActionColumn()
    action: HistoryActionType;

    @MappedColumn<Requirement>((requirement: Requirement) => requirement.id, { name: 'requirement_id' })
    historyId: string;
}
