import {
    HistoryActionColumn,
    HistoryActionType,
    HistoryFor,
    MappedColumn,
    SnapshotColumn
} from '@kittgen/nestjs-typeorm-history';
import { Entity, PrimaryGeneratedColumn } from 'typeorm';
import { Plants } from '../plants.entity';

@Entity('th_plants_history')
@HistoryFor(Plants)
export class PlantsHistoryEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @SnapshotColumn({ type: 'jsonb'  })
    payload: Plants;

    @HistoryActionColumn()
    action: HistoryActionType;

    @MappedColumn<Plants>((plants: Plants) => plants.id, { name: 'plants_id' })
    historyId: string;
}
