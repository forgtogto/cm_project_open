import {
    HistoryActionColumn,
    HistoryActionType,
    HistoryFor,
    MappedColumn,
    SnapshotColumn
} from '@kittgen/nestjs-typeorm-history';
import { Entity, PrimaryGeneratedColumn } from 'typeorm';
import { PlantSystem } from '../plant-system.entity';

@Entity('th_plant_system_history')
@HistoryFor(PlantSystem)
export class PlantSystemHistoryEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @SnapshotColumn({ type: 'jsonb'  })
    payload: PlantSystem;

    @HistoryActionColumn()
    action: HistoryActionType;

    @MappedColumn<PlantSystem>((plantSystem: PlantSystem) => plantSystem.id, { name: 'plant_system_id' })
    historyId: string;
}
