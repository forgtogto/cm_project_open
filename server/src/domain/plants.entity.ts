/* eslint-disable @typescript-eslint/no-unused-vars */
import {
    Entity,
    Column,
    JoinColumn,
    OneToOne,
    ManyToOne,
    OneToMany,
    ManyToMany,
    JoinTable,
} from 'typeorm';
import { BaseEntity } from './base/base.entity';

/**
 * A Plants.
 */
@Entity('tn_plants')
export class Plants extends BaseEntity {
    @Column({ name: 'code', unique: true })
    code: string;

    @Column({ name: 'name' })
    name: string;

    @Column({ type: 'text', name: 'comment', nullable: true })
    comment: any;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
}
