/* eslint-disable @typescript-eslint/no-unused-vars */
import {
    Entity,
    Column,
    JoinColumn,
    OneToOne,
    ManyToOne,
    OneToMany,
    ManyToMany,
    JoinTable,
} from 'typeorm';
import { BaseEntity } from './base/base.entity';

/**
 * A FbsCode.
 */
@Entity('tc_fbs_code')
export class FbsCode extends BaseEntity {
    @Column({ name: 'code', unique: true })
    code: string;

    @Column({ name: 'fbs_type', nullable: true })
    fbsType: string;

    @Column({ type: 'text', name: 'comment', nullable: true })
    comment: any;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
}
