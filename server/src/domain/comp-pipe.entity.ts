/* eslint-disable @typescript-eslint/no-unused-vars */
import {
    Entity,
    Column,
    JoinColumn,
    OneToOne,
    ManyToOne,
    OneToMany,
    ManyToMany,
    JoinTable,
} from 'typeorm';
import { BaseEntity } from './base/base.entity';

import { PlantSystem } from './plant-system.entity';
import { EquipmentCode } from './equipment-code.entity';
import { ComponentsEntity } from './base/components.entity';

/**
 * A CompPipe.
 */
@Entity('tn_comp_pipe')
export class CompPipe extends ComponentsEntity {
    @Column({ name: 'pipe_class', nullable: true })
    pipeClass: string;

    @Column({ name: 'schedule', nullable: true })
    schedule: string;

    @Column({ name: 'thickness', nullable: true })
    thickness: string;

    @ManyToOne(type => PlantSystem)
    plantSystem: PlantSystem;

    @ManyToOne(type => EquipmentCode)
    equipmentCode: EquipmentCode;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
}
