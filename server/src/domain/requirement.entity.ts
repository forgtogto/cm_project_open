/* eslint-disable @typescript-eslint/no-unused-vars */
import {
    Entity,
    Column,
    JoinColumn,
    OneToOne,
    ManyToOne,
    OneToMany,
    ManyToMany,
    JoinTable,
} from 'typeorm';
import { BaseEntity } from './base/base.entity';

import { PlantSystem } from './plant-system.entity';
import { ReqStatus } from './enumeration/req-status';
import { ReqCmGrade } from './enumeration/req-cm-grade';
import { RequesterGroup } from './enumeration/requester-group';

import { User } from './user.entity';

/**
 * A Requirement.
 */
@Entity('tn_requirement')
export class Requirement extends BaseEntity {
    @Column({ name: 'code', unique: true })
    code: string;

    @Column({ name: 'name' })
    name: string;

    @Column({ type: 'text', name: 'comment', nullable: true })
    comment: any;

    @Column({ type: 'simple-enum', name: 'req_status', enum: ReqStatus })
    reqStatus: ReqStatus;

    @Column({ type: 'simple-enum', name: 'cm_grade', enum: ReqCmGrade })
    cmGrade: ReqCmGrade;

    @Column({ type: 'simple-enum', name: 'requester_group', enum: RequesterGroup })
    requesterGroup: RequesterGroup;

    @ManyToOne(type => User)
    users: User;

    @ManyToOne(type => PlantSystem)
    plantSystem: PlantSystem;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
}
