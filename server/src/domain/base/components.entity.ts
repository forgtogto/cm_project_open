import { Column, ObjectIdColumn, PrimaryGeneratedColumn } from 'typeorm';
import { ComSubName } from '../enumeration/com-sub-name';
import { ReqCmGrade } from '../enumeration/req-cm-grade';
import { EquipSubName } from '../enumeration/equip-sub-name';

export abstract class ComponentsEntity {
    @ObjectIdColumn()
    @PrimaryGeneratedColumn('uuid')
    id?: string;

    @Column({ nullable: true })
    createdBy?: string;
    @Column({ nullable: true })
    createdDate?: Date;
    @Column({ nullable: true })
    lastModifiedBy?: string;
    @Column({ nullable: true })
    lastModifiedDate?: Date;

    @Column({ nullable: true })
    version?: string;
    @Column({ nullable: false, unique: true })
    code?: string;
    @Column({ nullable: false })
    name?: string;
    @Column({ nullable: true })
    serialNo?: string;
    @Column({ nullable: true })
    filePath?: string;
    @Column({ nullable: true })
    fileName?: string;

    @Column({ type: 'simple-enum', name: 'com-sub-name', enum: ComSubName })
    compSubName: ComSubName;

    @Column({ type: 'simple-enum', name: 'equip-sub-name', enum: EquipSubName })
    equipSubName: EquipSubName;

    @Column({ type: 'simple-enum', name: 'req-cm-grade', enum: ReqCmGrade })
    cmGrade: ReqCmGrade;

    @Column({ nullable: true })
    manufacturer?: string;
    @Column({ nullable: true })
    size?: string;
    @Column({ nullable: true })
    envZoneFlag?: string;
}
