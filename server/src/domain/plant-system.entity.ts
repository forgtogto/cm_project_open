/* eslint-disable @typescript-eslint/no-unused-vars */
import {
    Entity,
    Column,
    JoinColumn,
    OneToOne,
    ManyToOne,
    OneToMany,
    ManyToMany,
    JoinTable,
} from 'typeorm';
import { BaseEntity } from './base/base.entity';

import { Plants } from './plants.entity';

/**
 * A PlantSystem.
 */
@Entity('tn_plant_system')
export class PlantSystem extends BaseEntity {
    @Column({ name: 'code', unique: true })
    code: string;

    @Column({ name: 'name' })
    name: string;

    @Column({ type: 'text', name: 'comment', nullable: true })
    comment: any;

    @ManyToOne(type => Plants)
    plants: Plants;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
}
