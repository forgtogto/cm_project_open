/**
 * The RequesterGroup enumeration.
 */
export enum RequesterGroup {
    GROUP1 = '건축 부서',
    GROUP2 = '토목 부서',
    GROUP3 = '생산 부서',
    GROUP4 = '사업관리 부서',
    GROUP5 = '기계 부서',
    GROUP6 = '전기 부서',
}
