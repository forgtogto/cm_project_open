/**
 * The ReqCmGrade enumeration.
 */
export enum ComSubName {
    PIPE = '배관(Pipe)',
    VALVE = '벨브(Valve)',
    PUMP = '펌프(Pump)',
    TANK = '탱크(Tank)',
}
