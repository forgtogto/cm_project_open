import { Authority } from './authority.entity';
import { Entity, Column, ManyToMany, JoinTable } from 'typeorm';
import { BaseEntity } from './base/base.entity';
import { config } from '../config';
import { EncryptionTransformer } from 'typeorm-encrypted';
import { RequesterGroup } from './enumeration/requester-group';

@Entity('tn_user')
export class User extends BaseEntity {
    @Column({ unique: true })
    login: string;

    @Column({ nullable: true })
    name?: string;
    @Column( { nullable: true })
    address?: string;
    @Column( { nullable: true })
    birth?: Date;
    @Column( { nullable: true })
    phone?: string;
    @Column({ type: 'simple-enum', name: 'requester_group', enum: RequesterGroup, nullable: true  })
    department: RequesterGroup;

    @Column()
    email: string;
    @Column({ default: false })
    activated?: boolean;
    @Column({ default: 'en' })
    langKey?: string;

    // eslint-disable-next-line
	@ManyToMany(type => Authority)
    @JoinTable({name: 'tr_user_authority'})
    authorities?: any[];

    @Column({
        type: 'varchar',
        transformer: new EncryptionTransformer({
            key: config.get('crypto.key'),
            algorithm: 'aes-256-cbc',
            ivLength: 16,
            iv: config.get('crypto.iv'),
        }),
    })
    password: string;
    @Column({ nullable: true })
    imageUrl?: string;
    @Column({ nullable: true })
    activationKey?: string;
    @Column({ nullable: true })
    resetKey?: string;
    @Column({ nullable: true })
    resetDate?: Date;
}
