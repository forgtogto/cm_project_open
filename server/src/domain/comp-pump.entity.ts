/* eslint-disable @typescript-eslint/no-unused-vars */
import {
    Entity,
    Column,
    JoinColumn,
    OneToOne,
    ManyToOne,
    OneToMany,
    ManyToMany,
    JoinTable,
} from 'typeorm';
import { BaseEntity } from './base/base.entity';

import { EquipmentCode } from './equipment-code.entity';
import { PlantSystem } from './plant-system.entity';
import { ComponentsEntity } from './base/components.entity';

/**
 * A CompPump.
 */
@Entity('tn_comp_pump')
export class CompPump extends ComponentsEntity {
    @Column({ name: 'de_pressure', nullable: true })
    dePressure: string;

    @Column({ name: 'op_pressure', nullable: true })
    opPressure: string;

    @Column({ name: 'quantity', nullable: true })
    quantity: string;

    @ManyToOne(type => EquipmentCode)
    equipmentCode: EquipmentCode;

    @ManyToOne(type => PlantSystem)
    plantSystem: PlantSystem;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
}
