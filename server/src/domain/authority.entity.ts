import { Entity, PrimaryColumn } from 'typeorm';
import { ApiModelProperty } from '@nestjs/swagger';

@Entity('tn_authority')
export class Authority {
    @ApiModelProperty({ example: 'ROLE_USER', description: 'User role' })
    @PrimaryColumn()
    name: string;
}
