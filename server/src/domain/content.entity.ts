/* eslint-disable @typescript-eslint/no-unused-vars */
import {
    Entity,
    Column,
    JoinColumn,
    OneToOne,
    ManyToOne,
    OneToMany,
    ManyToMany,
    JoinTable,
} from 'typeorm';
import { BaseEntity } from './base/base.entity';

import { PlantSystem } from './plant-system.entity';
import { DepartmentCode } from './department-code.entity';

/**
 * A Content.
 */
@Entity('tn_content')
export class Content extends BaseEntity {
    @Column({ name: 'version', nullable: true })
    version: string;

    @Column({ name: 'code', unique: true })
    code: string;

    @Column({ name: 'name' })
    name: string;

    @Column({ name: 'serial_no', nullable: true })
    serialNo: string;

    @Column({ name: 'file_path', nullable: true })
    filePath: string;

    @Column({ name: 'file_name', nullable: true })
    fileName: string;

    @ManyToOne(type => PlantSystem)
    plantSystem: PlantSystem;

    @ManyToOne(type => DepartmentCode)
    departmentCode: DepartmentCode;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
}
