import { Test, TestingModule } from '@nestjs/testing';
import request = require('supertest');
import { AppModule } from '../src/app.module';
import { INestApplication } from '@nestjs/common';
import { AuthGuard } from '../src/security/guards/auth.guard';
import { RolesGuard } from '../src/security/guards/roles.guard';
import { PlantSystemDTO } from '../src/service/dto/plant-system.dto';
import { PlantSystemService } from '../src/service/plant-system.service';

describe('PlantSystem Controller', () => {
    let app: INestApplication;

    const authGuardMock = { canActivate: (): any => true };
    const rolesGuardMock = { canActivate: (): any => true };
    const entityMock: any = {
        id: 'entityId',
    };

    const serviceMock = {
        findById: (): any => entityMock,
        findAndCount: (): any => [entityMock, 0],
        save: (): any => entityMock,
        update: (): any => entityMock,
        deleteById: (): any => entityMock,
    };

    beforeEach(async () => {
        const moduleFixture: TestingModule = await Test.createTestingModule({
            imports: [AppModule],
        })
            .overrideGuard(AuthGuard)
            .useValue(authGuardMock)
            .overrideGuard(RolesGuard)
            .useValue(rolesGuardMock)
            .overrideProvider(PlantSystemService)
            .useValue(serviceMock)
            .compile();

        app = moduleFixture.createNestApplication();
        await app.init();
    });

    it('/GET all plant-systems ', async () => {
        const getEntities: PlantSystemDTO[] = (
            await request(app.getHttpServer())
                .get('/api/plant-systems')
                .expect(200)
        ).body;

        expect(getEntities).toEqual(entityMock);
    });

    it('/GET plant-systems by id', async () => {
        const getEntity: PlantSystemDTO = (
            await request(app.getHttpServer())
                .get('/api/plant-systems/' + entityMock.id)
                .expect(200)
        ).body;

        expect(getEntity).toEqual(entityMock);
    });

    it('/POST create plant-systems', async () => {
        const createdEntity: PlantSystemDTO = (
            await request(app.getHttpServer())
                .post('/api/plant-systems')
                .send(entityMock)
                .expect(201)
        ).body;

        expect(createdEntity).toEqual(entityMock);
    });

    it('/PUT update plant-systems', async () => {
        const updatedEntity: PlantSystemDTO = (
            await request(app.getHttpServer())
                .put('/api/plant-systems')
                .send(entityMock)
                .expect(201)
        ).body;

        expect(updatedEntity).toEqual(entityMock);
    });

    it('/DELETE plant-systems', async () => {
        const deletedEntity: PlantSystemDTO = (
            await request(app.getHttpServer())
                .delete('/api/plant-systems/' + entityMock.id)
                .expect(204)
        ).body;

        expect(deletedEntity).toEqual({});
    });

    afterEach(async () => {
        await app.close();
    });
});
