import { Test, TestingModule } from '@nestjs/testing';
import request = require('supertest');
import { AppModule } from '../src/app.module';
import { INestApplication } from '@nestjs/common';
import { AuthGuard } from '../src/security/guards/auth.guard';
import { RolesGuard } from '../src/security/guards/roles.guard';
import { EquipmentCodeDTO } from '../src/service/dto/equipment-code.dto';
import { EquipmentCodeService } from '../src/service/equipment-code.service';

describe('EquipmentCode Controller', () => {
    let app: INestApplication;

    const authGuardMock = { canActivate: (): any => true };
    const rolesGuardMock = { canActivate: (): any => true };
    const entityMock: any = {
        id: 'entityId',
    };

    const serviceMock = {
        findById: (): any => entityMock,
        findAndCount: (): any => [entityMock, 0],
        save: (): any => entityMock,
        update: (): any => entityMock,
        deleteById: (): any => entityMock,
    };

    beforeEach(async () => {
        const moduleFixture: TestingModule = await Test.createTestingModule({
            imports: [AppModule],
        })
            .overrideGuard(AuthGuard)
            .useValue(authGuardMock)
            .overrideGuard(RolesGuard)
            .useValue(rolesGuardMock)
            .overrideProvider(EquipmentCodeService)
            .useValue(serviceMock)
            .compile();

        app = moduleFixture.createNestApplication();
        await app.init();
    });

    it('/GET all equipment-codes ', async () => {
        const getEntities: EquipmentCodeDTO[] = (
            await request(app.getHttpServer())
                .get('/api/equipment-codes')
                .expect(200)
        ).body;

        expect(getEntities).toEqual(entityMock);
    });

    it('/GET equipment-codes by id', async () => {
        const getEntity: EquipmentCodeDTO = (
            await request(app.getHttpServer())
                .get('/api/equipment-codes/' + entityMock.id)
                .expect(200)
        ).body;

        expect(getEntity).toEqual(entityMock);
    });

    it('/POST create equipment-codes', async () => {
        const createdEntity: EquipmentCodeDTO = (
            await request(app.getHttpServer())
                .post('/api/equipment-codes')
                .send(entityMock)
                .expect(201)
        ).body;

        expect(createdEntity).toEqual(entityMock);
    });

    it('/PUT update equipment-codes', async () => {
        const updatedEntity: EquipmentCodeDTO = (
            await request(app.getHttpServer())
                .put('/api/equipment-codes')
                .send(entityMock)
                .expect(201)
        ).body;

        expect(updatedEntity).toEqual(entityMock);
    });

    it('/DELETE equipment-codes', async () => {
        const deletedEntity: EquipmentCodeDTO = (
            await request(app.getHttpServer())
                .delete('/api/equipment-codes/' + entityMock.id)
                .expect(204)
        ).body;

        expect(deletedEntity).toEqual({});
    });

    afterEach(async () => {
        await app.close();
    });
});
