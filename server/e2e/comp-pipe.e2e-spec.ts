import { Test, TestingModule } from '@nestjs/testing';
import request = require('supertest');
import { AppModule } from '../src/app.module';
import { INestApplication } from '@nestjs/common';
import { AuthGuard } from '../src/security/guards/auth.guard';
import { RolesGuard } from '../src/security/guards/roles.guard';
import { CompPipeDTO } from '../src/service/dto/comp-pipe.dto';
import { CompPipeService } from '../src/service/comp-pipe.service';

describe('CompPipe Controller', () => {
    let app: INestApplication;

    const authGuardMock = { canActivate: (): any => true };
    const rolesGuardMock = { canActivate: (): any => true };
    const entityMock: any = {
        id: 'entityId',
    };

    const serviceMock = {
        findById: (): any => entityMock,
        findAndCount: (): any => [entityMock, 0],
        save: (): any => entityMock,
        update: (): any => entityMock,
        deleteById: (): any => entityMock,
    };

    beforeEach(async () => {
        const moduleFixture: TestingModule = await Test.createTestingModule({
            imports: [AppModule],
        })
            .overrideGuard(AuthGuard)
            .useValue(authGuardMock)
            .overrideGuard(RolesGuard)
            .useValue(rolesGuardMock)
            .overrideProvider(CompPipeService)
            .useValue(serviceMock)
            .compile();

        app = moduleFixture.createNestApplication();
        await app.init();
    });

    it('/GET all comp-pipes ', async () => {
        const getEntities: CompPipeDTO[] = (
            await request(app.getHttpServer())
                .get('/api/comp-pipes')
                .expect(200)
        ).body;

        expect(getEntities).toEqual(entityMock);
    });

    it('/GET comp-pipes by id', async () => {
        const getEntity: CompPipeDTO = (
            await request(app.getHttpServer())
                .get('/api/comp-pipes/' + entityMock.id)
                .expect(200)
        ).body;

        expect(getEntity).toEqual(entityMock);
    });

    it('/POST create comp-pipes', async () => {
        const createdEntity: CompPipeDTO = (
            await request(app.getHttpServer())
                .post('/api/comp-pipes')
                .send(entityMock)
                .expect(201)
        ).body;

        expect(createdEntity).toEqual(entityMock);
    });

    it('/PUT update comp-pipes', async () => {
        const updatedEntity: CompPipeDTO = (
            await request(app.getHttpServer())
                .put('/api/comp-pipes')
                .send(entityMock)
                .expect(201)
        ).body;

        expect(updatedEntity).toEqual(entityMock);
    });

    it('/DELETE comp-pipes', async () => {
        const deletedEntity: CompPipeDTO = (
            await request(app.getHttpServer())
                .delete('/api/comp-pipes/' + entityMock.id)
                .expect(204)
        ).body;

        expect(deletedEntity).toEqual({});
    });

    afterEach(async () => {
        await app.close();
    });
});
