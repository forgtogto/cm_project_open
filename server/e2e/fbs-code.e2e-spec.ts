import { Test, TestingModule } from '@nestjs/testing';
import request = require('supertest');
import { AppModule } from '../src/app.module';
import { INestApplication } from '@nestjs/common';
import { AuthGuard } from '../src/security/guards/auth.guard';
import { RolesGuard } from '../src/security/guards/roles.guard';
import { FbsCodeDTO } from '../src/service/dto/fbs-code.dto';
import { FbsCodeService } from '../src/service/fbs-code.service';

describe('FbsCode Controller', () => {
    let app: INestApplication;

    const authGuardMock = { canActivate: (): any => true };
    const rolesGuardMock = { canActivate: (): any => true };
    const entityMock: any = {
        id: 'entityId',
    };

    const serviceMock = {
        findById: (): any => entityMock,
        findAndCount: (): any => [entityMock, 0],
        save: (): any => entityMock,
        update: (): any => entityMock,
        deleteById: (): any => entityMock,
    };

    beforeEach(async () => {
        const moduleFixture: TestingModule = await Test.createTestingModule({
            imports: [AppModule],
        })
            .overrideGuard(AuthGuard)
            .useValue(authGuardMock)
            .overrideGuard(RolesGuard)
            .useValue(rolesGuardMock)
            .overrideProvider(FbsCodeService)
            .useValue(serviceMock)
            .compile();

        app = moduleFixture.createNestApplication();
        await app.init();
    });

    it('/GET all fbs-codes ', async () => {
        const getEntities: FbsCodeDTO[] = (
            await request(app.getHttpServer())
                .get('/api/fbs-codes')
                .expect(200)
        ).body;

        expect(getEntities).toEqual(entityMock);
    });

    it('/GET fbs-codes by id', async () => {
        const getEntity: FbsCodeDTO = (
            await request(app.getHttpServer())
                .get('/api/fbs-codes/' + entityMock.id)
                .expect(200)
        ).body;

        expect(getEntity).toEqual(entityMock);
    });

    it('/POST create fbs-codes', async () => {
        const createdEntity: FbsCodeDTO = (
            await request(app.getHttpServer())
                .post('/api/fbs-codes')
                .send(entityMock)
                .expect(201)
        ).body;

        expect(createdEntity).toEqual(entityMock);
    });

    it('/PUT update fbs-codes', async () => {
        const updatedEntity: FbsCodeDTO = (
            await request(app.getHttpServer())
                .put('/api/fbs-codes')
                .send(entityMock)
                .expect(201)
        ).body;

        expect(updatedEntity).toEqual(entityMock);
    });

    it('/DELETE fbs-codes', async () => {
        const deletedEntity: FbsCodeDTO = (
            await request(app.getHttpServer())
                .delete('/api/fbs-codes/' + entityMock.id)
                .expect(204)
        ).body;

        expect(deletedEntity).toEqual({});
    });

    afterEach(async () => {
        await app.close();
    });
});
