import { Test, TestingModule } from '@nestjs/testing';
import request = require('supertest');
import { AppModule } from '../src/app.module';
import { INestApplication } from '@nestjs/common';
import { AuthGuard } from '../src/security/guards/auth.guard';
import { RolesGuard } from '../src/security/guards/roles.guard';
import { DepartmentCodeDTO } from '../src/service/dto/department-code.dto';
import { DepartmentCodeService } from '../src/service/department-code.service';

describe('DepartmentCode Controller', () => {
    let app: INestApplication;

    const authGuardMock = { canActivate: (): any => true };
    const rolesGuardMock = { canActivate: (): any => true };
    const entityMock: any = {
        id: 'entityId',
    };

    const serviceMock = {
        findById: (): any => entityMock,
        findAndCount: (): any => [entityMock, 0],
        save: (): any => entityMock,
        update: (): any => entityMock,
        deleteById: (): any => entityMock,
    };

    beforeEach(async () => {
        const moduleFixture: TestingModule = await Test.createTestingModule({
            imports: [AppModule],
        })
            .overrideGuard(AuthGuard)
            .useValue(authGuardMock)
            .overrideGuard(RolesGuard)
            .useValue(rolesGuardMock)
            .overrideProvider(DepartmentCodeService)
            .useValue(serviceMock)
            .compile();

        app = moduleFixture.createNestApplication();
        await app.init();
    });

    it('/GET all department-codes ', async () => {
        const getEntities: DepartmentCodeDTO[] = (
            await request(app.getHttpServer())
                .get('/api/department-codes')
                .expect(200)
        ).body;

        expect(getEntities).toEqual(entityMock);
    });

    it('/GET department-codes by id', async () => {
        const getEntity: DepartmentCodeDTO = (
            await request(app.getHttpServer())
                .get('/api/department-codes/' + entityMock.id)
                .expect(200)
        ).body;

        expect(getEntity).toEqual(entityMock);
    });

    it('/POST create department-codes', async () => {
        const createdEntity: DepartmentCodeDTO = (
            await request(app.getHttpServer())
                .post('/api/department-codes')
                .send(entityMock)
                .expect(201)
        ).body;

        expect(createdEntity).toEqual(entityMock);
    });

    it('/PUT update department-codes', async () => {
        const updatedEntity: DepartmentCodeDTO = (
            await request(app.getHttpServer())
                .put('/api/department-codes')
                .send(entityMock)
                .expect(201)
        ).body;

        expect(updatedEntity).toEqual(entityMock);
    });

    it('/DELETE department-codes', async () => {
        const deletedEntity: DepartmentCodeDTO = (
            await request(app.getHttpServer())
                .delete('/api/department-codes/' + entityMock.id)
                .expect(204)
        ).body;

        expect(deletedEntity).toEqual({});
    });

    afterEach(async () => {
        await app.close();
    });
});
