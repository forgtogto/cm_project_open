# CM Prototype System

이 어플리케이션은 Jhipster 6.8.0 [NodeJS blueprint](https://github.com/jhipster/generator-jhipster-nodejs)로 제작 되었습니다.

you can find documentation and help at https://www.jhipster.tech/documentation-archive/v6.8.0. For any questions you can refer to the stream lead: Angelo Manganiello.

## 간략 소개

1. 사용 스킬 : NestJS, React, Bootstrap5, PostgreSQL
2. 설명: CM(Configuration & Management) 관리를 위한 프로젝트로서 항목들의 관계 설정 및 히스토리 관리에 대한 서비스를 제공합니다.

![img.png](web_main.png)

---

## Development


 

이 프로젝트를 빌드하려면 먼저 컴퓨터에 다음 종속성을 설치하고 구성해야합니다.

1. [Node.js][]: Node.js를 사용하여 개발 웹 서버를 실행하고 프로젝트를 빌드합니다.

Node를 설치 한 후 다음 명령을 실행하여 개발 도구를 설치할 수 있어야합니다. [package.json](package.json).에서 종속성이 변경된 경우에만이 명령을 실행하면됩니다.

    npm install
    cd server && npm install

npm 스크립트와 [Webpack][]을 빌드 시스템으로 사용합니다.

두 개의 별도 터미널에서 다음 명령을 실행하여 하드 드라이브에서 파일이 변경 될 때 브라우저가 자동으로 새로 고쳐지는 행복한 개발 환경을 만듭니다.

    cd server && npm start
    npm start


#### 확인사항
* /server/.env 파일에 개인 메일 정보를 추가하세요.
* /server/src/orm.config.ts 파일 내 DB 정보를 추가하세요.

## Building and running

#### Running

```bash
npm run start:app
```

#### Building

```bash
npm build:app
```

모든 컴파일 된 소스가있는 빌드 폴더는 **server/dist** 입니다.

> 전체 스택 서버 / 클라이언트 빌드에 대한 자세한 설명은 [server/README.md](server/README.md) 를 확인하세요.

### Client tests

Unit tests are run by [Jest][] and written with [Jasmine][]. They're located in [src/test/javascript/](src/test/javascript/) and can be run with:

    npm test

UI end-to-end tests are powered by [Protractor][], which is built on top of WebDriverJS. They're located in [src/test/javascript/e2e](src/test/javascript/e2e)
and can be run in a terminal (`npm run e2e`) after that the full application is run (`npm run start:app`).

For more information, refer to the [Running tests page][].

[jhipster homepage and latest documentation]: https://www.jhipster.tech
[jhipster 6.8.0 archive]: https://www.jhipster.tech/documentation-archive/v6.8.0
[using jhipster in development]: https://www.jhipster.tech/documentation-archive/v6.8.0/development/
[using docker and docker-compose]: https://www.jhipster.tech/documentation-archive/v6.8.0/docker-compose
[using jhipster in production]: https://www.jhipster.tech/documentation-archive/v6.8.0/production/
[running tests page]: https://www.jhipster.tech/documentation-archive/v6.8.0/running-tests/
[code quality page]: https://www.jhipster.tech/documentation-archive/v6.8.0/code-quality/
[setting up continuous integration]: https://www.jhipster.tech/documentation-archive/v6.8.0/setting-up-ci/
[node.js]: https://nodejs.org/
[yarn]: https://yarnpkg.org/
[webpack]: https://webpack.github.io/
[jest]: https://facebook.github.io/jest/
[nestjs]: https://nestjs.com/
[nestjs cli]: https://docs.nestjs.com/cli/usages
[angular cli]: https://cli.angular.io/
[browsersync]: http://www.browsersync.io/
[jasmine]: http://jasmine.github.io/2.0/introduction.html
[protractor]: https://angular.github.io/protractor/
[leaflet]: http://leafletjs.com/
[definitelytyped]: http://definitelytyped.org/
